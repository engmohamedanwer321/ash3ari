import { AppRegistry, I18nManager, Platform } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { Navigation } from "react-native-navigation";
import registerScreens from './src/screens'
import { responsiveWidth } from './src/utils/responsiveDimensions';

console.disableYellowBox = true
I18nManager.allowRTL(false)
registerScreens()

const options = {



}


Navigation.events().registerAppLaunchedListener(() => {

  //determine properities of topBar,layout,statusBar of all screens 
  Navigation.setDefaultOptions({

    topBar: {
      visible: false
    },
    layout: {
      backgroundColor: 'white',
      orientation: ['portrait'] // An array of supported orientations
    },
    statusBar: {
      //visible: false,
      backgroundColor: '#16476A',  //in android only
      blur: true // in ios only
    },
    animations: {
      // animation for root screen only
      setRoot: {
        enabled: 'true', //| 'false', // Optional, used to enable/disable the animation
        alpha: {
          from: 0,
          to: 1,
          duration: 600,
          startDelay: 0,
          interpolation: 'accelerate'
        }
      },
      // animation for push screens
      push: {
        enabled: 'true',  //| 'false', // Optional, used to enable/disable the animation
        content: {
          // animation from right to left
          /*x: {
            from: 800,
            to: 0,
            startDelay: 0,
            duration: 1500,
          }*/
          //animation from bottom to top
          y: {
            from: 2000,
            to: 0,
            startDelay: 0,
            duration: 600,
          }
        },
        topBar: {
          id: 'TEST', // Optional, id of the TopBar we'd like to animate.
          alpha: {
            from: 0,
            to: 1
          }
        },
      },
      pop: {
        enabled: true,
        //animation from bottom to top
        content: {
          y: {
            from: 0,
            to: 2000,
            startDelay: 0,
            duration: 600,
          }
        }
      }
    }

  })

  Navigation.setRoot({
    root: {
      sideMenu: {
        id: 'slideMenu',

        left: {

          component: {
            id: 'MenuContent',
            name: 'MenuContent'
          }
        },
        center: {
          stack: {
            id: 'AppStack',
            children: [
              {
                component: {
                  id: 'SplashScreen',
                  name: "SplashScreen"
                }
              },

            ]
          },
        },

        right: {

          component: {
            id: 'MenuContent',
            name: 'MenuContent'
          }
        },
       options:
       Platform.OS === 'android' ?
        {  ////// With Android Only
        
          sideMenu: {
            left: {

              width:  responsiveWidth(75)  // You can set the width here...
            },

            right: {
              width: responsiveWidth(75) 
            }
          }
          

        }
        :
          {}




      }
    }
  });
});


