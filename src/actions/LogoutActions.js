import { LOGOUT,LOGOUT_LOADING,LOGOUT_LOADING_END, LOGGEDOUT } from "./types";
import { BASE_END_POINT } from '../AppConfig';
import AsyncStorage  from '@react-native-community/async-storage'
//import { AsyncStorage, Platform } from 'react-native';
import axios from 'axios';


export function logout(FB_token, BE_token, navigator) {
  
    return  dispatch => {
        dispatch({type:LOGOUT_LOADING})
        axios.post(`${BASE_END_POINT}logout`,{}, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${BE_token}`
          },
        }).then(response => {
          
            
             AsyncStorage.removeItem("@QsathaUser");
            if (Platform.OS === 'android') {
                navigator.resetTo({
                    screen: 'Login',
                    animated: true,
                    animationType:'slide-horizontal'
                })       
            }
            else {
                navigator.resetTo({
                    screen: 'Login',
                    animated: true,
                    animationType:'slide-horizontal'
                })
            }

            dispatch({ type: LOGOUT })
          
        }).catch(error => {
        
          
            dispatch({type:LOGOUT_LOADING_END})
        });
        
    }
}

export function loggedOut(){
    return  dispatch => {
    dispatch({ type: LOGGEDOUT })
    }
}