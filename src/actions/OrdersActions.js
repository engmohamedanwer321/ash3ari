import axios from 'axios';
import Strigs from '../assets/strings';
import { BASE_END_POINT} from '../AppConfig';
import {
    ORDERS_COUNT
} from './types';
import { RNToasty } from 'react-native-toasty';



export function getOrdersCount(token, userType) {
 
    if (userType=='client'){
        url = `${BASE_END_POINT}client/ordered/pending?page=1`
        
    }else{
        url = `${BASE_END_POINT}provider/ordered/pending?page=1`
    }
    
    return dispatch => {
        axios.get(url, {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`  
            },
        })
        .then(response=>{
          
            dispatch({type:ORDERS_COUNT,payload:response.data.data.paginate.total})
        }).catch(error=>{
           
        })
    }
}



