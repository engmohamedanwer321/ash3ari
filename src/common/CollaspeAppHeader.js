import React, { Component } from 'react';
import {
  View,TouchableOpacity,Text,ImageBackground,Animated, Platform
} from 'react-native';
import { Icon, Button,Badge } from 'native-base';
import { connect } from 'react-redux';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import {openSideMenu,push,pop} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from './AppFont'
import FastImage from 'react-native-fast-image'
import * as colors from '../assets/colors'


class CollaspeAppHeader  extends Component {
//openSideMenu(true,isRTL)
  

  render() {
    const {isRTL,title,back} = this.props;
    return (
      <View style={{justifyContent:'center', backgroundColor:colors.darkBlue,width:responsiveWidth(100),height:responsiveHeight(10),marginTop:Platform.OS ==='ios'? moderateScale(0) : moderateScale(0)}}>
         <View style={{alignItems:'center', flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',width:responsiveWidth(98),alignSelf:'center',marginTop:Platform.OS ==='ios'? moderateScale(5) : moderateScale(0)}}>
            
            {back?
            <Button  
             onPress={()=>{pop()}}
             style={{backgroundColor:colors.darkBlue,elevation:0,shadowOffset:{height:0,width:0}}} 
             >
               <Icon name={isRTL?'arrowright':'arrowleft'} type='AntDesign' style={{color:colors.white}} />
            </Button>
             :
             <Button 
             onPress={()=>{openSideMenu(true,isRTL)}}
            style={{backgroundColor:colors.darkBlue,elevation:0,shadowOffset:{height:0,width:0}}} >
                <Icon name='md-menu' type='Ionicons' style={{color:colors.white}} />
            </Button>
            }

            <Text style={{fontSize:responsiveFontSize(7), alignSelf:'center',fontFamily:isRTL?arrabicFont:englishFont,color:colors.white}}>{title} </Text>

            <View style={{width:responsiveWidth(10)}} /> 
        

         </View>  
      </View>
    );
  }
}


const mapStateToProps = state => ({
  isRTL: state.lang.RTL,
  color: state.lang.color,
  currentUser: state.auth.currentUser
});

export default connect(mapStateToProps)(CollaspeAppHeader );
