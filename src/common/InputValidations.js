import React, { Component } from 'react';


export default class GlobalFunctions extends React.Component {


    static validateEmail = (text) => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/;
        if (reg.test(text) === false) {
            return false;
        }
        else {
            return true;
        }
    }

}