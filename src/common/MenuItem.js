//
import React, { Component } from "react";
import { Text, Platform, View, TouchableOpacity } from "react-native";

import { connect } from "react-redux";
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Icon } from 'native-base';
import { arrabicFont, englishFont } from '../common/AppFont'
import * as colors from '../assets/colors'


const MyTouchableOpacity = withPreventDoubleClick(TouchableOpacity);

class MenuItem extends Component {
    //color={focused? '#ff6f61' : 'white'}
    renderNormal() {
        const { badge, type, text, focused, onPress, userType, isRTL, iconName, iconSize, badgeNumber } = this.props;

        return (
            <MyTouchableOpacity style={{ backgroundColor: "transparent", height: hp(5), justifyContent: 'center', marginTop: 2.5 }} onPress={onPress}>
                <View style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', width: wp(60), marginHorizontal: moderateScale(7), flexDirection: isRTL ? "row-reverse" : 'row', alignItems: 'center', justifyContent: 'space-between' }} >

                    <View style={{ flexDirection: isRTL ? "row-reverse" : 'row' }}>
                        <View style={{ width: wp(8), justifyContent: 'center', alignItems: 'center'}} >
                            <Icon style={{ color: 'white', fontSize: responsiveFontSize(8) }} name={iconName} type={type} />
                        </View>
                        <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(1), fontWeight: '600', textAlign: 'center', color: 'white' }}>{text}</Text>
                    </View>


                    {badge &&
                        <View style={{ justifyContent: 'center', alignItems: 'center', height: 20, width: 20, borderRadius: 10, backgroundColor: 'green' }}>
                            <Text style={{ fontFamily: englishFont, fontSize: responsiveFontSize(4), color: 'white' }}>{badgeNumber}</Text>
                        </View>
                    }

                </View>
            </MyTouchableOpacity>
        )
    }
    render() {
        return this.renderNormal();
    }
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(MenuItem);
