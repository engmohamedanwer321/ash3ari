import React,{Component} from 'react';
import { connect } from "react-redux";
import { setUser, userToken } from '../actions/AuthActions';
import firebase,{Notification } from 'react-native-firebase';
import { push } from '../controlls/NavigationControll';

class PushNotficationSys extends React.Component {


    ServerNotification(title, body) {
      
        let notification = new firebase.notifications.Notification();
        notification = notification.setNotificationId(new Date().valueOf().toString())
            .setTitle(title)
            .setBody(body)
            .setSound("bell.mp3")
            .setData({
                now: new Date().toISOString()
            });
        notification.ios.badge = 10
        //notification.android.setAutoCancel(true);  
        //notification.android.setBigPicture("https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_120x44dp.png", "https://image.freepik.com/free-icon/small-boy-cartoon_318-38077.jpg", "content title", "summary text")
        notification.android.setColor("#16476A")
        notification.android.setColorized(true)
        notification.android.setOngoing(true)
        //notification.android.setPriority(firebase.notifications.Android.Priority.High)
        notification.android.setSmallIcon("noti_icon")
        notification.android.setVibrate([300])
        //notification.android.addAction(new firebase.notifications.Android.Action("view", "noti_icon", "VIEW"))
        // notification.android.addAction(new firebase.notifications.Android.Action("reply", "ic_launcher", "REPLY").addRemoteInput(new firebase.notifications.Android.RemoteInput("input")) )
        notification.android.setChannelId("test-channel")

        firebase.notifications().displayNotification(notification)

    }

    checkNotificationPermision = ()=>{
        firebase.messaging().hasPermission()
        .then(enabled => {
            if (enabled) {
         
            } else {
               
            firebase.messaging().requestPermission()
            } 
        });  
    }

    getNotificationToken = ()=>{
        firebase.messaging().getToken().then(token => {
           
            this.checkToken(token);
          }).catch(error=>{
                  
           
          });

    }

    clickOnNotification = ()=>{
        firebase.notifications().onNotificationOpened((notificationOpen) => {
            firebase.notifications().removeDeliveredNotification(notificationOpen.notification._notificationId)
          
            
            push('Login')
            /*console.log("noti body   ",notificationOpen.notification)
            if(notificationOpen.action == 'android.intent.action.VIEW'){
                console.log("noti openmmmmmmmmmmahmoud")
                 push('Home')
            }*/
        });
    }

    showNotifcation = () => {
        firebase.notifications().onNotification((notification) => {
           
          
            //console.log(notification._title,notification._body)
            this.ServerNotification(notification._title,notification._body)
        });
    }


    componentDidMount(){  
         this.checkNotificationPermision()
         this.getNotificationToken()
         this.showNotifcation()
         this.clickOnNotification()
    }

    render(){
        return(null)
    }

}

const mapStateToProps = state => ({
    currentUser: state.auth.currentUser,
    userTokens: state.auth.userToken,
})

const mapDispatchToProps = {
    setUser,
    //changeLanguage,
    //userToken,
    //selectMenu,
}


export default connect(mapStateToProps,mapDispatchToProps)(PushNotficationSys)