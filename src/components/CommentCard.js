import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text, Modal } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import { push } from '../controlls/NavigationControll';
import Dialog, { DialogTitle, DialogContent } from 'react-native-popup-dialog';


class CommentCard extends Component {

    render() {
        const { isRTL, data } = this.props;
        return (
        <View>
        <Animatable.View
        style={{marginBottom:moderateScale(6), marginTop: moderateScale(0), alignSelf: 'center', width: responsiveWidth(96) }}
        animation={"slideInLeft"}
        duration={1500}
        >
            <View style={{flex:1,flexDirection:isRTL?'row-reverse':'row',}}>
                <Thumbnail source={require('../assets/imgs/profileicon.jpg')} />
                <View style={{marginHorizontal:moderateScale(3),marginTop:moderateScale(8)}}>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start', maxWidth: responsiveWidth(75), marginHorizontal:moderateScale(2), color: colors.sky, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{data.name}</Text>     
                </View>
            </View>
            <Text style={{ alignSelf:isRTL?'flex-end':'flex-start',marginHorizontal:moderateScale(3), maxWidth: responsiveWidth(90),color: colors.black, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{data.comment}</Text>     
        </Animatable.View>
        </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(CommentCard);
