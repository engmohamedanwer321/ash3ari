import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import { enableSideMenu, push } from '../controlls/NavigationControll'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'



const MyTouchableOpacity = withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class DeapartmentCard extends Component {



    render() {
        const { isRTL, navigator, data } = this.props;
        
        return (
            <TouchableOpacity
                onPress={() => push('SubCategories', data)}
              
                >
                <Animatable.View style={{ marginVertical:moderateScale(4), marginHorizontal: moderateScale(5), backgroundColor: 'white', borderTopRightRadius: moderateScale(2), borderTopLeftRadius: moderateScale(3), width: responsiveWidth(42),  shadowOffset: { height: 2, width: 0 }, shadowColor: 'black', shadowOpacity: 0.1, elevation: 2, alignSelf: 'center', alignItems: 'center', borderStyle: 'solid', borderWidth: 0.5, borderColor: colors.drakGray }}>
                    <FastImage
                        resizeMode='stretch'
                        style={{ borderTopRightRadius: moderateScale(2), borderTopLeftRadius: moderateScale(3), width: responsiveWidth(42), height: responsiveHeight(17), }}
                        source={{ uri: data.image }}
                    />

                    <Text style={{ maxWidth: responsiveWidth(40), marginTop: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), color: colors.black, textAlign: 'center' }} >{isRTL? data.name_ar : data.name_en}</Text>

                </Animatable.View>
            </TouchableOpacity>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(DeapartmentCard);
