import React,{Component} from 'react';
import {View,TouchableOpacity,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import { Thumbnail,Icon } from 'native-base';
import {arrabicFont,englishFont} from '../common/AppFont'


class DietCard extends Component {
    
    state = {
        show:false,
    }
    
  

    render(){
        const {noti,isRTL,navigator} = this.props;
        //const dateToFormat = () => <Moment date={date} />,
        return(
        <View style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1, elevation:15, marginTop:20, alignSelf:'center',justifyContent:'center', alignItems:'center'}}>          
            <TouchableOpacity style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1, width:responsiveWidth(96),height:responsiveHeight(15),elevation:15,alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                <View style={{backgroundColor:'#679C8A',justifyContent:'center',alignItems:'center',width:responsiveWidth(60),height:responsiveHeight(15)}}>
                    <Text style={{fontFamily:isRTL?arrabicFont:englishFont,  alignItems:'center',justifyContent:'center', color:'white',fontSize:responsiveFontSize(7),margin:moderateScale(5)}}>وجبة الافطار</Text>
                </View>
                <View style={{elevation:15, backgroundColor:'white',justifyContent:'center',alignItems:'center',width:responsiveWidth(36),height:responsiveHeight(15)}}>
                <Text style={{fontFamily:isRTL?arrabicFont:englishFont,  color:'black',fontSize:responsiveFontSize(7),margin:moderateScale(5)}}>الساعة 5.30 صباحا</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity
            onPress={()=>{this.setState({show:!this.state.show})}}
             style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,marginTop:moderateScale(-12),alignSelf:isRTL?'flex-start':'flex-end',elevation:20,backgroundColor:'#C7BE75', width:50,height:50,borderRadius:25,justifyContent:'center', alignItems:'center'}}>
            <Icon type='AntDesign' name={this.state.show?'arrowup':'arrowdown'} style={{color:'white',fontSize:responsiveFontSize(7)}} />  
            </TouchableOpacity>
            {this.state.show&&
            <View style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,justifyContent:'center',alignItems:'center', marginTop:moderateScale(-8),elevation:15,backgroundColor:'#679C8A',width:responsiveWidth(96),height:responsiveHeight(25),borderBottomLeftRadius:moderateScale(7),borderBottomRightRadius:moderateScale(7)}}>
                <Text style={{fontFamily:isRTL?arrabicFont:englishFont,  margin:moderateScale(6), color:'white'}}>شرب 2 كوب من الماء على غيار ريق ثم ناكل ويكون الاكل عبارة عن بيضة مسلوقة ونص رغيف عيش بلدى ومعلقتن جبنة بدون ملح عليهم زيزت زتون وكمون و2 كوب معايا بعد الفطار بساعتين</Text>

            </View>
            }
        </View>  
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
}


export default connect(mapStateToProps,mapDispatchToProps)(DietCard);
