import React, { Component } from "react";
import { View, ActivityIndicator, Text } from "react-native";
import { responsiveHeight, responsiveWidth, moderateScale,responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';

class LoadingDialogOverlay extends Component {

    render() {
        const { isRTL,title } = this.props
        return (
            <View style={{elevation:5, zIndex: 10000, position: 'absolute', bottom: 0, left: 0, right: 0, top: 0,backgroundColor: 'rgba(0,0,0,0.4)', alignItems:'center', justifyContent: 'center',minHeight:responsiveHeight(65), }} >
                <View style={{alignItems:'center', height:responsiveHeight(10), elevation: 90,backgroundColor: 'white', width: '80%', borderRadius: 10, flexDirection:isRTL?"row-reverse":'row', position:'absolute', bottom:moderateScale(120)}}>
                   <ActivityIndicator style={{marginHorizontal:moderateScale(8)}} color='black'/>
                   <View>
                    <Text style={{color:'black',fontSize:responsiveFontSize(6)}}>{title}</Text> 
                   </View>
                </View>
            </View>
            );
    }
}


const mapToStateProps = state => {
    return {
        isRTL: state.lang.RTL
    }
}
export default connect(mapToStateProps)(LoadingDialogOverlay);