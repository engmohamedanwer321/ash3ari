import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon,Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import {push} from '../controlls/NavigationControll'


const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class MarketCard extends Component {
  

    
    render(){
        const {isRTL,type,iconName,iconType,title,} = this.props;
        return(
        <TouchableOpacity onPress={()=>{
          const data ={
            type:type,
            title:title
          }
          push('RequestsAndOffers',data)
        }}> 
        <Animatable.View   style={{ flexDirection:isRTL?'row-reverse':'row',alignItems:'center', marginVertical:moderateScale(2), backgroundColor:'white',borderRadius:moderateScale(5),width:responsiveWidth(90),marginHorizontal:moderateScale(5),height:responsiveHeight(10),shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,elevation:4,marginTop:moderateScale(5), alignSelf:'center' }}>          
          
          <View style={{marginHorizontal:moderateScale(5)}}>
            <Icon name={iconName} type={iconType}  />
          </View>
        
        <Text style={{ fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(7),color:colors.black,textAlign:'center'}} >{title}</Text> 
         
        </Animatable.View>  
        </TouchableOpacity>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps,mapDispatchToProps)(MarketCard);
