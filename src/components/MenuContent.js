import React, { Component } from 'react';
import {
  View,
  Text,
  Alert,
  Image,
  ActivityIndicator,
  Platform,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Share,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { Button, Content, Icon, Thumbnail } from 'native-base';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions/MenuActions';
import { logout, loggedOut } from '../actions/LogoutActions';
import MenuItem from '../common/MenuItem';
import {
  responsiveHeight,
  responsiveWidth,
  moderateScale,
  responsiveFontSize,
} from '../utils/responsiveDimensions';
import * as colors from '../assets/colors';
import { rootNavigator } from '../screens/Login';
import Strings from '../assets/strings';
import { selectMenu } from '../actions/MenuActions';
import strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Dialog, { DialogContent, DialogTitle } from 'react-native-popup-dialog';
import { openSideMenu, push, resetTo } from '../controlls/NavigationControll';
import { arrabicFont, englishFont } from '../common/AppFont';
import { setUser } from '../actions/AuthActions';
import { BASE_END_POINT } from '../AppConfig';
import FastImage from 'react-native-fast-image'
import axios from 'axios';

const { height: SCREEN_HEIGHT } = Dimensions.get('window');
const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;

class MenuContent extends Component {
  state = {
    v: false,
    load: false,
    showLogoutDialog: false,
  };

  render() {
    const { item, currentUser, color, isRTL } = this.props;
    if (this.props.isRTL) {
      Strings.setLanguage('ar');
    } else {
      Strings.setLanguage('en');
    }


    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{
          flex: 1,
          backgroundColor: colors.sky,
          width: Platform.OS === 'ios' ? 310 : responsiveWidth(75),
          //flexDirection: isRTL ? 'row-reverse' : 'row',
        }}>

        {Platform.OS === 'ios' ?
          <TouchableOpacity onPress={() => openSideMenu(false, this.props.isRTL)} >
            <Icon name='close' type='AntDesign' style={{ color: 'white', position: 'absolute', left: isRTL ? moderateScale(10) : null, right: isRTL ? null : moderateScale(20) }} />
          </TouchableOpacity>
          :
          <TouchableOpacity onPress={() => openSideMenu(false, this.props.isRTL)}
            style={{ width: responsiveWidth(10), height: responsiveWidth(10), position: 'absolute', left: isRTL ? 20 : null, right: isRTL ? null : 0, top: 20 }}>
            <Icon name='close' type='AntDesign' style={{ color: 'white', }} />
          </TouchableOpacity>
        }


        <View
          style={{
            alignSelf: 'center',
            marginTop: moderateScale(2),
            justifyContent: 'center',
            alignItems: 'center',
          }}>




          <View
            style={{
              marginTop: hp(5),
              alignSelf: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}>





            <FastImage
              style={{ borderWidth: 3, borderColor: 'white', width: responsiveWidth(28), height: responsiveWidth(28), borderRadius: responsiveWidth(14) }}

              source={
                currentUser && currentUser.data.image
                  ? { uri: currentUser.data.image }
                  : require('../assets/imgs/profileicon.jpg')
              }
            />
          </View>

          {currentUser && (
            <View
              style={{
                marginTop: moderateScale(4),
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: isRTL ? arrabicFont : englishFont,
                  color: 'white',
                  fontSize: responsiveFontSize(8),
                }}>
                {currentUser.data.name}
              </Text>
            </View>
          )}

          {currentUser && (
            <View
              style={{
                marginTop: moderateScale(2),
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: isRTL ? arrabicFont : englishFont,
                  color: 'white',
                  fontSize: responsiveFontSize(6),
                }}>
                {currentUser.data.phone}
              </Text>
            </View>
          )}
        </View>

        <View
          style={{
            marginTop: moderateScale(5),
            width: responsiveWidth(70),
            height: 5,
            alignSelf: 'center',
            backgroundColor: 'white',
            borderRadius: 3,
          }}
        />

        <View
          showsVerticalScrollIndicator={false}
          //style={{ marginTop: hp(4),marginHorizontal : isRTL? Platform.OS ==='ios' ? (IS_IPHONE_X ? 35 :8) : 0 :  Platform.OS ==='ios' ? (IS_IPHONE_X ? -15 :-15) : 0}}>
          style={{
            marginTop: hp(4),
            marginHorizontal: isRTL
              ? Platform.OS === 'ios'
                ? moderateScale(6)
                : moderateScale(-6)
              : Platform.OS === 'ios'
                ? moderateScale(-6)
                : moderateScale(-7),
          }}>
          <View style={{ marginBottom: hp(3) }}>
            {currentUser && (
              <MenuItem
                onPress={() => {
                  //this.props.currentUser
                  if (currentUser) {
                    if (item[item.length - 1] == 'Home') {
                      openSideMenu(false, this.props.isRTL);
                    } else {
                      openSideMenu(false, this.props.isRTL);
                      this.props.selectMenu('HOME');
                      push('Home');
                    }
                  } else {
                    RNToasty.Warn({ title: Strings.checkUser });
                  }
                }}
                focused={item[item.length - 1] == 'HOME'}
                iconName="home"
                type="FontAwesome5"
                text={Strings.home}
              />
            )}



            {currentUser && (
              <MenuItem
                onPress={() => {
                  if (currentUser) {
                    if (item[item.length - 1] == 'NOTIFICATIONS') {
                      openSideMenu(false, this.props.isRTL);
                    } else {
                      openSideMenu(false, this.props.isRTL);
                      this.props.selectMenu('NOTIFICATIONS');
                      push('Notifications');
                    }
                  } else {
                    RNToasty.Warn({ title: Strings.checkUser });
                  }
                }}
                focused={item[item.length - 1] == 'NOTIFICATIONS'}
                badge
                badgeNumber={this.props.unReadNoti}
                iconName="bell"
                type="MaterialCommunityIcons"
                text={Strings.notifications}
              />
            )}

            {currentUser && (
              <MenuItem
                onPress={() => {
                  if (currentUser) {
                    if (item[item.length - 1] == 'ORDERS') {
                      openSideMenu(false, this.props.isRTL);
                    } else {
                      openSideMenu(false, this.props.isRTL);
                      this.props.selectMenu('ORDERS');
                      if (currentUser.data.role == 'provider') {
                        push('ProviderOrders');
                      } else {
                        push('Orders');
                      }
                    }
                  } else {
                    RNToasty.Warn({ title: Strings.checkUser });
                    push('Login');
                  }
                }}
                focused={item[item.length - 1] == 'ORDERS'}
                badge
                badgeNumber={this.props.ordersCount}
                iconName="file-document-edit-outline"
                type="MaterialCommunityIcons"
                text={Strings.myOrders}
              />
            )}

            {currentUser && currentUser.data.role == 'client' && (
              <MenuItem
                onPress={() => {
                  //this.props.currentUser
                  if (currentUser) {
                    if (item[item.length - 1] == 'MYACCOUNT') {
                      openSideMenu(false, this.props.isRTL);
                    } else {
                      openSideMenu(false, this.props.isRTL);
                      this.props.selectMenu('MYACCOUNT');
                      push('MyAccount');
                    }
                  } else {
                    RNToasty.Warn({ title: Strings.checkUser });
                  }
                }}
                focused={item[item.length - 1] == 'PROFILE'}
                iconName="user-alt"
                type="FontAwesome5"
                text={Strings.myAccount}
              />
            )}

            {currentUser &&
              currentUser.data.role == 'provider' && [
                <MenuItem
                  onPress={() => {
                    if (currentUser) {
                      if (item[item.length - 1] == 'PROFILE') {
                        openSideMenu(false, this.props.isRTL);
                      } else {
                        openSideMenu(false, this.props.isRTL);
                        this.props.selectMenu('PROFILE');
                        push('ServiceProviderProfile');
                      }
                    } else {
                      RNToasty.Warn({ title: Strings.checkUser });
                    }
                  }}
                  focused={item[item.length - 1] == 'PROFILE'}
                  iconName="google-pages"
                  type="MaterialCommunityIcons"
                  text={Strings.profile}
                />,
                <MenuItem
                  onPress={() => {
                    if (currentUser) {
                      if (item[item.length - 1] == 'ADDNEWPROJECT') {
                        openSideMenu(false, this.props.isRTL);
                      } else {
                        openSideMenu(false, this.props.isRTL);
                        this.props.selectMenu('ADDNEWPROJECT');
                        push('AddNewProject');
                      }
                    } else {
                      RNToasty.Warn({ title: Strings.checkUser });
                    }
                  }}
                  focused={item[item.length - 1] == 'ADDNEWPROJECT'}
                  iconName="google-pages"
                  type="MaterialCommunityIcons"
                  text={Strings.addNewProject}
                />,
                <MenuItem
                  onPress={() => {
                    if (currentUser) {
                      if (item[item.length - 1] == 'PROVIDERPROJECTS') {
                        openSideMenu(false, this.props.isRTL);
                      } else {
                        openSideMenu(false, this.props.isRTL);
                        this.props.selectMenu('PROVIDERPROJECTS');
                        push('ProviderProjects');
                      }
                    } else {
                      RNToasty.Warn({ title: Strings.checkUser });
                    }
                  }}
                  focused={item[item.length - 1] == 'PROVIDERPROJECTS'}
                  iconName="google-pages"
                  type="MaterialCommunityIcons"
                  text={Strings.myProjects}
                />,
              ]}


            {currentUser && currentUser.data.role == 'provider' && (
              <MenuItem
                onPress={() => {
                  if (currentUser) {
                    if (item[item.length - 1] == 'UPDSERVICEPROFILE') {
                      openSideMenu(false, this.props.isRTL);
                    } else {
                      openSideMenu(false, this.props.isRTL);
                      this.props.selectMenu('UPDSERVICEPROFILE');
                      push('UpdateServiceProviderProfile');
                    }
                  } else {
                    RNToasty.Warn({ title: Strings.checkUser });
                  }
                }}
                focused={item[item.length - 1] == 'UPDSERVICEPROFILE'}
                iconName="edit"
                type="FontAwesome"
                text={Strings.updateProfile}
              />
            )}


            {currentUser && (
              <MenuItem
                onPress={() => {
                  if (currentUser) {
                    if (item[item.length - 1] == 'PAYMENT') {
                      openSideMenu(false, this.props.isRTL);
                    } else {
                      openSideMenu(false, this.props.isRTL);
                      this.props.selectMenu('PAYMENT');
                      push('Payment');
                    }
                  } else {
                    RNToasty.Warn({ title: Strings.checkUser });
                  }
                }}
                focused={item[item.length - 1] == 'PAYMENT'}
                iconName={
                  currentUser.data.role == 'provider' ? 'money' : 'price-ribbon'
                }
                type={
                  currentUser.data.role == 'provider' ? 'FontAwesome' : 'Entypo'
                }
                text={
                  currentUser.data.role == 'provider'
                    ? Strings.thePay
                    : Strings.memberPromotion
                }
              />
            )}



            {/*true&&
            <MenuItem
            onPress={()=>{
            if(true){
               if(item[item.length-1]=="CHAT"){
                openSideMenu(false)
               }else{
                openSideMenu(false)
                this.props.selectMenu('CHAT');
                 push("ChatPeople")
               }
            }else{
             RNToasty.Warn({title:Strings.checkUser})
            }
             
           }}
           focused={item[item.length-1] == 'CHAT'} badge
           iconName='email' type='MaterialCommunityIcons' text={Strings.messages}/>
          */}

            {/*currentUser &&
              <MenuItem
                onPress={() => {
                  if (currentUser) {
                    if (item[item.length - 1] == "ADDREQANDOFF") {
                      openSideMenu(false, this.props.isRTL)
                    } else {
                      openSideMenu(false, this.props.isRTL)
                      this.props.selectMenu('ADDREQANDOFF');
                      push("AddRequestAndOffer")
                      
                    }
                  } else {
                    RNToasty.Warn({ title: Strings.checkUser })
                  }

                }}
                focused={item[item.length - 1] == 'ADDREQANDOFF'}
                iconName='local-offer' type='MaterialIcons' text={Strings.addRequestAndOffer} />*/}

            {!currentUser && (
              <MenuItem
                onPress={() => {
                  if (item[item.length - 1] == 'LOGIN') {
                    openSideMenu(false, this.props.isRTL);
                  } else {
                    openSideMenu(false, this.props.isRTL);
                    this.props.selectMenu('LOGIN');
                    push('Login');
                  }
                }}
                focused={item[item.length - 1] == 'CONTACTUS'}
                iconName="sign-in"
                type="FontAwesome"
                text={Strings.login}
              />
            )}


            <MenuItem
              onPress={() => {
                if (item[item.length - 1] == 'SHAREAPP') {
                  openSideMenu(false, this.props.isRTL);
                } else {
                  //openSideMenu(false, this.props.isRTL);
                  //this.props.selectMenu('CHANGELANGUAGE');
                  Share.share(
                    {
                      message:
                        Platform.OS === 'android'
                          ? 'https://play.google.com/store/apps/details?id=com.ash3arimobileapp'
                          : 'https://apps.apple.com/us/app/id1523239962',
                    },
                    {
                      // Android only:
                      dialogTitle: 'Share',
                      // iOS only:
                      excludedActivityTypes: [
                        'com.apple.UIKit.activity.PostToTwitter',
                      ],
                    },
                  );
                }
              }}
              focused={item[item.length - 1] == 'SHAREAPP'}
              iconName="share"
              type="Entypo"
              text={Strings.shareOurApp}
            />




            {/*s<TouchableOpacity
              onPress={() => {
                Share.share(
                  {
                    message:
                      Platform.OS === 'android'
                        ? 'https://play.google.com/store/apps/details?id=com.ash3arimobileapp'
                        : 'https://apps.apple.com/us/app/id1523239962',
                  },
                  {
                    // Android only:
                    dialogTitle: 'Share',
                    // iOS only:
                    excludedActivityTypes: [
                      'com.apple.UIKit.activity.PostToTwitter',
                    ],
                  },
                );
              }}
              style={{
                flexDirection: isRTL ? 'row-reverse' : 'row',
                alignItems: 'center',
                marginTop: 2.5,
              }}>
              <View
                style={{
                  alignSelf: isRTL ? 'flex-end' : 'flex-start',
                  width: wp(60),
                  marginHorizontal: moderateScale(7),
                  flexDirection: isRTL ? 'row-reverse' : 'row',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: wp(8),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    style={{ fontSize: responsiveFontSize(8), color: 'white' }}
                    name="share"
                    type="Entypo"
                  />
                </View>
                <Text
                  style={{
                    fontFamily: isRTL ? arrabicFont : englishFont,
                    marginHorizontal: moderateScale(5),
                    fontSize: responsiveFontSize(7),
                    fontWeight: '500',
                    color: 'white',
                  }}>
                  {Strings.shareOurApp}
                </Text>
              </View>
            </TouchableOpacity>*/}

            <MenuItem
              onPress={() => {
                if (item[item.length - 1] == 'CHANGELANGUAGE') {
                  openSideMenu(false, this.props.isRTL);
                } else {
                  openSideMenu(false, this.props.isRTL);
                  this.props.selectMenu('CHANGELANGUAGE');
                  push('SelectLanguage');
                }
              }}
              focused={item[item.length - 1] == 'CHANGELANGUAGE'}
              iconName="web"
              type="MaterialCommunityIcons"
              text={Strings.changeLanguage}
            />

            {currentUser && (
              <MenuItem
                onPress={() => {
                  if (item[item.length - 1] == 'CONTACTUS') {
                    openSideMenu(false, this.props.isRTL);
                  } else {
                    openSideMenu(false, this.props.isRTL);
                    this.props.selectMenu('CONTACTUS');
                    push('ContactUs');
                  }
                }}
                focused={item[item.length - 1] == 'CONTACTUS'}
                iconName="contact-mail"
                type="MaterialIcons"
                text={Strings.contactUS}
              />
            )}

            {currentUser &&
              <MenuItem
                onPress={() => {
                  if (item[item.length - 1] == 'CONTACTUS') {
                    openSideMenu(false, this.props.isRTL);
                  } else {
                    openSideMenu(false, this.props.isRTL);
                    this.props.selectMenu('CONTACTUS');
                    axios
                      .get(`${BASE_END_POINT}user/logout`, {
                        headers: {
                          Authorization: `Bearer ${this.props.currentUser.data.token}`,
                        },
                      })
                      .then((response) => {

                        openSideMenu(false, this.props.isRTL);
                        AsyncStorage.removeItem('USER');


                       // RNToasty.Success({ title: Strings.signedOutSuccess })
                        this.props.loggedOut()
                        resetTo('Home');
                        this.props.setUser(null);
                      })
                      .catch((error) => {
                        console.log(error)
                      });
                  }
                }}
                focused={item[item.length - 1] == 'CONTACTUS'}
                iconName="logout"
                type="MaterialCommunityIcons"
                text={Strings.logout}
              />
            }


            {currentUser && (
              <></>
              // this.props.logout(this.props.userToken,this.props.currentUser.token,this.props.navigator)
              /*<TouchableOpacity
                onPress={() => {
                  axios
                    .get(`${BASE_END_POINT}user/logout`, {
                      headers: {
                        Authorization: `Bearer ${this.props.currentUser.data.token}`,
                      },
                    })
                    .then((response) => {

                      openSideMenu(false, this.props.isRTL);
                      AsyncStorage.removeItem('USER');


                      RNToasty.Success({ title: Strings.signedOutSuccess })

                      resetTo('SplashScreen');
                      this.props.setUser(null);
                    })
                    .catch((error) => {

                    });

                  // this.props.logout('token',this.props.currentUser.token,this.props.navigator)
                }}
                style={{
                  marginBottom: moderateScale(5),
                  height: hp(5),
                  marginTop: 2.5,
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    alignSelf: isRTL ? 'flex-end' : 'flex-start',
                    width: wp(60),
                    marginHorizontal: moderateScale(7),
                    flexDirection: isRTL ? 'row-reverse' : 'row',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      width: wp(8),
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon
                      style={{ color: 'white', fontSize: responsiveFontSize(10) }}
                      name="logout"
                      type="MaterialCommunityIcons"
                    />
                  </View>
                  <Text
                    style={{
                      fontFamily: isRTL ? arrabicFont : englishFont,
                      fontSize: responsiveFontSize(7),
                      fontWeight: '500',
                      marginHorizontal: moderateScale(5),
                      color: 'white',
                    }}>
                    {Strings.logout}
                  </Text>
                </View>
              </TouchableOpacity>*/
            )}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  item: state.menu.item,
  isRTL: state.lang.RTL,
  currentUser: state.auth.currentUser,
  userToken: state.auth.userToken,
  unReadNoti: state.noti.unReaded,
  ordersCount: state.orderCount.ordersCount,
});

const mapDispatchToProps = {
  selectMenu,
  logout,
  setUser,
  loggedOut
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuContent);
// MenuContent
// connect(mapStateToProps, mapDispatchToProps, )(MenuContent);
