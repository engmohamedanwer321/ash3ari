import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text, Modal } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import { push } from '../controlls/NavigationControll';
import Dialog, { DialogTitle, DialogContent } from 'react-native-popup-dialog';



const MyTouchableOpacity = withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class NotificationCard extends Component {

    state = {
        showDialog: false
    }

    markAsReadNotification = (noti_id) => {

        var uri = ''
        if (this.props.currentUser.data.role == 'client') {
            uri = `${BASE_END_POINT}client/showNotification/${noti_id}`
        } else {
            // Alert.alert("p")
            uri = `${BASE_END_POINT}provider/showNotification/${noti_id}`
        }
        axios.get(uri, {
            headers: {
                Authorization: `Bearer ${this.props.currentUser.data.token}`
            }
        })
            .then(response => {

                this.props.getUnreadNotificationsNumers(this.props.currentUser.data.token)

            })
            .catch(error => {

                this.setState({ notifications404: true, notificationsLoad: false, })
            })
    }


    notificationDetailsDialog = () => {
        const { isRTL, currentUser, data } = this.props;

        return (
            <Dialog
                visible={this.state.showDialog}
                onTouchOutside={() => this.setState({ showDialog: false })}
                onHardwareBackPress={() => this.setState({ showDialog: false })}
            //dialogTitle={<DialogTitle title="Dialog Title" />}
            >
                <View style={{ width: responsiveWidth(90), height: responsiveHeight(50) }}>
                   


                    <View style={{position:'relative', backgroundColor: colors.darkBlue, justifyContent: 'center', alignItems: 'center', width: responsiveWidth(90), height: responsiveHeight(7) }}>
                    <TouchableOpacity onPress={()=>this.setState({showDialog:false})}
                     style={{position:'absolute',left: isRTL ? moderateScale(5) : null, right: isRTL ? null : moderateScale(5), width:responsiveWidth(8),height:responsiveWidth(8), alignItems:'center',justifyContent:'center' }}>
                        <Icon name='close' type='AntDesign' style={{ color: 'white',fontSize:responsiveFontSize(8) }} />
                    </TouchableOpacity>

                        <Text style={{ fontFamily: englishFont, fontSize: responsiveFontSize(6), color: 'white', textAlign: 'center', marginHorizontal: moderateScale(0) }} >{Strings.notificationDetails}</Text>
                    </View>


                    <View style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => {/*push('ServiceProviderProfile',data.order.provider)*/ }} >
                            <Thumbnail small source={{ uri: data.provider_image }} />
                        </TouchableOpacity>
                        <View style={{ width: responsiveWidth(70), marginHorizontal: moderateScale(3), marginTop: moderateScale(5), alignSelf: 'center', justifyContent: 'center' }}>

                            <Text style={{ textAlign: 'center', color: colors.darkBlue, fontSize: responsiveFontSize(7), fontFamily: isRTL ? arrabicFont : englishFont }}>{data.order.provider.name}</Text>

                        </View>
                    </View>
                    <View style={{ width: responsiveWidth(80), alignSelf: 'center', marginVertical: moderateScale(6) }}>
                        <Text style={{ textAlign: isRTL ? 'right' : 'left', color: colors.black, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{data.title}</Text>
                    </View>


                </View>
            </Dialog>
        )
    }

    render() {
        const { isRTL, currentUser, data } = this.props;

        moment.locale(this.props.isRTL ? 'ar' : 'en');

        console.log('FFFF:', data.provider_image)
        return (
            <Animatable.View
                animation={"slideInLeft"}
                duration={1500}
            >
                <TouchableOpacity style={{ width: responsiveWidth(96), alignSelf: 'center', marginVertical: moderateScale(2), elevation: 4, zIndex: 10000, backgroundColor: 'white', borderRadius: moderateScale(5), shadowOffset: { height: 0, width: 2 }, shadowColor: 'black', shadowOpacity: 0.2, }} activeOpacity={1} onPress={() => [this.setState({ showDialog: true }), this.markAsReadNotification(data.id)]} >
                    <View
                        style={{ marginVertical: moderateScale(2), alignSelf: 'center', width: responsiveWidth(96) }}
                    >

                        <View style={{ alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(96), paddingHorizontal: moderateScale(2) }}>
                            <TouchableOpacity onPress={() => {/*push('ServiceProviderProfile',data.order.provider)*/ }} >
                                <Thumbnail source={{ uri: data.provider_image }} />
                            </TouchableOpacity>
                            <View style={{ width: responsiveWidth(77), marginHorizontal: moderateScale(3) }}>
                                <Text style={{ alignSelf: isRTL ? 'flex-start' : 'flex-end', fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.lightGray }}>{moment(data.created_at.date).fromNow()}</Text>
                                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.darkBlue, fontSize: responsiveFontSize(7), fontFamily: isRTL ? arrabicFont : englishFont }}>{data.order.provider.name}</Text>
                                {/*<Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: colors.lightGray, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{data.order.provider.job}</Text>*/}
                            </View>
                        </View>
                        <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-between', width: responsiveWidth(90), alignSelf: 'center', marginBottom: moderateScale(4) }}>
                            <View style={{ width: responsiveWidth(9) }} />
                            <View style={{ borderRadius: moderateScale(3), backgroundColor: colors.darkBlue, alignItems: 'center', justifyContent: 'center', }}>
                                <Text style={{ borderRadius: moderateScale(3), textAlign: 'center', width: responsiveWidth(60), padding: moderateScale(3), color: 'white', fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, }}>{data.title}</Text>
                            </View>
                            <FastImage
                                source={{ uri: data.notification_status }}
                                style={{ width: responsiveWidth(8), height: responsiveHeight(9) }}
                            />
                        </View>



                    </View>
                    {this.notificationDetailsDialog()}
                </TouchableOpacity>
            </Animatable.View>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(NotificationCard);
