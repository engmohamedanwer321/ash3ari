import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon,Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'



const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class PageTitleLine extends Component {
 
    render(){
        const {isRTL,title,iconName,iconType} = this.props;
        return(
       <View style={{alignSelf:'center',width:responsiveWidth(96),marginTop:moderateScale(10),borderBottomColor:colors.lightGray,borderBottomWidth:1}}>
           <View  style={{marginBottom:moderateScale(2), alignSelf:isRTL?'flex-end':'flex-start', flexDirection:isRTL?'row-reverse':'row',alignItems:'center',marginHorizontal:moderateScale(5)}}>
                <Icon name={iconName} type={iconType} style={{fontSize:responsiveFontSize(8), color:'gray'}} />
                <Text style={{fontSize:responsiveFontSize(7), marginHorizontal:moderateScale(3), color:'gray',fontFamily:isRTL?arrabicFont:englishFont}}>{title}</Text>
            </View>
            <View style={{alignSelf:isRTL?'flex-end':'flex-start', width:responsiveWidth(30),backgroundColor:'green',height:2,}} />
       </View>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps,mapDispatchToProps)(PageTitleLine);
