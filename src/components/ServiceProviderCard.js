import React, { Component } from 'react';
import { View, Alert, TouchableOpacity, Text, Image, Platform } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail, Icon, Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import { getUnreadNotificationsNumers } from '../actions/NotificationAction'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import { Rating, AirbnbRating } from 'react-native-ratings';
import { push } from '../controlls/NavigationControll'

//moment(birth_date).format("YYYY-MM-DD")


class ServiceProviderCard extends Component {


    serviceProviderImageSection = () => {
        const { currentUser, data, isRTL } = this.props
        return (
            <>
                {currentUser &&
                    (data.is_special == 1 || data.is_special == '1') &&
                    <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#CEDCE4', borderRadius: moderateScale(4), marginBottom: moderateScale(5), width: responsiveWidth(20), height: responsiveHeight(4), marginHorizontal: moderateScale(0), zIndex: 100000, alignSelf: 'center', elevation: 2, shadowOffset: { height: 2, width: 0 }, shadowColor: 'black', shadowOpacity: 0.2 }} >
                        <Text style={{ fontSize: responsiveFontSize(4), alignSelf: 'center', marginHorizontal: moderateScale(2), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.discount} {data.discount ? data.discount : '0'}%</Text>
                    </View>
                }

                <View style={{ borderWidth: 5, borderColor: 'green', width: 70, height: 70, borderRadius: 35, justifyContent: 'center', alignItems: 'center', marginLeft: moderateScale(3), alignSelf: 'center', elevation: 2, shadowOffset: { height: 2, width: 0 }, shadowColor: 'black', shadowOpacity: 0.2, zIndex: 10000 }} >

                    {Number(data.is_special) == 1 &&
                        <View style={{ elevation: 2, shadowOffset: { height: 2, width: 0 }, shadowColor: 'black', shadowOpacity: 0.2, elevation: 20000, position: 'absolute', right: moderateScale(-4), top: moderateScale(0), zIndex: 10000 }} >
                            <Icon name='star' type='FontAwesome' style={{ color: 'gold', fontSize: responsiveFontSize(11) }} />
                        </View>
                    }
                    <FastImage
                        style={{ backgroundColor: 'white', width: 60, height: 60, borderRadius: 30, borderWidth: 2, borderColor: 'white', elevation: 2, alignSelf: 'center', elevation: 2, shadowOffset: { height: 2, width: 0 }, shadowColor: 'black', shadowOpacity: 0.2 }}
                        source={{ uri: data.image }}
                    />
                </View>
            </>
        )
    }

    render() {
        const { isRTL, data, currentUser } = this.props;
  
        return (
            <View>
                <Animatable.View animation={"flipInY"} duration={1000} style={{ width: responsiveWidth(47), borderWidth: 0, alignSelf: 'center', marginHorizontal: moderateScale(2), marginVertical: moderateScale(5) }} >
                    <TouchableOpacity onPress={() => push('ServiceProviderPage', data)}
                    >

                        {Platform.OS === 'ios' ?
                            <View style={{ flexDirection: 'row', alignSelf: 'center', zIndex: 1000 }}>
                                {this.serviceProviderImageSection()}
                            </View>
                            :
                            <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                {this.serviceProviderImageSection()}
                            </View>
                        }

                        <View style={{ width: responsiveWidth(46), alignSelf: 'center', borderRadius: moderateScale(4), marginTop: moderateScale(-8), backgroundColor: '#CEDCE4', alignSelf: 'center', elevation: 2, shadowOffset: { height: 2, width: 0 }, shadowColor: 'black', shadowOpacity: 0.2 }}>
                            <Text style={{ width: responsiveWidth(45), textAlign: 'center', fontSize: responsiveFontSize(4.8), alignSelf: 'center', marginTop: moderateScale(10), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{data.name}</Text>
                            <View style={{ marginTop: moderateScale(3), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', alignSelf: 'center', marginHorizontal: moderateScale(6) }} >
                                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', alignSelf: 'center', marginHorizontal: moderateScale(2) }}>
                                    <Icon name='map-marker' type='FontAwesome' style={{ color: colors.darkBlue, fontSize: responsiveFontSize(6) }} />
                                    <Text style={{ fontSize: responsiveFontSize(4), alignSelf: 'center', marginHorizontal: moderateScale(2), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{data.city_name}</Text>
                                </View>
                                <View style={{ width: 2, height: 15, backgroundColor: colors.darkBlue, marginHorizontal: moderateScale(2) }} />
                                <Text style={{ fontSize: responsiveFontSize(4), alignSelf: 'center', marginHorizontal: moderateScale(2), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{data.provider_type == 'one' ? Strings.person : Strings.company} </Text>
                            </View>

                            <View style={{ marginTop: moderateScale(3), alignSelf: 'center' }}>
                                <AirbnbRating
                                    isDisabled
                                    showRating={false}
                                    starContainerStyle={{ margin: 0 }}
                                    style={{ paddingVertical: 0 }}
                                    count={5}
                                    defaultRating={data.rate}
                                    size={12}
                                />
                            </View>

                            <Button onPress={() => { push('ServiceProviderPage', data) }} style={{ alignSelf: 'center', marginTop: moderateScale(2), marginBottom: moderateScale(5), height: responsiveHeight(5), width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(3) }} >
                                <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.details}</Text>
                            </Button>


                        </View>


                    </TouchableOpacity>
                </Animatable.View>
            </View>

        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps, mapDispatchToProps)(ServiceProviderCard);
