import React,{Component} from 'react';
import {View,Animated,TouchableOpacity,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';



const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class VisitCard extends Component {
    
    state = {
        show:false,
    }

    item = (key,value) => {
    const {isRTL,} = this.props;
      return(
        <View style={{marginTop:moderateScale(2),flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center',width:responsiveWidth(96),height:responsiveHeight(7),borderBottomColor:'white',borderBottomWidth:0.3}} >
            <Text style={{fontFamily:isRTL?arrabicFont:englishFont, marginHorizontal:moderateScale(7), fontSize:responsiveFontSize(7),fontWeight:'600',color:'#f5f681'}}>{key}</Text>
            <Text style={{fontFamily:isRTL?arrabicFont:englishFont, marginHorizontal:moderateScale(7), color:'white'}}>{value}</Text>
        </View>
      )
    }
    
    render(){
        const {isRTL,navigator,data} = this.props;
        return(
            <Animatable.View animation={"flipInY"} duration={1500} style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,elevation:15, marginTop:20, alignSelf:'center',justifyContent:'center', alignItems:'center'}}>          
            <TouchableOpacity style={{ width:responsiveWidth(96),height:responsiveHeight(15),elevation:15,alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                <View style={{backgroundColor:'#679C8A',justifyContent:'center',alignItems:'center',width:responsiveWidth(30),height:responsiveHeight(15)}}>
                    <Text style={{fontFamily:isRTL?arrabicFont:englishFont, alignItems:'center',justifyContent:'center', color:'white',fontSize:responsiveFontSize(7),margin:moderateScale(5)}}>{Strings.visitDay}</Text>
                </View>
                <View style={{elevation:15, backgroundColor:'white',justifyContent:'center',alignItems:'center',width:responsiveWidth(66),height:responsiveHeight(15)}}>
                 <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'black',fontSize:responsiveFontSize(7),margin:moderateScale(5)}}>{data.visit_date}</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
            onPress={()=>{this.setState({show:!this.state.show})}}
             style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,marginTop:moderateScale(-12),alignSelf:isRTL?'flex-start':'flex-end',elevation:20,backgroundColor:'#C7BE75', width:50,height:50,borderRadius:25,justifyContent:'center', alignItems:'center'}}>
            <Icon type='AntDesign' name={this.state.show?'arrowup':'arrowdown'} style={{color:'white',fontSize:responsiveFontSize(8)}} />  
            </TouchableOpacity>

            {this.state.show&&
            <Animatable.View animation={"lightSpeedIn"} duration={1500} style={{shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1, marginTop:moderateScale(-8),elevation:15,backgroundColor:'#679C8A',width:responsiveWidth(96),borderBottomLeftRadius:moderateScale(7),borderBottomRightRadius:moderateScale(7)}}>
               {this.item(Strings.weight,data.weight?data.weight:Strings.noDataAtRecent)}
               {this.item(Strings.height,data.height?data.height:Strings.noDataAtRecent)}
               {this.item(Strings.salt,data.salt?data.salt:Strings.noDataAtRecent)}
               {this.item(Strings.protein,data.protein?data.protein:Strings.noDataAtRecent)}
               {this.item(Strings.water,data.water?data.water:Strings.noDataAtRecent)}
               {this.item(Strings.fat,data.fat?data.fat:Strings.noDataAtRecent)}
               {this.item(Strings.bmi,data.bmi?data.bmi:Strings.noDataAtRecent)} 
               
               <View style={{marginTop:moderateScale(5),width:responsiveWidth(96)}}>
                <Text style={{fontFamily:isRTL?arrabicFont:englishFont, alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(7), fontSize:responsiveFontSize(8),fontWeight:'600',color:'#f5f681'}}>{Strings.notes}</Text>
                <Text style={{fontFamily:isRTL?arrabicFont:englishFont, marginVertical:moderateScale(6), alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(7), color:'white'}}>{data.note?data.note:Strings.noDataAtRecent}</Text>
               </View>  
            </Animatable.View>
            }
        </Animatable.View>  
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps,mapDispatchToProps)(VisitCard);
