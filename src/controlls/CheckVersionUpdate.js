import React, {useEffect} from 'react'
import {Alert} from 'react-native'
import VersionCheck from 'react-native-version-check';

export const CheckVersionUpdate = ()=>{
    useEffect(() => {
        checkUpdateNeeded();
    }, []);
}

const checkUpdateNeeded = async () => {
    let updateNeeded = await VersionCheck.needUpdate();
    if (updateNeeded.isNeeded) {
        Alert.alert('Check Your Application for Update')
        //Alert the user and direct to the app url
    }
}