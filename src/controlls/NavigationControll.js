import React, { Component } from 'react';
import { Navigation } from "react-native-navigation";
import { Provider } from 'react-redux';
import store from '../store';


export const openSideMenu = (open, lang) => {
 

  if (lang) {
    Navigation.mergeOptions('slideMenu', {
      sideMenu: {
        right: {
          visible: open ? lang ? true : false : false
        }
        
      }
    });
  } else {
    Navigation.mergeOptions('slideMenu', {
      sideMenu: {
        left: {
          visible: open ? lang ? false : true : false
        }
      
      }
    });
  }


}

export const HOC = (Component) => {
  return (props) => (
    <Provider store={store}>
      <Component {...props} />
    </Provider>
  );
}

export const enableSideMenu = (enable, lang) => {


    Navigation.mergeOptions('slideMenu', {
      sideMenu: {
        right: {
          enabled: enable? lang ? true : false : false,
        },
      },
    });
    
 

  Navigation.mergeOptions('slideMenu', {
    sideMenu: {
      left: {
        enabled: enable  ? lang ? false : true : false,
      }
    },
  });

}

export const push = (pageName, data) => {
  Navigation.push('AppStack', {
    component: {
      name: pageName,
      passProps: {
        data: data ? data : null
      }
    }
  })
}

export const pop = () => {
  Navigation.pop('AppStack');
}

export const resetTo = (pageName) => {
  Navigation.setStackRoot('AppStack', [
    {
      component: {
        name: pageName,
        /*options: {
          animations:{
            push:{
              enabled: 'true' ,
              content:{
              y: {
                from: 4000,
                to: 0,
                startDelay: 0,
                duration: 10000,
                interpolation: 'decelerate'
              }
            }
            }
          }
        }*/
      }
    }
  ]);
}

//resetTo



