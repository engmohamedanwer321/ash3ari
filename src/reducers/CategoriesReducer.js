import {
    GET_ADS_CATEGORIES
} from '../actions/types';

import {
    DataProvider,
} from 'recyclerlistview';


const initState = {
    loading: false,
    notifications: new DataProvider(),
    pages: null,
    refresh: false,
    unReaded: 0,
}

const CategoriesReducer = (state = initState, action) => {
    switch (action.type) {
        case GET_ADS_CATEGORIES:
          
            return {
                ...state,
              //  categories: action.payload,
                loading: false,
                refresh: false,
             //   pages: action.pages,
                errorText: null,
            };

        default:
            return state;
    }
}

export default CategoriesReducer;