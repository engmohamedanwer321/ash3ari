import * as types from "../actions/types"
import * as colors from '../assets/colors' 

const initialState = {
    unSeen:0,
    open:false,
    image:''
}

const ChatReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.CHAT:
            
            return { ...state, unSeen: action.payload };    
        case types.OPEN_IMAGE:
            
            return { ...state, open:action.payload, image:action.img };        
        default:
            return state;
    }

}

export default ChatReducer;