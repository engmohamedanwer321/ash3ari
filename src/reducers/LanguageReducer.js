import * as types from "../actions/types"
import * as colors from '../assets/colors' 

const initialState = {
    RTL: true,
    color: '#207cd0'
}

const LanguageReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.CHANGE_LANGUAGE:
            console.log('in reducer =>'+action.payload)
            return { ...state, RTL: action.payload };   
        case types.CHANGE_COLOR:
            console.log('color =>'+action.payload)
            return { ...state, color: action.payload };    
        default:
            return state;
    }

}

export default LanguageReducer;