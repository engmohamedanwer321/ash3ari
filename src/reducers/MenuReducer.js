

import * as types from "../actions/types";



const initialState = {

    item: ["HOME"],
    logoutLoading: false,
    loggedOut: false
}

const MenuReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.SELECT_MENU:


            return { item: [...state.item, action.payload] };
        case types.POP_ITEM:
            state.item.pop()
            return { item: [...state.item] };
        case types.LOGOUT_LOADING:
            return { ...state, logoutLoading: true };
        case types.LOGOUT_LOADING_END:
            return { ...state, logoutLoading: false };
        case types.LOGOUT:
            return initialState;
        case types.LOGGEDOUT:
            return { ...state, loggedOut: true }
        default:
            return state;
    }

}



export default MenuReducer;