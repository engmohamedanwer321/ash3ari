import {
    FETCH_NOTIFICATIONS_REFRESH,FETCH_NOTIFICATIONS_REQUEST,
    FETCH_NOTIFICATIONS_SUCCESS,FETCH_NOTIFICATIONS_FAIL,LOGOUT,
    UNREADEDNOTI
} from '../actions/types';

import {
    DataProvider,
} from 'recyclerlistview';


const initState = {
    loading:false,
    notifications: new DataProvider(),
    pages:null,
    refresh: false,
    unReaded: 0,
}

const NotificationsReducer = (state=initState, action) => {
    switch(action.type){
        case FETCH_NOTIFICATIONS_REQUEST:
            return { ...state,errorText:null, loading: true, refresh: false };
        case FETCH_NOTIFICATIONS_SUCCESS:
        
            return {
                ...state,
                notifications: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(state.refresh || action.page ==1 ? [...action.payload] : [...state.notifications.getAllData(), ...action.payload]),
                loading: false,
                refresh: false,
                pages: action.pages,
                errorText:null,
            };
        case FETCH_NOTIFICATIONS_FAIL:
            return { ...state, loading: false, refresh: false, errorText: action.payload };
        case FETCH_NOTIFICATIONS_REFRESH:
            return { ...state,errorText:null, refresh: true, loading: false };
        case UNREADEDNOTI:
            console.log("nossss "+ action.payload)
            return { ...state,unReaded:action.payload };    
        case LOGOUT:
            return initState;       
            default: 
        return state; 
    }
}

export default NotificationsReducer;