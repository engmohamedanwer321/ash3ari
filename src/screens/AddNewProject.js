import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
    View,
    Animated,
    ScrollView,
    TouchableOpacity,
    Text,
    Modal,
    ImageBackground,
    TextInput,
    StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail, Button } from 'native-base';
import {
    responsiveWidth,
    moderateScale,
    responsiveFontSize,
    responsiveHeight,
} from '../utils/responsiveDimensions';
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from 'redux-form';
import { setUser } from '../actions/AuthActions';
import { BASE_END_POINT } from '../AppConfig';
import axios from 'axios';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader';
import strings from '../assets/strings';
import {
    enableSideMenu,
    push,
    pop,
    resetTo,
} from '../controlls/NavigationControll';
import { arrabicFont, englishFont } from '../common/AppFont';
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import CollaspeAppHeader from '../common/CollaspeAppHeader';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import InputValidations from '../common/InputValidations';
import PageTitleLine from '../components/PageTitleLine';
import RadioButton from 'radio-button-react-native';
import ImagePicker from 'react-native-image-crop-picker';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import FastImage from 'react-native-fast-image'

class AddNewProject extends Component {
    state = {
        type: 'request',
        title: ' ',
        describtion: ' ',
        image: null,
        attatchments: null,
        time: ' ',
        price: ' ',
        loading: false,
    };

    componentDidMount() {
        enableSideMenu(false, this.props.isRTL);
    }

    componentDidUpdate() {
        enableSideMenu(false, this.props.isRTL);
    }

    componentWillUnmount() {
        this.props.removeItem();
    }


    handlePrice(val) {
        var newVal = ''
        newVal = (val.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (d) {
            return d.charCodeAt(0) - 1632;
        }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function (d) { return d.charCodeAt(0) - 1776; })
        );
        this.setState({ price: newVal })
    }

    send = () => {
        const { title, price, image, describtion, type } = this.state;

        var url;

        url = `${BASE_END_POINT}provider/projects/new`;

        if (!title.replace(/\s/g, '').length) {
            this.setState({ title: '' });
            RNToasty.Error({ title: Strings.insertTheRequiredData });
        }

        //if (price.replace(/\s/g, '').length) {

        if (isNaN(price) && price.replace(/\s/g, '').length) {
            // RNToasty.Error({ title: Strings.valueEnteredNumberOnly })
        }


        /*if (!describtion.replace(/\s/g, '').length) {
            this.setState({ describtion: '' })
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }
        if (!image) {
            this.setState({ image:false})
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }
        if (!attatchments) {
            this.setState({ attatchments:false})
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }*/

        else if (title.replace(/\s/g, '').length) {
            this.setState({ loading: true });
            var data = new FormData();
            data.append('title', title);
            data.append('description', describtion);
            data.append('price', price);
            data.append('file_type', 'image');

            if (image) {
                data.append('file', {
                    uri: image,
                    type: 'multipart/form-data',
                    name: 'fileN',
                });
            }

            axios
                .post(url, data, {
                    headers: {
                        Authorization: 'Bearer ' + this.props.currentUser.data.token,
                    },
                })
                .then((response) => {
                    this.setState({ loading: false });
                    resetTo('Home');

                    RNToasty.Success({ title: strings.sendSuccessfully });
                })
                .catch((error) => {
                    console.log(error.response)
                    this.setState({ loading: false });
                });
        }
        // }
    };

    showLoading = () => {
        return this.state.loading && <LoadingDialogOverlay title={Strings.wait} />;
    };

    typeRadioButtonInputs = () => {
        const { type } = this.state;
        const { isRTL } = this.props;
        return (
            <View
                style={{
                    marginBottom: moderateScale(5),
                    width: responsiveWidth(60),
                    alignSelf: 'center',
                    flexDirection: isRTL ? 'row-reverse' : 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}>
                <View style={{}}>
                    <RadioButton
                        innerCircleColor="orange"
                        currentValue={type}
                        value={'request'}
                        onPress={(value) => {
                            this.setState({ type: value });
                        }}>
                        <Text
                            style={{
                                marginTop: moderateScale(1),
                                fontFamily: isRTL ? arrabicFont : englishFont,
                                marginHorizontal: moderateScale(2),
                            }}>
                            {Strings.request}
                        </Text>
                    </RadioButton>
                </View>

                <View style={{}}>
                    <RadioButton
                        innerCircleColor="orange"
                        currentValue={type}
                        value={'offer'}
                        onPress={(value) => {
                            this.setState({ type: value });
                        }}>
                        <Text
                            style={{
                                marginTop: moderateScale(1),
                                fontFamily: isRTL ? arrabicFont : englishFont,
                                marginHorizontal: moderateScale(2),
                            }}>
                            {Strings.offer}
                        </Text>
                    </RadioButton>
                </View>
            </View>
        );
    };

    titleInput = () => {
        const { title } = this.state;
        const { isRTL } = this.props;
        return (
            <Animatable.View
                style={{
                    marginTop: moderateScale(5),
                    width: responsiveWidth(85),
                    alignSelf: 'center',
                }}>
                {/*<View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                    <View style={{ width: responsiveWidth(6)}}>
                        <Icon type='MaterialCommunityIcons' name='subtitles-outline' style={{ fontSize: responsiveFontSize(8), color:colors.darkBlue }} />
                    </View>


        <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(3) }}>{Strings.title}</Text>
                </View>*/}
                <View
                    style={{
                        flexDirection: isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        marginTop: moderateScale(1),
                        width: responsiveWidth(85),
                        borderBottomRadius: moderateScale(10),
                        borderBottomWidth: 0.5,
                        borderColor: '#cccbcb',
                        height: responsiveHeight(8),
                    }}>
                    <TextInput
                        onChangeText={(val) => {
                            this.setState({ title: val });
                        }}
                        placeholder={Strings.projectTitle}
                        underlineColorAndroid="transparent"
                        style={{
                            fontSize: responsiveFontSize(6),
                            fontFamily: isRTL ? arrabicFont : englishFont,
                            width: responsiveWidth(78),
                            color: 'gray',
                            direction: isRTL ? 'rtl' : 'ltr',
                            textAlign: isRTL ? 'right' : 'left',
                        }}
                    />
                </View>
                {title.length == 0 && (
                    <Text
                        style={{
                            color: 'red',
                            fontSize: responsiveFontSize(6),
                            alignSelf: isRTL ? 'flex-start' : 'flex-end',
                        }}>
                        {' '}
                        {Strings.require}
                    </Text>
                )}
            </Animatable.View>
        );
    };

    descriptionInput = () => {
        const { describtion } = this.state;
        const { isRTL } = this.props;
        return (
            <Animatable.View
                style={{
                    marginTop: moderateScale(7),
                    width: responsiveWidth(85),
                    alignSelf: 'center',
                }}>
                {/*<View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                <View style={{ width: responsiveWidth(6)}}>
                        <Icon type='FontAwesome' name='file-text-o' style={{ fontSize: responsiveFontSize(7), color:colors.darkBlue }} />
                    </View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(3) }}>{Strings.adsContent}</Text>
                </View>*/}
                <View
                    style={{
                        borderColor: colors.lightGray,
                        flexDirection: isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'center',
                        backgroundColor: 'white',
                        marginTop: moderateScale(1),
                        width: responsiveWidth(85),
                        borderRadius: moderateScale(3),
                        borderWidth: 0.5,
                        height: responsiveHeight(20),
                        marginTop: moderateScale(5),
                    }}>
                    <TextInput
                        onChangeText={(val) => {
                            this.setState({ describtion: val });
                        }}
                        multiline={true}
                        placeholder={Strings.projectDesription}
                        underlineColorAndroid="transparent"
                        style={{
                            textAlignVertical: 'top',
                            fontSize: responsiveFontSize(6),
                            fontFamily: isRTL ? arrabicFont : englishFont,
                            width: responsiveWidth(75),
                            color: 'gray',
                            direction: isRTL ? 'rtl' : 'ltr',
                            textAlign: isRTL ? 'right' : 'left',
                        }}
                    />
                </View>
                {describtion.length == 0 && (
                    <Text
                        style={{
                            marginHorizontal: moderateScale(3),
                            color: 'red',
                            fontSize: responsiveFontSize(6),
                            alignSelf: isRTL ? 'flex-start' : 'flex-end',
                        }}>
                        {' '}
                        {Strings.require}
                    </Text>
                )}
            </Animatable.View>
        );
    };



    priceInput = () => {
        const { price } = this.state;
        const { isRTL } = this.props;
        return (
            <Animatable.View
                style={{
                    marginTop: moderateScale(5),
                    width: responsiveWidth(85),
                    alignSelf: 'center',
                }}>
                {/*<View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                <View style={{ width: responsiveWidth(6)}}>
                        <Icon type='MaterialIcons' name='access-time' style={{ fontSize: responsiveFontSize(9), color:colors.darkBlue }} />
                    </View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(3) }}>{Strings.expectedTime}</Text>
                </View>*/}
                <View
                    style={{
                        flexDirection: isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        marginTop: moderateScale(1),
                        width: responsiveWidth(85),
                        borderBottomRadius: moderateScale(10),
                        borderBottomWidth: 0.5,
                        borderColor: '#cccbcb',
                        height: responsiveHeight(8),
                    }}>
                    <TextInput
                        onChangeText={(val) => {
                            //this.setState({ price: val });
                            this.handlePrice(val)
                        }}
                        placeholder={Strings.estimatedValueWork}
                        underlineColorAndroid="transparent"
                        style={{
                            fontSize: responsiveFontSize(6),
                            fontFamily: isRTL ? arrabicFont : englishFont,
                            width: responsiveWidth(78),
                            color: 'gray',
                            direction: isRTL ? 'rtl' : 'ltr',
                            textAlign: isRTL ? 'right' : 'left',
                        }}
                    />
                </View>
                {price.length == 0 ?
                    <Text
                        style={{
                            color: 'red',
                            fontSize: responsiveFontSize(6),
                            alignSelf: isRTL ? 'flex-start' : 'flex-end',
                        }}>
                        {' '}
                        {Strings.require}
                    </Text>
                    :
                    isNaN(price) &&
                    <Text
                        style={{
                            color: 'red',
                            fontSize: responsiveFontSize(6),
                            alignSelf: isRTL ? 'flex-start' : 'flex-end',
                        }}>
                        {' '}
                        {Strings.valueEnteredNumberOnly}
                    </Text>

                }
            </Animatable.View>
        );
    };

    imageInput = () => {
        const { image } = this.state;
        const { isRTL } = this.props;
        return (
            <Animatable.View
                style={{
                    marginTop: moderateScale(7),
                    width: responsiveWidth(85),
                    alignSelf: 'center',
                }}>
                <View
                    style={{
                        flexDirection: isRTL ? 'row-reverse' : 'row',
                        alignItems: 'center',
                    }}>
                    {/*<View style={{ width: responsiveWidth(6) }}>
                        <Icon
                            type="Feather"
                            name="image"
                            style={{ fontSize: responsiveFontSize(8), color: colors.darkBlue }}
                        />
                    </View>*/}
                    <Text
                        style={{
                            color: 'black',
                            fontSize: responsiveFontSize(6),
                            alignSelf: isRTL ? 'flex-end' : 'flex-start',
                            fontFamily: isRTL ? arrabicFont : englishFont,
                            marginHorizontal: moderateScale(3),
                        }}>
                        {strings.defaultProjectImage}
                    </Text>
                </View>
                <View
                    style={{
                        flexDirection: isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        marginTop: moderateScale(1),
                        width: responsiveWidth(78),
                        borderBottomRadius: moderateScale(10),
                        borderBottomWidth: 0.5,
                        borderColor: '#cccbcb',
                        height: responsiveHeight(8),
                    }}>
                    <Text
                        style={{
                            fontSize: responsiveFontSize(6),
                            fontFamily: isRTL ? arrabicFont : englishFont,
                            width: responsiveWidth(58),
                            color: 'gray',
                        }}>
                        {image ? image : strings.noImageSelected}
                    </Text>
                    <Button
                        onPress={() => {
                            ImagePicker.openPicker({
                                width: 300,
                                height: 400,
                                cropping: true,
                            }).then((image) => {

                                this.setState({ image: image.path });
                            });
                        }}
                        style={{
                            backgroundColor: colors.grayButton,
                            width: responsiveWidth(20),
                            height: responsiveHeight(5),
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                        <Text>{strings.upload}</Text>
                    </Button>
                </View>
                {image == false && (
                    <Text
                        style={{
                            color: 'red',
                            fontSize: responsiveFontSize(6),
                            alignSelf: isRTL ? 'flex-start' : 'flex-end',
                        }}>
                        {' '}
                        {Strings.require}
                    </Text>
                )}
            </Animatable.View>
        );
    };

    attachInput = () => {
        const { attatchments } = this.state;
        const { isRTL } = this.props;
        return (
            <Animatable.View
                style={{
                    marginTop: moderateScale(7),
                    width: responsiveWidth(85),
                    alignSelf: 'center',
                }}>
                <View
                    style={{
                        flexDirection: isRTL ? 'row-reverse' : 'row',
                        alignItems: 'center',
                    }}>
                    <View style={{ width: responsiveWidth(6) }}>
                        <Icon
                            type="MaterialIcons"
                            name="attach-file"
                            style={{ fontSize: responsiveFontSize(9), color: colors.darkBlue }}
                        />
                    </View>
                    <Text
                        style={{
                            color: 'black',
                            fontSize: responsiveFontSize(6),
                            alignSelf: isRTL ? 'flex-end' : 'flex-start',
                            fontFamily: isRTL ? arrabicFont : englishFont,
                            marginHorizontal: moderateScale(3),
                        }}>
                        {strings.attachment}
                    </Text>
                </View>
                <View
                    style={{
                        flexDirection: isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        marginTop: moderateScale(1),
                        width: responsiveWidth(78),
                        borderBottomRadius: moderateScale(10),
                        borderBottomWidth: 0.5,
                        borderColor: '#cccbcb',
                        height: responsiveHeight(8),
                    }}>
                    <Text
                        style={{
                            fontSize: responsiveFontSize(6),
                            fontFamily: isRTL ? arrabicFont : englishFont,
                            width: responsiveWidth(58),
                            color: 'gray',
                        }}>
                        {attatchments ? attatchments : strings.noAttachSelected}
                    </Text>
                    <Button
                        onPress={() => {
                            ImagePicker.openPicker({
                                width: 300,
                                height: 400,
                                cropping: true,
                            }).then((image) => {

                                this.setState({ attatchments: image.path });
                            });
                        }}
                        style={{
                            backgroundColor: colors.grayButton,
                            width: responsiveWidth(20),
                            height: responsiveHeight(5),
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                        <Text>{strings.upload}</Text>
                    </Button>
                </View>
                {attatchments == false && (
                    <Text
                        style={{
                            color: 'red',
                            fontSize: responsiveFontSize(6),
                            alignSelf: isRTL ? 'flex-start' : 'flex-end',
                        }}>
                        {' '}
                        {Strings.require}
                    </Text>
                )}
            </Animatable.View>
        );
    };

    sendButton = () => {
        const { isRTL, barColor } = this.props;
        return (
            <View style={{ alignSelf: 'center' }}>
                <Button
                    onPress={() => {
                        this.send();
                    }}
                    style={{
                        marginTop: moderateScale(12),
                        backgroundColor: colors.darkBlue,
                        borderRadius: moderateScale(3),
                        width: responsiveWidth(30),
                        height: responsiveHeight(7),
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                    <Text
                        style={{
                            fontFamily: isRTL ? arrabicFont : englishFont,
                            color: colors.white,
                        }}>
                        {strings.send}
                    </Text>
                </Button>
            </View>
        );
    };

    content = () => {
        const { isRTL, barColor } = this.props;
        return (
            <View>
                <PageTitleLine
                    title={Strings.addNewProject}
                    iconName="local-offer"
                    iconType="MaterialIcons"
                />
                <View
                    style={{
                        marginBottom: moderateScale(15),
                        width: responsiveWidth(90),
                        alignSelf: 'center',
                        marginTop: moderateScale(10),
                    }}>
                    {this.titleInput()}
                    {this.descriptionInput()}
                    {this.imageInput()}

                    {this.priceInput()}
                    {this.sendButton()}
                </View>
                {this.showLoading()}
            </View>
        );
    };

    render() {
        const { currentUser, isRTL } = this.props;
        const { name, email, phone, message } = this.state;
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => (
                    <CollaspeAppHeader back title={Strings.addNewProject} />
                )}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1 }}
            />*/

            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={Strings.addNewProject} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
        );
    }
}

const mapDispatchToProps = {
    //setUser,
    removeItem,
};

const mapToStateProps = (state) => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color,
});

export default connect(mapToStateProps, mapDispatchToProps)(AddNewProject);
