import React, { Component } from 'react';
import { View, RefreshControl, Alert, FlatList, ScrollView, Text, TouchableOpacity, Clipboard, Image, Linking } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import ListFooter from '../common/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import ChatPeopleCard from '../components/ChatPeopleCard';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader';
import { enableSideMenu, pop, push } from '../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import * as colors from '../assets/colors'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import Swipeout from 'react-native-swipeout';
import NotificationCard from '../components/NotificationCard'
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header'
import PageTitleLine from '../components/PageTitleLine'
import WebView from 'react-native-webview'
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import ImagePicker from 'react-native-image-crop-picker';
import { RNToasty } from 'react-native-toasty'
import ParallaxScrollView from 'react-native-parallax-scroll-view';

class BankAccounts extends Component {

    page = 1;
    list = [];
    state = {
        networkError: null,
        rooms: [],
        refresh: false,
        loading: false,
        pages: null,
        paymentType: 0,
        currentPage: 0,
        sandboxUrl: '',
        image: null,
        imageHeight: 0,
        note: '',
        clipboardContent: null,
    }



    componentDidMount() {
        enableSideMenu(false, this.props.isRTL)
    }
    componentWillUnmount() {
        this.props.removeItem('PAYMENT')
    }

    sendTransferReport = () => {
        const { transferType } = this.props.data
        const { image, note } = this.state
  
        this.setState({ loading: true })
        var data = new FormData()

        data.append('image', {
            uri: image,
            type: 'multipart/form-data',
            name: 'photoImage'
        })
        data.append('type', transferType)
        data.append('message', note)
        axios.post(`${BASE_END_POINT}user/send_report`, data, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.props.currentUser.data.token}`
            }
        })
            .then(response => {
                this.setState({ loading: false })
                const res = response.data

                if (response.data.success == true) {
                    RNToasty.Success({ title: res.msg })
                    pop()
                    // this.setState({ loading: false })
                } else {
               
                    RNToasty.Error({ title: res.msg })
                }

            })
            .catch(error => {

           
                this.setState({ loading: false })

            })

    }


    writeToClipboard = async (code) => {
        //To copy the text to clipboard
        await Clipboard.setString(code);
        Alert.alert(Strings.numberHasBeenCopied);
    };

    page2() {
        const { sandboxUrl } = this.state
        const { isRTL } = this.props
        return (

            <WebView
                source={{ uri: sandboxUrl }}
                style={{ marginTop: 20, width: responsiveWidth(100), height: responsiveHeight(50) }}
            />

        )
    }

    page1 = () => {
        const { isRTL, barColor } = this.props
        const { paymentType, image, imageHeight } = this.state
    
        return (
            <View>

                <PageTitleLine title={this.props.data.data} iconName={this.props.data.data == Strings.payForExcellence ? 'star-o' : 'money'} iconType='FontAwesome' />

                <View style={{ marginTop: moderateScale(10), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}>
                    <Text style={{ fontSize: responsiveFontSize(8), marginHorizontal: moderateScale(7), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.bankTransfer}</Text>
                </View>

                <View style={{ marginTop: moderateScale(6), alignSelf: 'center' }}>
                    <Text style={{ fontSize: responsiveFontSize(6), marginHorizontal: moderateScale(7), color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont, backgroundColor: colors.lightGray, padding: moderateScale(2),paddingHorizontal:moderateScale(4), marginVertical: moderateScale(2), borderRadius: moderateScale(2) }}>1- {Strings.copyAccountNumber}</Text>
                    <Text style={{ fontSize: responsiveFontSize(6), marginHorizontal: moderateScale(7), color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont, backgroundColor: colors.lightGray, padding: moderateScale(2),paddingHorizontal:moderateScale(4), marginVertical: moderateScale(2), borderRadius: moderateScale(2) }}>2- {Strings.pressOnBankIcon}</Text>
                    <Text style={{ fontSize: responsiveFontSize(6), marginHorizontal: moderateScale(7), color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont, backgroundColor: colors.lightGray, padding: moderateScale(2),paddingHorizontal:moderateScale(4), marginVertical: moderateScale(2), borderRadius: moderateScale(2) }}>3- {Strings.makeTransfer}</Text>
                </View>

                <View style={{ alignSelf: 'center', width: responsiveWidth(80) }}>

                    <View style={{ marginTop: moderateScale(10), }}>
                        <Text style={{ fontSize: responsiveFontSize(8), marginHorizontal: moderateScale(5), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' }}>{Strings.kindlySendReceiptFromHere}</Text>
                    </View>
                    <TouchableOpacity onPress={() => {
                        push('UploadTransferReceipt', { data: this.props.data.data, transferType: 2 })

                    }} style={{ elevation: 4, shadowOffset: { height: 0, width: 2 }, shadowOpacity: 0.2, backgroundColor: 'green', marginVertical: moderateScale(10), borderRadius: moderateScale(5), width: responsiveWidth(70), height: responsiveHeight(6.5), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.uploadTransferImage}</Text>
                    </TouchableOpacity>


                    <View style={{ borderBottomColor: colors.greenApp, borderBottomWidth: 0.8 }}>
                        <Text style={{ textAlign: 'center', color: colors.darkBlue, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.mainAccount}</Text>
                    </View>

                    <TouchableOpacity style={{ width: responsiveWidth(96), alignSelf: 'center', marginVertical: moderateScale(2), zIndex: 10000, backgroundColor: 'white', borderRadius: moderateScale(5) }} activeOpacity={1}
                    >


                        <View style={{ alignSelf: 'center', alignItems: 'center', width: responsiveWidth(96) }}>

                            <View style={{ width: responsiveWidth(77), marginHorizontal: moderateScale(3), alignSelf: 'center', alignItems: 'center' }}>
                                <TouchableOpacity
                                onPress={()=>Linking.openURL('https://new.alahlionline.com/ui/#/login/full-login-login')}>
                                <FastImage source={require('../assets/imgs/bank-ahli.png')} style={{ width: responsiveWidth(30), height: responsiveHeight(10), resizeMode: 'contain' }} />
                                </TouchableOpacity>
                                
                                <TouchableOpacity
                                    onPress={() => this.writeToClipboard('14700000123707')}>
                                    <Text style={{ textAlign: 'center', fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.darkGray }}>{Strings.accountNo}: 14700000123707</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.writeToClipboard('SA8510000014700000123707')}>
                                <Text style={{ textAlign: 'center', color: colors.darkGray, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.iban}: SA8510000014700000123707</Text>
                                </TouchableOpacity>
                                <Text style={{ textAlign: 'center', color: colors.darkGray, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.founderShaariDalili}</Text>

                            </View>
                        </View>
                        <View style={{ width: responsiveWidth(80), alignSelf: 'center', marginBottom: moderateScale(4) }}>
                            <Text style={{ color: colors.black, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}></Text>
                        </View>



                    </TouchableOpacity>

                    <View style={{ borderBottomColor: colors.greenApp, borderBottomWidth: 0.8 }}>
                        <Text style={{ textAlign: 'center', color: colors.darkBlue, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.alternateAccounts}</Text>
                    </View>



                    <TouchableOpacity style={{ width: responsiveWidth(96), alignSelf: 'center', marginVertical: moderateScale(2), zIndex: 10000, backgroundColor: 'white', borderRadius: moderateScale(5), }} activeOpacity={1}
                    >


                        <View style={{ alignSelf: 'center', alignItems: 'center', width: responsiveWidth(96) }}>

                            <View style={{ width: responsiveWidth(77), marginHorizontal: moderateScale(3), alignSelf: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={()=>Linking.openURL('https://www.bankalbilad.com/ar/personal/Pages/home.aspx')}>
                                <FastImage source={require('../assets/imgs/bank1.png')} style={{ width: responsiveWidth(30), height: responsiveHeight(10), resizeMode: 'contain' }} />
                                </TouchableOpacity>
                                
                                <TouchableOpacity
                                    onPress={() => this.writeToClipboard('866587853480002')}>
                                <Text style={{ textAlign: 'center', fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.darkGray }}>{Strings.accountNo}: 866587853480002</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.writeToClipboard('SA1115000866587853480002')}>
                                <Text style={{ textAlign: 'center', color: colors.darkGray, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.iban}: SA1115000866587853480002</Text>
                                </TouchableOpacity>
                                <Text style={{ textAlign: 'center', color: colors.darkGray, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.saadZaferSaadElahmary}</Text>

                            </View>
                        </View>




                    </TouchableOpacity>


                    <TouchableOpacity style={{ width: responsiveWidth(96), alignSelf: 'center', marginVertical: moderateScale(2), zIndex: 10000, backgroundColor: 'white', borderRadius: moderateScale(5), }} activeOpacity={1}
                    >


                        <View style={{ alignSelf: 'center', alignItems: 'center', width: responsiveWidth(96) }}>

                            <View style={{ width: responsiveWidth(77), marginHorizontal: moderateScale(3), alignSelf: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={()=>Linking.openURL('https://www.baj.com.sa/Aljaziraonline')}>
                                <FastImage source={require('../assets/imgs/bank2.png')} style={{ width: responsiveWidth(30), height: responsiveHeight(10), resizeMode: 'contain' }} />
                                </TouchableOpacity>
                                
                                <TouchableOpacity
                                    onPress={() => this.writeToClipboard('19361715001')}>
                                <Text style={{ textAlign: 'center', fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.darkGray }}>{Strings.accountNo}: 19361715001</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.writeToClipboard('SA8960000000019361715001')}>
                                <Text style={{ textAlign: 'center', color: colors.darkGray, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.iban}: SA8960000000019361715001</Text>
                                </TouchableOpacity>
                                <Text style={{ textAlign: 'center', color: colors.darkGray, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.saadZaferSaadElahmary}</Text>

                            </View>
                        </View>




                    </TouchableOpacity>


                    <TouchableOpacity style={{ width: responsiveWidth(96), alignSelf: 'center', marginVertical: moderateScale(2), zIndex: 10000, backgroundColor: 'white', borderRadius: moderateScale(5), }} activeOpacity={1}
                    >


                        <View style={{ alignSelf: 'center', alignItems: 'center', width: responsiveWidth(96) }}>

                            <View style={{ width: responsiveWidth(77), marginHorizontal: moderateScale(3), alignSelf: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={()=>Linking.openURL('https://www.almubasher.com.sa/portal/auth/login.do?locale=ar_SA')}>
                                <FastImage source={require('../assets/imgs/bank3.png')} style={{ width: responsiveWidth(30), height: responsiveHeight(10), resizeMode: 'contain' }} />
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => this.writeToClipboard('329608010116857')}>
                                <Text style={{ textAlign: 'center', fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.darkGray }}>{Strings.accountNo}: 329608010116857</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.writeToClipboard('SA9380000329608010116857')}>
                                <Text style={{ textAlign: 'center', color: colors.darkGray, fontSize: responsiveFontSize(5), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.iban}: SA9380000329608010116857</Text>
                                </TouchableOpacity>
                                <Text style={{ textAlign: 'center', color: colors.darkGray, fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.saadZaferSaadElahmary}</Text>

                            </View>
                        </View>



                    </TouchableOpacity>






                </View>

            </View>
        )
    }

    content = () => {
        const { isRTL, barColor } = this.props
        const { paymentType, currentPage, loading } = this.state
        return (
            <>

                {currentPage == 0 && this.page1()}
                {currentPage == 1 && this.page2()}
                {loading && <LoadingDialogOverlay title={Strings.wait} />}


            </>

        )
    }

    render() {
        const { isRTL } = this.props;
        return (
           /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={Strings.payment} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/

            <ParallaxScrollView
            backgroundColor={colors.darkBlue}
            contentBackgroundColor="white"
            renderFixedHeader={() => <CollaspeAppHeader back title={Strings.payment} />}
            parallaxHeaderHeight={responsiveHeight(35)}
            renderBackground={() => (
                <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

            )}
        >
            {this.content()}
        </ParallaxScrollView>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(BankAccounts);
