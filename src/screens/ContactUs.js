import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, Animated, ScrollView, TouchableOpacity, Text, Modal, ImageBackground, TextInput,
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail, Button } from "native-base";
import { responsiveWidth, moderateScale, responsiveFontSize, responsiveHeight } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import { getUser } from '../actions/AuthActions';
import { BASE_END_POINT } from '../AppConfig'
import axios from 'axios';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import strings from '../assets/strings';
import { enableSideMenu, push, pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors'
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import InputValidations from '../common/InputValidations'
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import FastImage from 'react-native-fast-image'


/*
<Animatable.View animation={isRTL?"slideInLeft":"slideInRight"} duration={1500} style={{marginTop:moderateScale(5), width:responsiveWidth(90),height:responsiveHeight(8), alignSelf:'center'}}>
            
            <View style={{width:responsiveWidth(90),height:responsiveHeight(8), flexDirection:isRTL?'row-reverse':'row', alignItems:'center', alignSelf:'center',borderRadius:moderateScale(3), borderWidth:1,borderColor:'#cccbcb'}} >

             <View style={{marginHorizontal:moderateScale(5)}}>
             <Icon name='email' type='MaterialIcons' style={{color:'gray',fontSize:responsiveFontSize(7)}} />
             </View>

             <TextInput 
             placeholder={Strings.email}
             onChangeText={(val)=>{this.setState({email:val})}}
              underlineColorAndroid='transparent' style={{fontSize:responsiveFontSize(6), fontFamily:isRTL?arrabicFont:englishFont, width:responsiveWidth(75), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}} 
             />

            </View>

             {email.length==0&&
             <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
             }
            </Animatable.View>
*/

class ContactUs extends Component {

    state = {
        name: ' ',
        email: ' ',
        phone: ' ',
        message: ' ',
        loading: false,
    }

    componentWillUnmount() {
        this.props.removeItem()
    }

    componentDidMount() {
        enableSideMenu(false, this.props.isRTL)
    }

    componentDidUpdate() {
        enableSideMenu(false, this.props.isRTL)
    }

    nameInput = () => {
        const { name } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1500} style={{ marginTop: moderateScale(10), width: responsiveWidth(80), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(80), borderRadius: moderateScale(2), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='user-alt' type='FontAwesome5' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        onChangeText={(val) => { this.setState({ name: val }) }}
                        placeholder={Strings.name} 
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(68), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {name.length == 0 &&
                    <Text style={{ marginHorizontal: moderateScale(3), color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }


    emailInput = () => {
        const { email } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={isRTL ? "slideInLeft" : "slideInRight"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(80), borderRadius: moderateScale(3), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='email' type='MaterialCommunityIcons' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        onChangeText={(val) => { this.setState({ email: val }) }}
                        placeholder={Strings.email}
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(68), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {email.length == 0 &&
                    <Text style={{ marginHorizontal: moderateScale(3), color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    phoneInput = () => {
        const { phone } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(80), borderRadius: moderateScale(3), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='phone' type='FontAwesome' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        keyboardType='phone-pad'
                        onChangeText={(val) => { this.setState({ phone: val }) }}
                        placeholder={Strings.phone}
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(68), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {phone.length == 0 &&
                    <Text style={{ marginHorizontal: moderateScale(3), color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    messageInput = () => {
        const { message } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(80), borderRadius: moderateScale(3), borderWidth: 0.5, borderColor: colors.darkBlue }}>
                    <View style={{ marginHorizontal: moderateScale(5), marginTop: moderateScale(4) }}>
                        <Icon name='pushpin' type='AntDesign' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        multiline
                        onChangeText={(val) => { this.setState({ message: val }) }}
                        placeholder={Strings.message}
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ textAlignVertical: 'top', height: responsiveHeight(20), fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(68), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {message.length == 0 &&
                    <Text style={{ marginHorizontal: moderateScale(3), color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    send = () => {
        const { loading, name, email, phone, message } = this.state
        const { currentUser, isRTL } = this.props;
        if (!name.replace(/\s/g, '').length) {
            this.setState({ name: '' })
        }
        if (!email.replace(/\s/g, '').length) {
            this.setState({ email: '' })
        }
        if (InputValidations.validateEmail(email) == false) {
            RNToasty.Error({ title: Strings.emailNotValid })
        }
        if (!phone.replace(/\s/g, '').length) {
            this.setState({ phone: '' })
        }
        if (!message.replace(/\s/g, '').length) {
            this.setState({ message: '' })
        }

        if (name.replace(/\s/g, '').length && email.replace(/\s/g, '').length && phone.replace(/\s/g, '').length && message.replace(/\s/g, '').length && InputValidations.validateEmail(email)) {
            this.setState({ loading: true })
            var data = new FormData()
            data.append('email', email)
            data.append('name', name)
            data.append('phone', phone)
            data.append('message', message)

            axios.post(`${BASE_END_POINT}contact`, data)
                .then(response => {
                    this.setState({ loading: false })

                    pop()
                    RNToasty.Success({ title: Strings.sendSuccessfully })
                })
                .catch(error => {

                    this.setState({ loading: false })
                })
        }
    }

    sendButton = () => {
        const { name, email, phone, message } = this.state
        const { currentUser, isRTL } = this.props;
        return (
            <Button onPress={() => { this.send() }} style={{ marginVertical: moderateScale(18), width: responsiveWidth(40), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }}>
                <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.send}</Text>
            </Button>
        )
    }

    showLoading = () => {
        return (
            this.state.loading &&
            <LoadingDialogOverlay title={Strings.wait} />
        )
    }

    content = () => {
        const { isRTL, barColor } = this.props
        return (
            <View>
                {this.nameInput()}
                {this.emailInput()}
                {this.phoneInput()}
                {this.messageInput()}
                {this.sendButton()}
                {this.showLoading()}
            </View>
        )
    }




    render() {
        const { currentUser, isRTL } = this.props;
        const { name, email, phone, message } = this.state
        return (
            /*<ReactNativeParallaxHeader
            scrollViewProps={{showsVerticalScrollIndicator:false}}
            headerMinHeight={responsiveHeight(10)}
            headerMaxHeight={responsiveHeight(35)}
            extraScrollHeight={20}
            navbarColor={colors.darkBlue}
            backgroundImage={require('../assets/imgs/header.png')}
            backgroundImageScale={1.2}
            renderNavBar={()=><CollaspeAppHeader back title={Strings.contactUS} />}
            renderContent={()=>this.content()}
            containerStyle={{flex:1}}
            contentContainerStyle={{flexGrow: 1}}
            innerContainerStyle={{flex: 1,}}
          /> */


            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={Strings.contactUS} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
        );
    }
}



const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color
})


export default connect(mapToStateProps, mapDispatchToProps)(ContactUs);

