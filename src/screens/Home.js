import React, { Component } from 'react';
import {
  View, TouchableOpacity, ScrollView, FlatList, Text, Platform, AppState,
  Alert, ImageBackground, BackHandler, Image, Dimensions, Picker, Animated
} from 'react-native';
import { moderateScale, responsiveFontSize, responsiveWidth, responsiveHeight } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import { Icon, Button, Thumbnail } from 'native-base';
import strings from '../assets/strings';
import AppHeader from '../common/AppHeader'
import { getUnreadNotificationsNumers } from '../actions/NotificationAction';
import { getOrdersCount } from '../actions/OrdersActions'
import { selectMenu, removeItem } from '../actions/MenuActions';
import { getUnseenMessages } from '../actions/ChatAction'
import { enableSideMenu, push } from '../controlls/NavigationControll'
import * as colors from '../assets/colors'
import { arrabicFont, englishFont } from '../common/AppFont'
import DeapartmentCard from '../components/DeapartmentCard'
import AdsCard from '../components/AdsCard'
import DesignsCard from '../components/DesignsCard'
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import RNScrollable from 'react-native-scrollable-header';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import MapView, { Marker, Callout } from 'react-native-maps';
import PageTitleLine from '../components/PageTitleLine'
import axios from 'axios'
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import NetInfo from "@react-native-community/netinfo";
import { BASE_END_POINT } from '../AppConfig';
import FastImage from 'react-native-fast-image'
import MarketCard from '../components/MarketCard'
import { RNToasty } from 'react-native-toasty';
import * as Animatable from 'react-native-animatable';
import ParallaxScrollView from 'react-native-parallax-scroll-view';


import {
  getFirebaseNotificationToken,
  checkFirbaseNotificationPermision,
  showFirebaseNotifcation,
  clickOnFirebaseNotification,

} from '../controlls/FirebasePushNotificationControll'


import { checkVersion } from "react-native-check-version";
import Dialog, { DialogContent, DialogTitle ,SlideAnimation} from 'react-native-popup-dialog';


defaultt = [
  {
    name: 'Wait',
    id: 1
  }]

//const version  = await checkVersion();
//console.log("Got version info:", version);
const {
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');
const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
class Home extends Component {

  constructor(props) {
    super(props)
    //this.checkAppVersion()
  }
  state = {
    noConnection: null,
    tabNumber: 1,
    departmentNumber: 1,
    countries: [],
    selectCountry: null,
    cities: [],
    selectCity: null,
    departments: [],
    departments_en: [],
    departments_ar: [],
    ads: [],
    adsLoad: true,
    ads404: null,
    eventDesigns: [],
    eventDesignsLoad: true,
    eventDesigns404: null,
    sliders: [],
    index: 0,
    categories: [],
    categoriesLoad: true,
    categories404: false,
    showCheckVersionDialog: false,

    //search Tab
    countries: [],
    selectCountry: null,
    cities: [],
    selectCity: null,
    departments: [],
    selectDepartment: null,
    providers: [],
    selectCategory: null,
    searchCategories: [],
    showLoggedOutDialog:this.props.loggedOut
  };






  componentDidMount() {

    //console.log("CURRENT USER  ", this.props.currentUser.data)
    enableSideMenu(true, this.props.isRTL)
    this.getCategories()
    this.getAds()
    //this.getSubCategories()
    this.getSlider()
    this.getCountries()

    if (this.props.currentUser) {
      this.props.getOrdersCount(this.props.currentUser.data.token, this.props.currentUser.data.role)
    }
    //console.log('user : ', this.props.currentUser.data)
    this.props.getUnreadNotificationsNumers(this.props.currentUser ? this.props.currentUser.data.token : null)

    checkFirbaseNotificationPermision()
    getFirebaseNotificationToken(this.props.currentUser ? this.props.currentUser.data.token : null)
    showFirebaseNotifcation(this.props.unReaded)
    clickOnFirebaseNotification()

    setInterval(() => {
      if (this.state.tabNumber == 1) {
        this.setState({ index: this.state.index == this.state.sliders.length - 1 ? 0 : this.state.index + 1 })
      }
    }, 3000)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isRTL != this.props.isRTL) {
      this.getCategories()
      //this.getSubCategories()
    }
    //enableSideMenu(true, this.props.isRTL)
  }


  componentWillUnmount() {
    this.props.removeItem()
  }

  getCountries = () => {
    axios.get(`${BASE_END_POINT}locations/countries`)
      .then(response => {
        console.log("DONE CONTRIES   ", response.data)
        const res = response.data.data
        this.setState({
          countries: [
            {
              name: strings.countries,
              id: 0,
              children: res
            }
          ]
        })
        //this.setState({ selectCountry: res[0] })
        //this.getCities(res[0].id)
      })
      .catch(error => {
        console.log("Error   ", error.response)
      })
  }

  getCities = (countryID) => {
    axios.get(`${BASE_END_POINT}locations/cities/${countryID}`)
      .then(response => {
        console.log("DONE CITIES ", response.data.data)
        this.setState({
          cities: [
            {
              name: strings.cities,
              id: 1,
              children: response.data.data
            }
          ],
          //selectCity: response.data.data[0]
        })

      })
      .catch(error => {
        console.log("Error   ", error.response)
      })
  }


  getCategories = () => {
    const { isRTL } = this.props
    axios.get(BASE_END_POINT + 'categories')
      .then(response => {
        console.log('DONE categories    ', response.data.data)
        const res = response.data.data

        var searchCategories = []
        var searchSubCategories = []

        response.data.data.map((itemCat) => {
          itemCat.sub_categories.map((itemSub) => {
            searchSubCategories = [...searchSubCategories, { name: isRTL ? itemSub.ar_name : itemSub.en_name, id: itemSub.id }]
          })
          searchCategories = [...searchCategories, { name: isRTL ? itemCat.name_ar : itemCat.item_en, id: itemCat.id, children: searchSubCategories }]
          searchSubCategories = []
        })

        console.log('searchCategories', searchCategories)

        this.setState({
          categories: res,
          categoriesLoad: false,
          /*searchCategories: [
            {
              name: strings.selectCategory,
              id: 0,
              children: searchCategories
            }
          ],*/
          departments: searchCategories

        })
      })
      .catch(error => {
        console.log('Error   ', error)
        this.setState({ categories404: true, categoriesLoad: false, })
      })
  }


  getAds = () => {
    axios.get(`${BASE_END_POINT}get-ads-categories`)
      .then(response => {
        console.log('Done   ', response.data)
        this.setState({ ads: response.data.data, adsLoad: false, })
      })
      .catch(error => {
        console.log('Error   ', error)
        this.setState({ ads404: true, adsLoad: false, })
      })
  }

  getProviderOnMap = () => {
    const { selectCountry, selectCity, selectDepartment } = this.state
    this.setState({ providers: [] })
    /*var data = {}
    if (selectCountry) {
      data['country_id'] = selectCountry.id
    }
    if (selectCity) {
      data['city_id'] = selectCity.id
    }
    if (selectDepartment) {
      data['sub_category_id'] = selectDepartment.id
    }*/

    var data = new FormData()

    if (selectCountry) {
      data.append('country_id', selectCountry.id)
    }

    if (selectCity) {
      data.append('city_id', selectCity.id)
    }

    if (selectDepartment) {
      data.append('sub_category_id', selectDepartment.id)
    }


    console.log("MY D", data)
    axios.post(`${BASE_END_POINT}search`, data)
      .then(response => {
        console.log('PROVIDERS   ', response.data)
        this.setState({ providers: response.data.data.providers })
        if (response.data.data.providers.length == 0) {
          RNToasty.Info({ title: strings.noResultsFound })
        }
      })
      .catch(error => {
        console.log('PROVIDERS ERROR   ', error.response)
      })

  }

  getSubCategories = () => {
    const { isRTL } = this.props
    axios.get(`${BASE_END_POINT}sub-categories`)
      .then(response => {
        console.log('SUB-CAT Done   ', response.data.data)
        var subCategories = []

        /*response.data.data.map((item) => {
          subCategories.push({id: item.id, name :isRTL? item.name_ar : item.name_en})
          
        })*/
        response.data.data.map((item) => {
          subCategories = [...subCategories, { name: isRTL ? item.name_ar : item.name_en, id: item.id }]
        })

        console.log('DDDFFGGHH: ', subCategories)

        this.setState({
          eventDesigns: response.data.data,
          eventDesignsLoad: false,
          departments: [
            {
              name: strings.categories,
              id: 1,
              children: subCategories
            }
          ],


          //selectDepartment: response.data.data[0]
        })
      })
      .catch(error => {
        console.log('Event Design Error   ', error)
        this.setState({ eventDesigns404: true, eventDesignsLoad: false, })
      })
  }


  getSubCategoriesById = (selectCategory) => {
    const { isRTL } = this.props
    //const {selectCategory} = this.state
    console.log('SUB-CAT Done   ', `${BASE_END_POINT}sub-categories/${selectCategory.id}`)
    axios.get(`${BASE_END_POINT}sub_categories/${selectCategory.id}`)
      .then(response => {

        var subCategories = []

        /*response.data.data.map((item) => {
          subCategories.push({id: item.id, name :isRTL? item.name_ar : item.name_en})
          
        })*/
        response.data.data.map((item) => {
          subCategories = [...subCategories, { name: isRTL ? item.name_ar : item.name_en, id: item.id }]
        })

        console.log('DDDFFGGHH: ', subCategories)

        this.setState({
          eventDesigns: response.data.data,
          eventDesignsLoad: false,
          departments: [
            {
              name: strings.categories,
              id: 1,
              children: subCategories
            }
          ],


          //selectDepartment: response.data.data[0]
        })
      })
      .catch(error => {
        console.log('Event Design Error   ', error)
        this.setState({ eventDesigns404: true, eventDesignsLoad: false, })
      })
  }

  getSlider = () => {
    axios.get(`${BASE_END_POINT}sliders`)
      .then(response => {
        console.log('SLiders Done   ', response.data)
        this.setState({ sliders: response.data.data, })
      })
      .catch(error => {
        console.log('SLiders Error   ', error)
      })
  }

  showDialogOVersion = () => {
    const { isRTL } = this.props
    return (
      <Dialog
        width={responsiveWidth(90)}
        onHardwareBackPress={() => { this.setState({ showCheckVersionDialog: false }) }}
        visible={this.state.showCheckVersionDialog}
      /*onTouchOutside={() => {
          this.setState({ showSentOrderDialog: false });
      }}*/
      >
        <View style={{ width: responsiveWidth(90), marginVertical: moderateScale(10), alignItems: 'center' }}>
          <Text style={{ color: colors.darkBlue, fontSize: responsiveFontSize(8), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont }}>{strings.versionMustBeUpdate}</Text>
          <TouchableOpacity
            onPress={() => {
              /* Platform.OS === 'android' ?
               Linking.openURL('https://play.google.com/store/apps/details?id=com.ash3arimobileapp'):
               Linking.openURL('https://apps.apple.com/us/app/id1523239962'),*/
              this.setState({ showCheckVersionDialog: false })

            }}
            style={{ width: responsiveWidth(30), height: responsiveHeight(6), alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(2), marginTop: moderateScale(3) }}>

            <Text style={{ color: 'white', fontSize: responsiveFontSize(6), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont }}>{strings.ok}</Text>
          </TouchableOpacity>




        </View>
      </Dialog>
    )
  }

  Tab = () => {
    const { isRTL } = this.props
    const { tabNumber } = this.state
    return (
      <View style={{ elevation: 4, zIndex: 10000, shadowOffset: { height: 2, width: 0 }, shadowOpacity: 0.2, paddingHorizontal: moderateScale(0), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-between', alignSelf:'center', height: responsiveHeight(6.5), width: '100%', backgroundColor: colors.white }}>

        <Button
          onPress={() => this.setState({ tabNumber: 1 })}
          style={{ elevation: 0, shadowOffset: { height: 0, width: 0 }, justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: tabNumber == 1 ? colors.blueTabs : colors.black, height: responsiveHeight(6), width: responsiveWidth(24), marginHorizontal: moderateScale(0.5) }}>
          <Text style={{ textAlign: 'center', fontSize: isRTL ? responsiveFontSize(5) : responsiveFontSize(4.7), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, color: colors.white }}>{strings.main}</Text>
        </Button>

        <Button
          onPress={() => this.setState({ tabNumber: 4 })}
          style={{ elevation: 0, shadowOffset: { height: 0, width: 0 }, justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: tabNumber == 4 ? colors.blueTabs : colors.black, height: responsiveHeight(6), width: responsiveWidth(24), marginHorizontal: moderateScale(0.5) }}>
          <Text style={{ textAlign: 'center', fontSize: isRTL ? responsiveFontSize(5) : responsiveFontSize(4.7), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, color: colors.white }}>{strings.mapSearch}</Text>
        </Button>


        <Button
          onPress={() => this.setState({ tabNumber: 2 })}
          style={{ elevation: 0, shadowOffset: { height: 0, width: 0 }, justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: tabNumber == 2 ? colors.blueTabs : colors.black, height: responsiveHeight(6), width: responsiveWidth(24), marginHorizontal: moderateScale(0.5) }}>
          <Text style={{ textAlign: 'center', fontSize: isRTL ? responsiveFontSize(5) : responsiveFontSize(4.7), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, color: colors.white }}>{strings.adds}</Text>
        </Button>

        <Button
          onPress={() => this.setState({ tabNumber: 3 })}
          style={{ elevation: 0, shadowOffset: { height: 0, width: 0 }, justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: tabNumber == 3 ? colors.blueTabs : colors.black, height: responsiveHeight(6), width: responsiveWidth(24), marginHorizontal: moderateScale(0.5) }}>
          <Text style={{ textAlign: 'center', fontSize: isRTL ? responsiveFontSize(5) : responsiveFontSize(4.7), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, color: colors.white }}>{strings.market}</Text>
        </Button>

        {/*<Button
          onPress={() => this.setState({ tabNumber: 4 })}
          style={{ elevation: 0, shadowOffset: { height: 0, width: 0 }, justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: tabNumber == 4 ? colors.blueTabs : colors.black, height:responsiveHeight(10) }}>
          <Text style={{ textAlign: 'center', fontSize: responsiveFontSize(6), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, color: colors.white }}>{strings.eventDesigns}  </Text>
       </Button>*/}


      </View>
    )
  }


  departmentsSection = () => {
    const { isRTL } = this.props
    const { departmentNumber, sliders, index } = this.state
    return (
      <View style={{ width: '100%', marginTop: moderateScale(0), alignSelf:'center' }} >

        {this.searchDepartment()}
        {sliders.length > 0 &&
          <FastImage
            style={{ width: '100%', height: responsiveHeight(30) }}
            source={{ uri: sliders[this.state.index].name }}
          />
        }

        <View style={{ marginTop: moderateScale(-10), flexDirection: 'row', alignSelf: 'center', }}>
          {sliders.map((v, i) => <View style={{ alignSelf: 'center', marginHorizontal: moderateScale(2), width: 10, height: 10, borderRadius: 5, backgroundColor: i == index ? colors.darkBlue : 'gray' }} />)}
        </View>

        <View style={{ marginTop: moderateScale(10), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', marginHorizontal: moderateScale(9), alignSelf: isRTL ? 'flex-end' : 'flex-start' }} >
          <TouchableOpacity style={{ borderBottomColor: departmentNumber == 1 ? colors.green : 'transparent', borderBottomWidth: 2, paddingVertical: moderateScale(3) }}>
            <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), color: colors.black }} >{strings.mainDepartmens}</Text>
          </TouchableOpacity>

          {/*<TouchableOpacity onPress={() => {
            if (departmentNumber != 2) {
              this.setState({ departmentNumber: 2 })
            }
          }} style={{ marginHorizontal: moderateScale(7), borderBottomColor: departmentNumber == 2 ? colors.green : 'transparent', borderBottomWidth: 2, paddingVertical: moderateScale(3) }}>
            <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), color: colors.black }} >{strings.subDepartmens}</Text>
          </TouchableOpacity>*/}
        </View>

        <FlatList

          contentContainerStyle={{ alignItems: isRTL ? 'flex-end' : 'flex-start' }}
          numColumns={2}
          style={{ marginTop: moderateScale(5), alignSelf: 'center' }}
          data={this.state.categories}
          renderItem={({ item }) => <DeapartmentCard data={item} />}
        />

      </View>
    )
  }

  searchDepartment = () => {
    const { isRTL } = this.props
    const { countries, selectCountry, cities, selectCity, searchCategories, departments, selectDepartment, selectCategory } = this.state

    return (
      <>
        <View style={{ paddingVertical: moderateScale(14), alignSelf: 'center', backgroundColor: colors.black, width: '100%'}} >
          <SectionedMultiSelect
            styles={{
              selectToggle: { width: responsiveWidth(80), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', alignSelf: 'center', borderColor: colors.darkGray, borderStyle: 'solid', borderWidth: 0.5, backgroundColor: colors.white },
              selectToggleText: { color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },

              subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              container: { minHeight: responsiveHeight(50), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
              button: { backgroundColor: colors.blueTabs }

            }}

            items={countries}
            alwaysShowSelectText
            single
            uniqueKey="id"
            subKey="children"
            selectText={selectCountry ? selectCountry.name : strings.selectCountry}
            readOnlyHeadings={true}
            expandDropDowns
            showDropDowns={false}
            modalWithTouchable
            hideSearch
            //hideConfirm
            confirmText={strings.close}
            searchPlaceholderText={strings.search}
            onSelectedItemsChange={(selectedItems) => {
              // this.setState({ countries: selectedItems });
            }
            }
            onSelectedItemObjectsChange={(selectedItems) => {
              console.log("ITEM2   ", selectedItems[0].name)
              this.setState({ selectCountry: selectedItems[0] });
              this.getAds(selectedItems[0].id)
              this.getCities(selectedItems[0].id)
            }
            }
          />


          <SectionedMultiSelect
            styles={{
              selectToggle: { marginTop: moderateScale(3), width: responsiveWidth(80), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', alignSelf: 'center', borderColor: colors.darkGray, borderStyle: 'solid', borderWidth: 0.5, backgroundColor: colors.white },
              selectToggleText: { color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
              subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(8), alignSelf: 'center' },
              button: { backgroundColor: colors.blueTabs }
            }}
            items={cities}
            alwaysShowSelectText
            single

            noItemsComponent={<Text style={{ textAlign: 'center', marginTop: moderateScale(20), fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{strings.mustChooseCountryFirst}</Text>}
            uniqueKey="id"
            subKey="children"
            selectText={selectCity ? selectCity.name : strings.selectCity}
            readOnlyHeadings={true}
            expandDropDowns
            showDropDowns={false}
            modalWithTouchable
            hideSearch
            confirmText={strings.close}
            searchPlaceholderText={strings.search}
            onSelectedItemsChange={(selectedItems) => {
              // this.setState({ countries: selectedItems });
            }
            }
            onSelectedItemObjectsChange={(selectedItems) => {
              console.log("ITEM2   ", selectedItems[0].name)
              this.setState({ selectCity: selectedItems[0] });
            }
            }
          />


          {/*<SectionedMultiSelect
            styles={{
              selectToggle: { marginTop: moderateScale(3), width: responsiveWidth(80), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', alignSelf: 'center', borderColor: colors.darkGray, borderStyle: 'solid', borderWidth: 0.5, backgroundColor: colors.white },
              selectToggleText: { color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
              subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(8), alignSelf: 'center' },
            }}
            items={searchCategories}
            alwaysShowSelectText
            single
            uniqueKey="id"
            subKey="children"
            selectText={selectCategory ? selectCategory.name : strings.selectMainCategory}
            readOnlyHeadings={true}
            expandDropDowns
            showDropDowns={false}
            modalWithTouchable
            hideConfirm
            searchPlaceholderText={strings.search}
            onSelectedItemsChange={(selectedItems) => {
              // this.setState({ countries: selectedItems });
            }
            }
            onSelectedItemObjectsChange={(selectedItems) => {
              console.log("ITEM2   ", selectedItems[0].id)
              this.setState({ selectCategory: selectedItems[0], departments: [], selectDepartment: null });
              this.getSubCategoriesById(selectedItems[0])
            }
            }
          />*/}



          <SectionedMultiSelect
            styles={{
              selectToggle: { marginTop: moderateScale(3), width: responsiveWidth(80), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', alignSelf: 'center', borderColor: colors.darkGray, borderStyle: 'solid', borderWidth: 0.5, backgroundColor: colors.white },
              selectToggleText: { color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
              subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(8), alignSelf: 'center' },
              button: { backgroundColor: colors.blueTabs }
            }}
            items={departments}
            alwaysShowSelectText
            single
            uniqueKey="id"
            subKey="children"
            selectText={selectDepartment ? selectDepartment.name : strings.selectCategory}
            readOnlyHeadings={true}
            expandDropDowns
            showDropDowns={false}
            modalWithTouchable
            confirmText={strings.close}
            searchPlaceholderText={strings.search}
            onSelectedItemsChange={(selectedItems) => {
              // this.setState({ countries: selectedItems });
            }
            }
            onSelectedItemObjectsChange={(selectedItems) => {
              console.log("ITEM2   ", selectedItems[0].name)
              this.setState({ selectDepartment: selectedItems[0] });
            }
            }
          />

          <Button onPress={() => {
            var data = {
              providerType: 'bySearch'
            }
            if (selectCity) {
              data['selectCity'] = selectCity.id
            }
            if (selectCountry) {
              data['selectCountry'] = selectCountry.id
            }
            if (selectDepartment) {
              data['selectDepartment'] = selectDepartment.id
            }
            push("ServiceProviders", data)


          }} style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(80), height: responsiveHeight(6), backgroundColor: colors.blueTabs, borderRadius: moderateScale(3), alignSelf: 'center', marginTop: moderateScale(8) }} >
            <Icon name='search' type='FontAwesome' style={{ color: colors.white, fontSize: responsiveFontSize(7) }} />
            <Text style={{ marginHorizontal: moderateScale(0), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{strings.search}</Text>
          </Button>

        </View>


      </>
    )
  }


  adsSection = () => {
    const { ads, ads404, adsLoad } = this.state
    return (
      ads404 ?
        <NetworError />
        :
        adsLoad ?
          <Loading />
          :
          ads.length > 0 ?
            <FlatList
              style={{ marginTop: moderateScale(5), alignSelf: 'center' }}
              data={ads}
              renderItem={({ item }) => <AdsCard data={item} />}
            />
            :
            <NoData />
    )
  }

  marketSection = () => {
    const { isRTL } = this.props
    return (
      <View style={{ marginTop: moderateScale(5), alignSelf: 'center', }}>
        {/*<MarketCard iconName='lightbulb-on' iconType='MaterialCommunityIcons' type='requests' title={strings.requests} />
        <MarketCard iconName='bullhorn' iconType='MaterialCommunityIcons' type='offers' title={strings.offers} />*/}
        {/*<Text style={{ fontsSize: responsiveFontSize(18), fontFamily: isRTL ? arrabicFont : englishFont, color: 'black', marginTop: moderateScale(15) }}>{strings.comingSoon}</Text>*/}
        <NoData />
      </View>
    )
  }


  designsSection = () => {
    const { eventDesigns, eventDesigns404, eventDesignsLoad } = this.state
    return (
      <View>
        <PageTitleLine title={strings.subDepartmens} iconName='axis-x-arrow' iconType='MaterialCommunityIcons' />
        {
          eventDesigns404 ?
            <NetworError />
            :
            eventDesignsLoad ?
              <Loading />
              :
              eventDesigns.length > 0 ?
                <FlatList
                  style={{ alignSelf: 'center', marginVertical: moderateScale(7) }}
                  numColumns={2}

                  data={eventDesigns}
                  renderItem={({ item }) => <DesignsCard data={item} />}
                />
                :
                <NoData />

        }
      </View>
    )
  }

  searchSection = () => {
    const { isRTL } = this.props
    const { countries, selectCountry, cities, selectCity, departments, selectDepartment, searchCategories, selectCategory } = this.state
    return (
      <>
        <View style={{ marginTop: moderateScale(7), alignSelf: 'center' }} >

          <SectionedMultiSelect
            styles={{
              selectToggle: { width: responsiveWidth(70), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', borderColor: colors.darkGray, borderStyle: 'solid', borderWidth: 0.5 },
              selectToggleText: { textAlign: 'center', color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
              subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              container: { minHeight: responsiveHeight(50), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
              button: { backgroundColor: colors.blueTabs }
            }}
            items={countries}
            alwaysShowSelectText
            single
            uniqueKey="id"
            subKey="children"
            selectText={selectCountry ? selectCountry.name : strings.selectCountry}
            readOnlyHeadings={true}
            expandDropDowns
            showDropDowns={false}
            modalWithTouchable
            hideSearch
            confirmText={strings.close}
            searchPlaceholderText={strings.search}
            onSelectedItemsChange={(selectedItems) => {
              // this.setState({ countries: selectedItems });
            }
            }
            onSelectedItemObjectsChange={(selectedItems) => {
              console.log("ITEM2   ", selectedItems[0].name)
              this.setState({ selectCountry: selectedItems[0] });
              this.getCities(selectedItems[0].id)
            }
            }
          />


          <SectionedMultiSelect
            styles={{
              selectToggle: { marginTop: moderateScale(3), width: responsiveWidth(70), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', borderColor: colors.darkGray, borderStyle: 'solid', borderWidth: 0.5 },
              selectToggleText: { textAlign: 'center', color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
              subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(8), alignSelf: 'center' },
              button: { backgroundColor: colors.blueTabs }
            }}
            items={cities}
            alwaysShowSelectText
            single
            noItemsComponent={<Text style={{ textAlign: 'center', marginTop: moderateScale(20), fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{strings.mustChooseCountryFirst}</Text>}
            uniqueKey="id"
            subKey="children"
            selectText={selectCity ? selectCity.name : strings.selectCity}
            readOnlyHeadings={true}
            expandDropDowns
            showDropDowns={false}
            modalWithTouchable
            hideSearch
            confirmText={strings.close}
            searchPlaceholderText={strings.search}
            onSelectedItemsChange={(selectedItems) => {
              // this.setState({ countries: selectedItems });
            }
            }
            onSelectedItemObjectsChange={(selectedItems) => {
              console.log("ITEM2   ", selectedItems[0].name)
              this.setState({ selectCity: selectedItems[0] });
            }
            }
          />

          {/*<SectionedMultiSelect
            styles={{
              selectToggle: { marginTop: moderateScale(3), width: responsiveWidth(70), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', alignSelf: 'center', borderColor: colors.darkGray, borderStyle: 'solid', borderWidth: 0.5, backgroundColor: colors.white },
              selectToggleText: { color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
              subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(8), alignSelf: 'center' },
            }}
            items={items}
            alwaysShowSelectText
            single
            uniqueKey="id"
            subKey="children"
            selectText={selectCategory ? selectCategory.name : strings.selectMainCategory}
            readOnlyHeadings={true}
            expandDropDowns
            showDropDowns={false}
            modalWithTouchable
            hideConfirm
            searchPlaceholderText={strings.search}
            onSelectedItemsChange={(selectedItems) => {
              // this.setState({ countries: selectedItems });
            }
            }
            onSelectedItemObjectsChange={(selectedItems) => {
              console.log("ITEM2   ", selectedItems[0].id)
              this.setState({ selectCategory: selectedItems[0], });
              this.getSubCategoriesById(selectedItems[0])
            }
            }
          />*/}


          <SectionedMultiSelect
            styles={{
              selectToggle: { marginTop: moderateScale(3), width: responsiveWidth(70), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', borderColor: colors.darkGray, borderStyle: 'solid', borderWidth: 0.5 },
              selectToggleText: { textAlign: 'center', color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
              subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(8), alignSelf: 'center' },
              button: { backgroundColor: colors.blueTabs }
            }}
            items={departments}
            alwaysShowSelectText
            single
            uniqueKey="id"
            subKey="children"
            selectText={selectDepartment ? selectDepartment.name : strings.selectCategory}
            readOnlyHeadings={true}
            expandDropDowns
            showDropDowns={false}
            modalWithTouchable
            hideSearch
            confirmText={strings.close}
            searchPlaceholderText={strings.search}
            onSelectedItemsChange={(selectedItems) => {
              // this.setState({ countries: selectedItems });
            }
            }
            onSelectedItemObjectsChange={(selectedItems) => {
              console.log("ITEM2   ", selectedItems[0].name)
              this.setState({ selectDepartment: selectedItems[0] });
            }
            }
          />

          <Button onPress={() => {
            //push('ServiceProviders')
            this.getProviderOnMap()
          }} style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), backgroundColor: colors.darkBlue, borderRadius: moderateScale(3), alignSelf: 'center', marginTop: moderateScale(10) }} >
            <Icon name='search' type='FontAwesome' style={{ color: colors.white, fontSize: responsiveFontSize(7) }} />
            <Text style={{ marginHorizontal: moderateScale(0), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{strings.search}</Text>
          </Button>

        </View>

        {this.map()}
      </>
    )
  }


  map() {
    const { isRTL, data } = this.props
    const { providers } = this.state
    console.log('providers', providers)
    return (
      <View style={{ width: responsiveWidth(93), alignSelf: 'center', marginVertical: moderateScale(8), }}>


        <MapView
          style={{ alignSelf: 'center', width: responsiveWidth(93), height: responsiveHeight(55) }}
          region={{
            latitude: providers.length > 0 ? Number(providers[0].lat) : 24.774265,
            longitude: providers.length > 0 ? Number(providers[0].lng) : 46.738586,
            latitudeDelta: 0.8,
            longitudeDelta: 0.8,

          }}
          showsMyLocationButton = {true}
          
        >
          {providers.length == 0 ?
            <Marker
              coordinate={{
                latitude: 31.1851704,
                longitude: 45.2006749,
                longitudeDelta: 0.8,
                longitudeDelta: 0.8

              }}
              image={require('../assets/imgs/marker.png')}
            />
            :
            providers.map((val, index) => {
              console.log('VAL    ', val)
              return (
                <Marker
                  coordinate={{
                    latitude: parseFloat(val.lat),
                    longitude: parseFloat(val.lng),
                    longitudeDelta: 0.8,
                    longitudeDelta: 0.8

                  }}
                  image={require('../assets/imgs/marker.png')}
                >
                  <Callout onPress={() => {
                    push('ServiceProviderPage', val)
                  }}>
                    <View style={{ borderRadius: moderateScale(8), alignItems: 'center', flexDirection: isRTL ? 'row-reverse' : 'row' }}>
                      <Thumbnail source={val.image ? { uri: val.image } : require('../assets/imgs/profileicon.jpg')} />
                      <View style={{ marginHorizontal: moderateScale(2) }}>
                        <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(5) }} >{val.name}</Text>
                        <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(5), textAlign: 'center' }} >{val.provider_type == 'one' ? strings.person : strings.company}</Text>
                      </View>
                    </View>
                  </Callout>
                </Marker>
              )
            }

            )}
        </MapView>

      </View>
    )
  }

  content = () => {
    const { tabNumber } = this.state
    console.log('LoggedOut',this.props.loggedOut)
    return (
      <View style={{ marginTop: Platform.OS === 'ios' ? (IS_IPHONE_X ? -22 : 0) : 0 }}>
        {this.Tab()}
        {this.showDialogOVersion()}
        {this.showDialogLoggedOut()}




        {tabNumber == 1 && this.departmentsSection()}
        {tabNumber == 2 && this.adsSection()}
        {tabNumber == 3 && this.marketSection()}
        {/*tabNumber == 4 && this.designsSection()*/}
        {tabNumber == 4 && this.searchSection()}
      </View>
    )
  }

  showDialogLoggedOut = () => {
    const { isRTL } = this.props
    return (
        <Dialog
            width={responsiveWidth(90)}
            onHardwareBackPress={() => { this.setState({ showLoggedOutDialog: false }) }}
            visible={this.state.showLoggedOutDialog}
            dialogAnimation ={new SlideAnimation({
              toValue: 400, // optional
              slideFrom: 'bottom', // optional
              useNativeDriver: true, // optional
            })}
        /*onTouchOutside={() => {
            this.setState({ showSentOrderDialog: false });
        }}*/
        >
            <View style={{ width: responsiveWidth(90), marginVertical: moderateScale(10), alignItems: 'center' }}>
                <Text style={{ color: colors.darkBlue, fontSize: responsiveFontSize(8), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.signedOutSuccess}</Text>
                <TouchableOpacity
                    onPress={() => {
                        this.setState({ showLoggedOutDialog: false })
              
                    }}
                    style={{ width: responsiveWidth(30), height: responsiveHeight(6), alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(2), marginTop: moderateScale(3) }}>

                    <Text style={{ color: 'white', fontSize: responsiveFontSize(6), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.finish}</Text>
                </TouchableOpacity>




            </View>
        </Dialog>
    )
}


  render() {
    const { tabNumber } = this.state
    //CheckVersionUpdate()
    return (

      /*<ReactNativeParallaxHeader
        scrollViewProps={{ showsVerticalScrollIndicator: false }}
        headerMinHeight={responsiveHeight(10)}
        headerMaxHeight={responsiveHeight(35)}
        extraScrollHeight={20}
        navbarColor={colors.darkBlue}
        backgroundColor={colors.darkBlue}
        backgroundImage={require('../assets/imgs/header.png')}
        backgroundImageScale={1.2}
        renderNavBar={() => <CollaspeAppHeader title={strings.home} />}
        renderContent={() => this.content()}
        containerStyle={{ flex: 1 }}
        contentContainerStyle={{ flexGrow: 1 }}
        innerContainerStyle={{ flex: 1, }}
      
      />*/


      <ParallaxScrollView
        backgroundColor={colors.darkBlue}
        contentBackgroundColor="white"
        renderFixedHeader={() => <CollaspeAppHeader title={strings.home} />}
        parallaxHeaderHeight={responsiveHeight(35)}
        renderBackground={() => (
          <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

        )}
      >
        {this.content()}
      </ParallaxScrollView>







    )
  }
}


const mapToStateProps = state => ({
  isRTL: state.lang.RTL,
  currentUser: state.auth.currentUser,
  unReaded: state.noti.unReaded,
  unSeen: state.chat.unSeen,
  loggedOut:state.menu.loggedOut
  //currentUser: state.auth.currentUser
})

const mapDispatchToProps = {
  selectMenu,
  getUnreadNotificationsNumers,
  removeItem,
  getUnseenMessages,
  getOrdersCount

}

export default connect(mapToStateProps, mapDispatchToProps)(Home);