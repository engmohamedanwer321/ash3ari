import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import { setUser, login } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../controlls/NavigationControll'
import AnimateLoadingButton from 'react-native-animate-loading-button';
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader';
import { removeItem } from '../actions/MenuActions';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import { RNToasty } from 'react-native-toasty'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig'
import InputValidations from '../common/InputValidations'
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import FastImage from 'react-native-fast-image'

class Login extends Component {

    state = {
        email: ' ',
        password: ' ',
        hidePassword: true,
        loginLoading: false,
        loginError: false,

    }

    componentDidMount() {
        enableSideMenu(false, null)
    }

    componentWillUnmount() {
        this.props.removeItem()
    }

    emailInput = () => {
        const { email } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1500} style={{ marginTop: moderateScale(12), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 1, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='email' type='MaterialCommunityIcons' style={{ color: colors.darkBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        onChangeText={(val) => { this.setState({ email: val }) }}
                        placeholderTextColor={colors.lightGray}
                        placeholder={Strings.email} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {email.length == 0 &&
                    <Text style={{ marginHorizontal: moderateScale(3), color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    passwordInput = () => {
        const { isRTL } = this.props
        const { password, hidePassword } = this.state
        return (
            <Animatable.View animation={isRTL ? "slideInLeft" : "slideInRight"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 1, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='lock1' type='AntDesign' style={{ color: colors.darkBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        secureTextEntry={hidePassword}
                        onChangeText={(val) => { this.setState({ password: val }) }}
                        placeholderTextColor={colors.lightGray}
                        placeholder={Strings.enterYourPassword} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(72), marginHorizontal: moderateScale(0), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                    <TouchableOpacity

                        onPress={() => { this.setState({ hidePassword: !hidePassword }) }}
                    >
                        <Icon style={{ fontSize: responsiveFontSize(6), color: '#D6D6D6' }} name={hidePassword ? 'eye-off' : 'eye'} type='Feather' />
                    </TouchableOpacity>
                </View>
                {password.length == 0 &&
                    <Text style={{ marginHorizontal: moderateScale(3), color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }


    login = () => {
        const { password, email } = this.state
        if (!email.replace(/\s/g, '').length) {
            this.setState({ email: '' })
        }
        if (InputValidations.validateEmail(email) == false) {
            RNToasty.Error({ title: Strings.emailNotValid })
        }
        if (!password.replace(/\s/g, '').length) {
            this.setState({ password: '' })
        }
        if (email.replace(/\s/g, '').length && password.replace(/\s/g, '').length && InputValidations.validateEmail(email) == true) {
            this.setState({ loginLoading: true })
            var data = new FormData()
            data.append('email', email)
            data.append('password', password)

            axios.post(`${BASE_END_POINT}auth/login`, data)
                .then(response => {
                    this.setState({ loginLoading: false })
                    const res = response.data
                    if ('data' in res) {
                        
                        if (res.data.is_verified == 0) {
                            const data = {
                                isVerify: true,
                                userData:res
                            }
                            if (res.data.role == 'client') {
                                push('SignupBeneficiaryMember', data)
                            } else {
                                push('SignupServiceProvider', data)
                            }

                        } else {
                            this.props.setUser(res)
                            console.log('Done  ', res)
                            AsyncStorage.setItem('USER', JSON.stringify(res))
                            resetTo('Home')
                        }
                    } else {
                        console.log('Error  ', res)
                        RNToasty.Error({ title: res.msg })
                    }
                })
                .catch(error => {
                    console.log('Error  ', error.response)
                    console.log('Error  ', error)
                    this.setState({ loginLoading: false })
                })
        }
    }



    loginAndForgetPasswordSection = () => {
        const { isRTL, userToken } = this.props;
        const { password, email } = this.state
        return (
            <View style={{ alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(15) }} >
                <TouchableOpacity onPress={() => { this.login() }} style={{ height: responsiveHeight(7), width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(3) }} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.login}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => { push('ForgetPassword') }} style={{ height: responsiveHeight(7), width: responsiveWidth(40), marginHorizontal: moderateScale(7), justifyContent: 'center', alignItems: 'center', backgroundColor: 'gray', borderRadius: moderateScale(3) }} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.forgetPassword}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    memberSection = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginTop: moderateScale(15), alignSelf: 'center' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ height: 2, width: responsiveWidth(45), backgroundColor: '#cccbcb' }} />
                    <Text style={{ marginHorizontal: moderateScale(1), color: colors.lightGray, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.ash3ariMember}</Text>
                    <View style={{ height: 2, width: responsiveWidth(45), backgroundColor: '#cccbcb' }} />
                </View>
                <Button onPress={() => { push('SignupUserType') }} style={{ alignSelf: 'center', marginTop: moderateScale(12), height: responsiveHeight(7), width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(3) }} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.signup}</Text>
                </Button>
            </View>
        )
    }

    showLoading = () => {
        return (
            this.state.loginLoading &&
            <LoadingDialogOverlay title={Strings.wait} />
        )
    }

    content = () => {
        const { isRTL, barColor } = this.props
        return (
            <View>
                {this.emailInput()}
                {this.passwordInput()}
                {this.loginAndForgetPasswordSection()}
                {this.memberSection()}
               
            </View>
        )
    }

    render() {
        return (
           /* <ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={Strings.login} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/


            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={Strings.login} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
                {this.showLoading()}
            </ParallaxScrollView>
        );
    }
}

const mapDispatchToProps = {
    setUser,
    removeItem,
    login
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    loginLoading: state.auth.loading,
    errorText: state.auth.errorText,
    currentUser: state.auth.currentUser
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(Login);

