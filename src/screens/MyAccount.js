import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, Animated, ScrollView, TouchableOpacity, Text, Modal, ImageBackground, TextInput, Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail, Button } from "native-base";
import { responsiveWidth, moderateScale, responsiveFontSize, responsiveHeight } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import { setUser } from '../actions/AuthActions';
import { BASE_END_POINT } from '../AppConfig'
import axios from 'axios';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import strings from '../assets/strings';
import { enableSideMenu, push, resetTo } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors'
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import PageTitleLine from '../components/PageTitleLine'
import InputValidations from '../common/InputValidations'
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import FastImage from 'react-native-fast-image'
import ImagePicker from 'react-native-image-crop-picker';
import ParallaxScrollView from 'react-native-parallax-scroll-view';


class MyAccount extends Component {

    countryKeys = [
        // this is the parent or 'item'
        {
            name: strings.countryKey,
            id: 0,
            // these are the children or 'sub items'
            children: [
                {
                    name: '966',
                    id: 966,
                },
                {
                    name: '971',
                    id: 971,
                },
                {
                    name: '965',
                    id: 965,
                },
                {
                    name: '973',
                    id: 973,
                },
                {
                    name: '968',
                    id: 968,
                },
            ],
        },
    ];



    state = {
        name: this.props.currentUser.data.name,
        email: this.props.currentUser.data.email,
        selectCountryKey: this.props.currentUser.data.phone.substring(0, 3).toString(),
        phone: this.props.currentUser.data.phone,
        password: ' ',
        img: null,
        hidePassword: false,
        countries: [],
        selectCountry: null,
        cities: [],
        selectCity: null,
        loading: false,
        regions: [],
        selectRegion: null
    }

    componentWillUnmount() {
        this.props.removeItem()
    }

    componentDidMount() {
        enableSideMenu(true, this.props.isRTL)
        this.getCountries()



    }

    componentDidUpdate() {
        enableSideMenu(true, this.props.isRTL)
    }

    getCountries = () => {
        axios.get(`${BASE_END_POINT}locations/countries`)
            .then(response => {
                console.log("DONE   ", response.data)
                this.setState({
                    countries: [
                        {
                            name: strings.countries,
                            id: 0,
                            children: response.data.data
                        }
                    ]
                })
                response.data.data.map(val => {
                    if (this.props.currentUser.data.country_id == val.id) {
                        this.setState({ selectCountry: val })
                        this.getCities(val.id)
                        console.log("equle   ")
                        return
                    }
                })
            })
            .catch(error => {
                console.log("Error   ", error.response)
            })
    }

    getCities = (countryID) => {
        axios.get(`${BASE_END_POINT}locations/cities/${countryID}`)
            .then(response => {
                console.log("DONE CITIES   ", response.data.data)
                this.setState({
                    cities: [
                        {
                            name: strings.cities,
                            id: 1,
                            children: response.data.data
                        }
                    ]
                })
                response.data.data.map(val => {
                    if (this.props.currentUser.data.city_id == val.id) {
                        this.setState({ selectCity: val })
                        this.getRegions(val.id)
                        console.log("equle   ")
                        return
                    }
                })
            })
            .catch(error => {
                console.log("Error   ", error.response)
            })
    }


    getRegions = (cityID) => {
        axios.get(`${BASE_END_POINT}locations/regions/${cityID}`)
            .then(response => {
                console.log("DONE REGIONS   ", response.data.data)
                this.setState({
                    regions: [
                        {
                            name: strings.regions,
                            id: 1,
                            children: response.data.data
                        }
                    ]
                })
                response.data.data.map(val => {
                    if (this.props.currentUser.data.region_id == val.id) {
                        this.setState({ selectRegion: val })
                        console.log("equle   ")
                        return
                    }
                })
            })
            .catch(error => {
                console.log("Error   ", error.response)
            })
    }

    nameInput = () => {
        const { name } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1500} style={{ marginTop: moderateScale(10), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(2), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='user-alt' type='FontAwesome5' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        value={name}
                        onChangeText={(val) => { this.setState({ name: val }) }}
                        placeholder={Strings.name} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {name.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }


    emailInput = () => {
        const { email } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={isRTL ? "slideInLeft" : "slideInRight"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='email' type='MaterialCommunityIcons' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        value={email}
                        onChangeText={(val) => { this.setState({ email: val }) }}
                        placeholder={Strings.email} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {email.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    countryKeyInput = () => {
        const { countryKey, selectCountryKey } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='telephone-accessible' type='Foundation' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(78), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', color: 'white' },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { height: responsiveHeight(45), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(22), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={this.countryKeys}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        selectText={this.state.selectCountryKey}
                        readOnlyHeadings={true}
                        expandDropDowns
                        showDropDowns={false}
                        modalWithTouchable
                        hideSearch
                        confirmText={strings.close}
                        searchPlaceholderText={Strings.search}
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }
                        onSelectedItemObjectsChange={(selectedItems) => {
                            console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectCountryKey: selectedItems[0].name });


                        }
                        }
                    />
                </View>
                {selectCountryKey.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    phoneInput = () => {
        const { phone } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='phone' type='FontAwesome' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        value={phone.split(phone.substring(0, 3).toString())[1]}
                        onChangeText={(val) => { this.setState({ phone: val }) }}
                        placeholder={Strings.phone} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {phone.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    passwordInput = () => {
        const { isRTL } = this.props
        const { password, hidePassword } = this.state
        return (
            <Animatable.View animation={isRTL ? "slideInLeft" : "slideInRight"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='lock1' type='AntDesign' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        secureTextEntry={hidePassword}
                        onChangeText={(val) => { this.setState({ password: val }) }}
                        placeholder={Strings.enterYourPassword} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(72), marginHorizontal: moderateScale(0), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                    <TouchableOpacity

                        onPress={() => { this.setState({ hidePassword: !hidePassword }) }}
                    >
                        <Icon style={{ fontSize: responsiveFontSize(6), color: '#D6D6D6' }} name={hidePassword ? 'eye-off' : 'eye'} type='Feather' />
                    </TouchableOpacity>
                </View>
                {password.length == 0 &&
                    <Text style={{ marginHorizontal: moderateScale(3), color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    countryAndCityPickers = () => {
        var test = 'هلا '
        const { isRTL } = this.props
        const { countries, cities, regions } = this.state
        return (
            <View style={{ marginTop: moderateScale(7), alignSelf: 'center' }} >

                {/*country */}
                <Animatable.View animation={isRTL ? "slideInRight" : "slideInLeft"} duration={1500} style={{ marginTop: moderateScale(2), width: responsiveWidth(90), height: responsiveHeight(8), borderRadius: moderateScale(3), alignSelf: 'center', borderWidth: 0.5, borderColor: colors.darkBlue, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>

                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='google-maps' type='MaterialCommunityIcons' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>

                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(77), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                            selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { height: responsiveHeight(60), position: 'absolute', width: responsiveWidth(85), top: responsiveHeight(20), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={countries}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        selectText={this.state.selectCountry ? this.state.selectCountry.name : strings.selectCountry}
                        readOnlyHeadings={true}
                        expandDropDowns
                        showDropDowns={false}
                        modalWithTouchable
                        hideSearch
                        confirmText={strings.close}
                        searchPlaceholderText={Strings.search}
                        onSelectedItemsChange={(selectedItems) => { }}
                        onSelectedItemObjectsChange={(selectedItems) => {
                            console.log("ITEM2   ", selectedItems[0])
                            this.getCities(selectedItems[0].id)
                            this.setState({ selectCountry: selectedItems[0], selectCity: null });

                        }
                        }
                    />

                </Animatable.View>

                {/*city */}
                <Animatable.View animation={isRTL ? "slideInLeft" : "slideInRight"} duration={1500} style={{ marginTop: moderateScale(9), width: responsiveWidth(90), alignSelf: 'center', borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8), borderRadius: moderateScale(3), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>

                    <View style={{ marginHorizontal: moderateScale(5) }}>
                        <Icon name='map-marker' type='FontAwesome' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>

                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(77), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center' },
                            selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(85), top: responsiveHeight(20), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={cities}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        selectText={this.state.selectCity ? this.state.selectCity.name : strings.selectCity}
                        readOnlyHeadings={true}
                        expandDropDowns
                        showDropDowns={false}
                        modalWithTouchable
                        hideSearch
                        confirmText={strings.close}
                        searchPlaceholderText={Strings.search}
                        onSelectedItemsChange={(selectedItems) => { }}
                        onSelectedItemObjectsChange={(selectedItems) => {
                            console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectCity: selectedItems[0] });
                        }
                        }
                    />

                </Animatable.View>

                {/*region 
            <Animatable.View animation={isRTL?"slideInLeft":"slideInRight"} duration={1500} style={{marginTop:moderateScale(9), width:responsiveWidth(90),alignSelf:'center', borderWidth:1,borderColor:colors.darkBlue,height:responsiveHeight(8),borderRadius:moderateScale(3), flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                <View style={{marginHorizontal:moderateScale(5)}}>
                <Icon name='map-marker' type='FontAwesome' style={{color:colors.mediumBlue,fontSize:responsiveFontSize(8)}} />
                </View>

                <SectionedMultiSelect
                styles={{
                    selectToggle:{width:responsiveWidth(77),height:responsiveHeight(6),borderRadius:moderateScale(3),alignItems:'center',justifyContent:'center'},
                    selectToggleText: {textAlign:isRTL?'right':'left', fontSize:responsiveFontSize(6), color:'gray', marginTop:moderateScale(6), fontFamily:isRTL?arrabicFont:englishFont},
                }}
                items={regions}
                alwaysShowSelectText
                single
                uniqueKey="id"
                subKey="children"
                searchPlaceholderText='search...'
                selectText={this.state.selectRegion?this.state.selectRegion.name:strings.selectRegion}
                showDropDowns={true}
                readOnlyHeadings={true}
                onSelectedItemsChange={(selectedItems) => {}}
                onSelectedItemObjectsChange={(selectedItems) => {
                    console.log("ITEM2   ",selectedItems[0].name)
                    this.setState({ selectRegion: selectedItems[0]});
                }
                }
                />

            </Animatable.View>
                */}
            </View>
        )
    }


    save = () => {
        const { loading, selectRegion, img, selectCity, selectCountry, name, email, phone, password, selectCountryKey } = this.state
        const { currentUser, isRTL } = this.props;
        if (!name.replace(/\s/g, '').length) {
            this.setState({ name: '' })
        }
        if (!email.replace(/\s/g, '').length) {
            this.setState({ email: '' })
        }
        if (!phone.replace(/\s/g, '').length) {
            this.setState({ phone: '' })
        }
        if (!password.replace(/\s/g, '').length) {
            this.setState({ password: '' })
        }
        if (!selectCity) {
            RNToasty.Error({ title: Strings.selectCity })
        }
        if (!selectCountry) {
            RNToasty.Error({ title: Strings.selectCountry })
        }
        /*if(!selectRegion){
            RNToasty.Error({ title: Strings.selectRegion })
        }*/

        if (selectCountry && selectCity && name.replace(/\s/g, '').length && email.replace(/\s/g, '').length && phone.replace(/\s/g, '').length && password.replace(/\s/g, '').length) {
            this.setState({ loading: true })
            var data = new FormData()
            data.append('email', email)
            data.append('name', name)
            data.append('code', selectCountryKey)
            data.append('phone', phone)
            data.append('password', password)
            data.append('country_id', selectCountry.id)
            data.append('city_id', selectCity.id)
            if (img) {
                data.append('image', {
                    uri: img,
                    type: 'multipart/form-data',
                    name: 'image'
                })
            }
            //data.append('region_id', selectRegion.id)


            axios.post(`${BASE_END_POINT}user/update/profile`, data, {
                headers: {
                    Authorization: `Bearer ${this.props.currentUser.data.token}`
                }
            })
                .then(response => {
                    this.setState({ loading: false })
                    const res = response.data
                    if ('data' in res) {
                        console.log('Done  ', res)
                        AsyncStorage.setItem('USER', JSON.stringify(res))
                        this.props.setUser(res)
                        RNToasty.Success({ title: Strings.updateProfileSuccess })
                        resetTo('Home')
                    } else {
                        console.log('Error  ', res)
                        RNToasty.Error({ title: res.msg })
                    }
                })
                .catch(error => {
                    console.log('Error  ', error.response)
                    this.setState({ loading: false })
                })
        }
    }

    sendButton = () => {
        const { name, email, phone, password } = this.state
        const { currentUser, isRTL } = this.props;
        return (
            <Button onPress={() => { this.save() }} style={{ marginVertical: moderateScale(18), width: responsiveWidth(40), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }}>
                <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.save}</Text>
            </Button>
        )
    }

    pageLabel = () => {
        const { isRTL } = this.props;
        return (
            <View style={{ padding: moderateScale(5), marginVertical: moderateScale(7), flexDirection: isRTL ? 'row-reverse' : 'row', marginHorizontal: moderateScale(10), alignItems: 'center', borderBottomColor: colors.green, borderBottomWidth: 2, alignSelf: isRTL ? 'flex-end' : 'flex-start' }}>

                <Icon name='user' type='FontAwesome5' style={{ color: 'gray', fontSize: responsiveFontSize(8) }} />
                <Text style={{ marginHorizontal: moderateScale(5), color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.myAccount}</Text>
            </View>
        )
    }

    showLoading = () => {
        return (
            this.state.loading &&
            <LoadingDialogOverlay title={Strings.wait} />
        )
    }

    imageView = () => {
        const { img } = this.state
        return (
            <TouchableOpacity
                onPress={() => {
                    ImagePicker.openPicker({
                        width: 300,
                        height: 400,
                        cropping: true
                    }).then(image => {
                        console.log(image);
                        this.setState({ img: image.path })
                    });
                }}
                style={{ alignSelf: 'center', marginTop: moderateScale(10) }}>
                <FastImage
                    style={{ width: 80, height: 80, borderRadius: 40 }}
                    source={img ? { uri: img } : this.props.currentUser.data.image ? { uri: this.props.currentUser.data.image } : require('../assets/imgs/profileicon.jpg')}
                />
                <View style={{ marginTop: moderateScale(-12), alignSelf: 'flex-end', justifyContent: 'center', alignItems: 'center', width: 40, height: 40, borderRadius: 20, backgroundColor: 'gray' }}>
                    <Icon name='camera' type='FontAwesome' style={{ color: 'white', fontSize: responsiveFontSize(8) }} />
                </View>
            </TouchableOpacity>
        )
    }

    content = () => {
        return (
            <View>
                <PageTitleLine title={Strings.accountMangement} iconName='user-alt' iconType='FontAwesome5' />
                {this.imageView()}
                {this.nameInput()}
                {this.emailInput()}
                {this.countryKeyInput()}
                {this.phoneInput()}
                {this.passwordInput()}
                {this.countryAndCityPickers()}
                {this.sendButton()}
                {this.showLoading()}
            </View>
        )
    }

    render() {
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={strings.myAccount} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/


            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={strings.myAccount} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
        );
    }
}



const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color
})


export default connect(mapToStateProps, mapDispatchToProps)(MyAccount);

