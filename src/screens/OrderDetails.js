import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, FlatList, TouchableOpacity, Text, RefreshControl, TextInput, Alert
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail, Button } from "native-base";
import { responsiveWidth, moderateScale, responsiveFontSize, responsiveHeight } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import { getUser } from '../actions/AuthActions';
import { getOrdersCount } from '../actions/OrdersActions'
import { BASE_END_POINT } from '../AppConfig'
import axios from 'axios';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import strings from '../assets/strings';
import { enableSideMenu, push , pop} from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors'
import OrderCard from '../components/OrderCard'
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import PageTitleLine from '../components/PageTitleLine'
import Dialog, { DialogTitle, DialogContent } from 'react-native-popup-dialog';
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import RNPrint from 'react-native-print';
import { ScrollView } from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import FastImage from 'react-native-fast-image'
import ImgToBase64 from 'react-native-image-base64';


class OrderDetails extends Component {
    page = 1

    state = {
        //pending
        orders: [],
        ordersLoad: true,
        ordersRefresh: false,
        orders404: null,
        pages: 0,

        dialogOrderData: null,
        showDialog: false,
        reason: 'no needed',
        tabNumber: 1,
    }

    componentWillUnmount() {
        this.props.removeItem()
    }

    componentDidMount() {
        const { currentUser } = this.props
        enableSideMenu(false, this.props.isRTL)
        this.getOrders(false, 1, 1)
        this.props.getOrdersCount(currentUser.data.token, currentUser.data.role)
    }

    componentDidUpdate() {
        const { currentUser } = this.props
        enableSideMenu(false, this.props.isRTL)
        this.props.getOrdersCount(currentUser.data.token, currentUser.data.role)
    }

    //pending
    getOrders = (refresh, page, tabNumber) => {
        var url;
        if (tabNumber == 1) {
            url = `${BASE_END_POINT}client/ordered/pending?page=${page}`
            console.log('1')
        } else if (tabNumber == 2) {
            url = `${BASE_END_POINT}client/ordered/accepted?page=${page}`
            console.log('2')
        } else if (tabNumber == 3) {
            url = `${BASE_END_POINT}client/ordered/finished?page=${page}`
            console.log('3')
        } else {
            url = `${BASE_END_POINT}client/ordered/canceled?page=${page}`
            console.log('4')
        }


        if (refresh) {
            this.setState({ ordersRefresh: true })
        }
        axios.get(url, {
            headers: {
                Authorization: `Bearer ${this.props.currentUser.data.token}`
            }
        })
            .then(response => {
                console.log('Done   ', response.data.data.orders)
                this.setState({
                    orders: refresh ? response.data.data.orders : [...this.state.orders, ...response.data.data.orders],
                    ordersLoad: false,
                    ordersRefresh: false,
                    pages: response.data.data.paginate.total_pages,
                })
            })
            .catch(error => {
                console.log('Error   ', error.response)
                this.setState({ orders404: true, ordersLoad: false, })
            })
    }

    cancelOrder = (id) => {
        var data = new FormData()
        data.append('order_id', id)
        //data.append('reason', reason)
        axios.post(`${BASE_END_POINT}client/ordered/cancel`, data, {
            headers: {
                Authorization: `Bearer ${this.props.currentUser.data.token}`,
            }
        })
            .then(response => {
                console.log('CANCEL  ', response)
                this.page = 1
                this.getOrders(true, 1, 1)
                //this.setState({ordersRefresh:true})
            })
            .catch(error => {
                console.log('ERROR  ', error.response)
            })
    }

    item = (key, val, isButton, data) => {
        const { isRTL } = this.props
        return (
            <View style={{ borderBottomWidth: 0, borderBottomColor: '#cccbcb', marginTop: moderateScale(0), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(90), height: responsiveHeight(5), marginVertical: isButton ? moderateScale(5) : 0 }} >
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: colors.sky, textAlign: 'center', marginHorizontal: moderateScale(6) }} >{key} : </Text>
                {isButton ?

                    <TouchableOpacity style={{ borderWidth: 2, borderRadius: moderateScale(3), borderColor: colors.lightGray, padding: moderateScale(2) }} onPress={() => { this.setState({ dialogOrderData: data, showDialog: true }) }}>
                        <Text style={{ fontFamily: englishFont, fontSize: responsiveFontSize(6), color: colors.greenApp, textAlign: 'center', marginHorizontal: moderateScale(0) }} >{val}</Text>

                    </TouchableOpacity>
                    :
                    <Text style={{ fontFamily: englishFont, fontSize: responsiveFontSize(6), color: 'gray', textAlign: 'center', marginHorizontal: moderateScale(0) }} >{val}</Text>
                }
            </View>
        )
    }

    orderDetailsItem = (key, val,) => {
        const { isRTL } = this.props
        return (
            <View style={{ marginTop: moderateScale(6), borderBottomColor: '#cccbcb', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', alignSelf: 'center', width: responsiveWidth(90), justifyContent: 'space-between' }} >
                <Text style={{ width: responsiveWidth(25), fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: colors.sky, textAlign: isRTL ? 'right' : 'left' }} >{key} : </Text>
                <View style={{ width: responsiveWidth(63), borderWidth: 1, borderColor: colors.lightGray, borderRadius: moderateScale(2), justifyContent: 'center' }}>
                    <Text style={{ textAlign: isRTL ? 'right' : 'left', fontFamily: englishFont, fontSize: responsiveFontSize(6), color: 'gray', marginHorizontal: moderateScale(2) }} >{val}</Text>
                </View>
            </View>
        )
    }

    print(data) {

        ImgToBase64.getBase64String('https://www.sheari.net/img/logo.png')
            .then(base64String => this.printBill(base64String))
            .catch(err => console.log(err));

    }

    printBill = async (base64String) => {
        const { data } = this.props
        console.log('base64String', data)
        var s;
        if (this.state.tabNumber == 1) {
            s = Strings.pending
        } else if (this.state.tabNumber == 2) {
            s = Strings.accepted
        } else if (this.state.tabNumber == 3) {
            s = Strings.finished
        } else {
            s = Strings.canceld
        }
        var recive = `
        <html>
        <head>
            <style>
                .parent{
                    display: flex;
                    flex: 1;
                    flex-direction: column;
                    margin-bottom: 30px;
                }
                .title{
                   border-bottom: 3px solid #16476A;
                    padding: 10px;
                    color: #16476A;
                    align-self: center;
                }
                .item{
                    margin-top: 15px;
                    margin-right: 10px;
                    align-self: flex-end;
                    border-bottom: 1px solid #cccbcb;
                    padding-bottom: 5px;
                    
                    
                }
                .itemKey{
                    font-size: 24px;
                    font-weight: 700;
                   
                   
                   
                }
                .itemVal{
                    font-size: 18px;
                    font-weight: 700;
                    margin-right: 5px;
                    
                }
            </style>
        </head>
        <body>
            <div class="parent" >
                <h1 class="title" >فتورة الطلب </h1>
               
                
                <img src="data:image/png;base64, ${base64String}" width="${responsiveWidth(50)}" height="${responsiveWidth(50)}" style="align-self:center;" >
            
<h2 style="width:90%; text-align:center; align-self:center">نذكركم بدفع عمولة شعاري
(5% للعضوية العادية و 2% لعضوية التميز)
عبر صفحة الدفع</h2>
                <div class="item">
                    <span class="itemKey">عنوان الطلب :</span>
                    <span class="itemVal">${data.title}</span>
                </div>
                
                <div class="item">
                    <span class="itemKey">طلب رقم :</span>
                    <span class="itemVal">${data.id}</span>
                </div>
                
                 <div class="item">
                    <span class="itemKey">تاريخ الطلب :</span>
                    <span class="itemVal">${data.full_date}</span>
                </div>
                
                 <div class="item">
                    <span class="itemKey">حالة الطلب :</span>
                    <span class="itemVal">${s}</span>
                </div>
                
                 
                
                 <div class="item">
                    <span class="itemKey">السعر :</span>
                    <span class="itemVal">${data.expected_money} ريال سعودى</span>
                </div>
                
                 <div class="item">
                    <span class="itemKey">بيانات الطلب :</span>
                    <span class="itemVal">${data.details}</span>
                </div>
                
                
                <div class="item">
                <span class="itemKey"> مقدم الطلب :</span>
                <span class="itemVal">${this.props.currentUser.data.name}</span>
                </div>
                
             
            
                
                <div class="item">
                    <span class="itemKey">اسم مقدم الخدمة :</span>
                    <span class="itemVal">${data.provider.name}</span>
                </div>
            
            
                
            </div>
        </body>
    </html>`

        await RNPrint.print({
            html: recive
        })
    }



    orderDetails = () => {
        const { isRTL, navigator, data } = this.props;
        const { dialogOrderData } = this.state


        return (

            <View style={{ width: responsiveWidth(90), height: responsiveHeight(85) }}>
                <View style={{ backgroundColor: colors.darkBlue, justifyContent: 'center', alignItems: 'center', width: responsiveWidth(90), height: responsiveHeight(7) }}>
                    <Text style={{ fontFamily: englishFont, fontSize: responsiveFontSize(6), color: 'white', textAlign: 'center', marginHorizontal: moderateScale(0) }} >{Strings.orderDetails}</Text>
                </View>
                <ScrollView >
                    {this.orderDetailsItem(Strings.name, data.provider.name)}
                    {this.orderDetailsItem(Strings.phone, data.provider.phone)}
                    {this.orderDetailsItem(Strings.orderTitle, data.title)}
                    {this.orderDetailsItem(Strings.orderNumber, data.id)}
                    {this.orderDetailsItem(Strings.orderDate, data.full_date)}
                    {this.orderDetailsItem(Strings.orderStatus, data.status)}
                    {/*this.orderDetailsItem(Strings.category,dialogOrderData.a)*/}
                    {/*this.orderDetailsItem(Strings.expectedTime, dialogOrderData.expected_time)*/}
                    {this.orderDetailsItem(Strings.sallry, data.expected_money)}
                    {this.orderDetailsItem(Strings.details, data.details)}
                </ScrollView>


                {data.status == 'pending' &&
                    <View style={{ marginVertical: moderateScale(6), justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>


                        <Button
                            style={{ marginTop: moderateScale(0), height: responsiveHeight(6), width: responsiveWidth(25), borderRadius: moderateScale(3), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue }}
                            onPress={() => {
                                this.setState({ showDialog: false })
                                this.cancelOrder(data.id)
                            }}>
                            <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: 'white', textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.cancel}</Text>
                        </Button>
                        {/*<Button style={{height:responsiveHeight(6), width:responsiveWidth(25),borderRadius:moderateScale(3), justifyContent:'center',alignItems:'center',backgroundColor:colors.sky}} onPress={()=>{this.setState({showDialog:false})}}>
                        <Text style={{ fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),color:'white',textAlign:'center',marginHorizontal:moderateScale(6)}} >{Strings.refuse}</Text> 
                    </Button>
                    */}
                    </View>
                }

            </View>

        )
    }

    acceptOrder = (id, reason) => {
        var data = new FormData()
        data.append('order_id', id)
        data.append('reason', '')
        axios.post(`${BASE_END_POINT}provider/ordered/accept`, data, {
            headers: {
                Authorization: `Bearer ${this.props.currentUser.data.token}`,
            }
        })
            .then(response => {

                this.page = 1
                this.getOrders(true, 1, 1)
                //this.setState({ordersRefresh:true})
            })
            .catch(error => {

            })
    }

    finishOrder = (id, reason) => {
        var data = new FormData()
        data.append('order_id', id)
        data.append('reason', '')
        axios.post(`${BASE_END_POINT}provider/ordered/finish`, data, {
            headers: {
                Authorization: `Bearer ${this.props.currentUser.data.token}`,
            }
        })
            .then(response => {

                this.page = 1
                this.getOrders(true, 1, 1)
                pop()
                //this.setState({ordersRefresh:true})
            })
            .catch(error => {

            })
    }

    orderProviderDetails = () => {
        const { isRTL, navigator, data, currentUser } = this.props;
        const { dialogOrderData, enablePrice, enableReason } = this.state
        var s;
        if (data.status == 'pending') {
            s = Strings.pending
        } else if (data.status == 'accepted') {
            s = Strings.accepted
        } else if (data.status == 'finished') {
            s = Strings.finished
        } else {
            s = Strings.canceld
        }
        console.log('DfDF:', data)
        return (

            <View style={{ width: responsiveWidth(90), marginTop: moderateScale(10), alignSelf: 'center', }}>
  

                {this.orderDetailsItem(Strings.name, data.user.name)}
                {this.orderDetailsItem(Strings.phone, data.user.phone)}
                {this.orderDetailsItem(Strings.orderTitle, data.title)}
                {this.orderDetailsItem(Strings.orderNumber, data.id)}
                {this.orderDetailsItem(Strings.orderDate, data.full_date)}
                {this.orderDetailsItem(Strings.orderStatus, s, data)}
                {/*this.orderDetailsItem(Strings.category,dialogOrderData.a)*/}
                {/*this.orderDetailsItem(Strings.expectedTime, dialogOrderData.expected_time)*/}
                {this.orderDetailsItem(Strings.sallry, data.expected_money)}
                {this.orderDetailsItem(Strings.details, data.details)}



                {data.status == 'pending' &&
                    <View>

                        {/*<View style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'flex-end',alignSelf:'center',width:responsiveWidth(70)}}>
                       
                        <TextInput 
                        
                        editable={enableReason}
                        onChangeText={(val)=>{this.setState({reason:val})}}
                        placeholder={Strings.reason}
                        style={{alignSelf:'center',marginTop:moderateScale(8), padding:0,paddingHorizontal:moderateScale(3),textAlign:isRTL?'right':'left', borderBottomWidth:1,borderBottomColor:enableReason?'green':colors.lightGray, width:responsiveWidth(60)}}
                        />
                         <TouchableOpacity
                        onPress={()=>{
                            this.setState({enableReason:!enableReason})
                        }} 
                        >
                            <Icon name='edit' type='AntDesign' style={{fontSize:responsiveFontSize(9),color:enableReason?'green':colors.darkGray}} />
                        </TouchableOpacity>
                    </View>*/}


                        {currentUser.data.is_special == 1 &&
                            <></>
                            /*<View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', alignItems: 'flex-end', alignSelf: 'center', width: responsiveWidth(70) }}>
                                <TextInput

                                    editable={enablePrice}
                                    onChangeText={(val) => { this.setState({ price: val }) }}
                                    placeholder={Strings.newPrice}
                                    style={{ alignSelf: 'center', marginTop: moderateScale(8), padding: 0, paddingHorizontal: moderateScale(3), textAlign: isRTL ? 'right' : 'left', borderBottomWidth: 1, borderBottomColor: enablePrice ? 'green' : colors.lightGray, width: responsiveWidth(60) }}
                                />
                                <TouchableOpacity
                                    onPress={() => {

                                        this.changePrice(data.id, this.state.price)

                                    }}
                                >
                                    <Icon name='send' type='MaterialCommunityIcons' style={{ fontSize: responsiveFontSize(10), color: enablePrice ? 'green' : colors.darkGray, transform: [{ rotateY: isRTL ? '180deg' : '0deg' }] }} />
                                </TouchableOpacity>
                            </View>*/


                        }
                    </View>
                }


                {data.status == 'pending' &&
                    <View style={{ marginBottom: moderateScale(10), flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(85), marginTop: moderateScale(25), justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center', }}>


                        <TouchableOpacity
                            style={{ height: responsiveHeight(6), width: responsiveWidth(25), borderRadius: moderateScale(3), justifyContent: 'center', alignItems: 'center', backgroundColor: 'gray' }}
                            onPress={() => {
                                this.setState({ showDialog: false })
                                this.page = 1
                                this.cancelOrder(data.id, this.state.reason)
                            }}>
                            <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: 'white', textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.cancelOrder}</Text>
                        </TouchableOpacity>


                        <TouchableOpacity
                            onPress={() => {
                                push('Chat', data)
                            }}
                            style={{ height: responsiveHeight(6), borderRadius: moderateScale(3), width: responsiveWidth(25), backgroundColor: colors.greenApp, flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: 'white', textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.chat}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                this.print(data)
                            }}
                            style={{  borderRadius: responsiveWidth(5), height: responsiveWidth(10), width: responsiveWidth(10), backgroundColor: colors.sky, flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='print' type='FontAwesome' style={{ fontSize: responsiveFontSize(9), color: 'white', }} />
                        </TouchableOpacity>

                    </View>
                }



                {data.status == 'accepted' &&
                    <View style={{ marginBottom: moderateScale(10), marginTop: moderateScale(5), width: responsiveWidth(80), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between' }}>
                        <Button
                            style={{ height: responsiveHeight(6), width: responsiveWidth(25), borderRadius: moderateScale(3), justifyContent: 'center', alignItems: 'center', backgroundColor: 'gray' }}
                            onPress={() => {
                                this.setState({ showDialog: false })
                                this.page = 1
                                this.finishOrder(data.id, this.state.reason)
                            }}>
                            <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: 'white', textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.finish}</Text>
                        </Button>


                        <TouchableOpacity
                                onPress={() => {
                                    push('Chat', data)
                                }}
                                style={{ borderRadius: moderateScale(3), height: responsiveHeight(6), width: responsiveWidth(25), backgroundColor: colors.greenApp, flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: 'white', textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.chat}</Text>
                            </TouchableOpacity>


                        <TouchableOpacity
                            onPress={() => {
                                this.print(data)
                            }}
                            style={{ borderRadius: responsiveWidth(5), height: responsiveWidth(10), width: responsiveWidth(10), backgroundColor: colors.sky, flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='print' type='FontAwesome' style={{ fontSize: responsiveFontSize(9), color: 'white', }} />
                        </TouchableOpacity>

                    </View>
                }


                {(data.status == 'finished' || data.status == 'canceled') &&
                    <View style={{ marginBottom: moderateScale(10), marginTop: moderateScale(10), width: responsiveWidth(80), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between' }}>


                        {(data.status == 'pending' || data.status == 'finished') &&
                            <TouchableOpacity
                                onPress={() => {
                                    push('Chat', data)
                                }}
                                style={{ borderRadius: moderateScale(3), height: 40, width: responsiveWidth(25), backgroundColor: colors.greenApp, flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: 'white', textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.chat}</Text>
                            </TouchableOpacity>
                        }

                        <TouchableOpacity
                            onPress={() => {
                                this.print(data)
                            }}
                            style={{ borderRadius: responsiveWidth(5), height: responsiveWidth(10), width: responsiveWidth(10), backgroundColor: colors.sky, flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='print' type='FontAwesome' style={{ fontSize: responsiveFontSize(9), color: 'white', }} />
                        </TouchableOpacity>

                    </View>
                }

            </View>

        )
    }


    content = () => {
        const { isRTL, data, currentUser } = this.props
        const { dialogOrderData, orders, orders404, ordersLoad, ordersRefresh, pages } = this.state
        console.log(this.props.currentUser)
        return (


            <View style={{ borderRadius: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center', marginTop: moderateScale(10), }}>
                <PageTitleLine title={Strings.orderDetails} iconName='user-alt' iconType='FontAwesome5' />


                {currentUser.data.role == 'provider' ? this.orderProviderDetails() : this.orderProviderDetails()}

            </View>
        )
    }


    render() {
        const { currentUser, isRTL } = this.props;
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={strings.myOrders} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/


            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={strings.myOrders} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
        );
    }
}



const mapDispatchToProps = {
    getUser,
    removeItem,
    getOrdersCount
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color
})


export default connect(mapToStateProps, mapDispatchToProps)(OrderDetails);

