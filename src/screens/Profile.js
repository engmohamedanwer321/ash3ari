import React, { Component } from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {
  View,Keyboard,ScrollView,TouchableOpacity,Text,Modal,ImageBackground, Alert
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail } from "native-base";
import { responsiveWidth, moderateScale,responsiveFontSize,responsiveHeight } from "../utils/responsiveDimensions";
import Strings from  '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import {getUser} from '../actions/AuthActions';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import {selectMenu,removeItem} from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import strings from '../assets/strings';
import {enableSideMenu,push} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from '../common/AppFont'



class Profile extends Component {

    state = {
        openModal:false
    }

    componentWillUnmount(){
        this.props.removeItem()
      }

    componentDidMount(){
        enableSideMenu(true,this.props.isRTL) 
    }  

    componentDidUpdate(){
        enableSideMenu(true,this.props.isRTL)
    }

   
    
    onUpdate(values) {
        var data = new FormData(); 
            data.append('firstname',values.firstName);
            data.append('lastname',values.lastName)
            data.append('phone',values.phoneNumber)
            data.append('email',values.email)
            data.append('address',values.address)
            data.append('language','arabic')
            data.append('cardNum',values.cardNum)
            
        if(this.state.userImage!=null){
            data.append('img',{
                uri: this.state.userImage,
                type: 'multipart/form-data',
                name: 'productImages'
            }) 
        } 
        console.log('new user') 
        //console.log(userData)
       this.setState({updateLoading:true})
        axios.put(`${BASE_END_POINT}user/updateInfo`, data, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
            console.log('update user profile done')      
            console.log(response.data)
            this.setState({updateLoading:false,noConnection:null});           
            const user = {...this.props.currentUser,user:{...response.data.user}}
            console.log('current user')
            console.log(user)
            RNToasty.Success({title:Strings.updateProfileSuccess})
            this.props.getUser(user)
            AsyncStorage.setItem('@QsathaUser', JSON.stringify(user));
            /*this.props.navigator.push({
                screen: 'Home',
                animated: true,
            })*/
        }).catch(error => {
            console.log('error');
            console.log(error.response);
            console.log(error);
            this.setState({updateLoading:false});
             
            if (!error.response) {
            this.setState({noConnection:Strings.noConnection});
            } 
          });
    }

    profileInfoItem = (iconName,iconType,data) => {
        const{isRTL} = this.props
        return(
            <View style={{flexDirection:isRTL?'row-reverse':'row', borderBottomColor:'#DBDBDB',borderBottomWidth:1,width:responsiveWidth(100),height:responsiveHeight(10),alignItems:'center'}}>
              
               <View style={{fontFamily:englishFont, flexDirection:'row', marginHorizontal:moderateScale(10)}}>
                   <Icon type={iconType} name={iconName} style={{fontSize:responsiveFontSize(9),color:'#679C8A'}} />
               </View>

                <Text style={{fontFamily:englishFont, flexDirection:'row', color:'black',fontSize:responsiveFontSize(6)}} >{data}</Text>

            </View>
        )
    }

   

    render(){
        const {currentUser,isRTL} = this.props;
       
        return(
            <View style={{ flex:1 }}>
                <AppHeader menu  title={strings.profile}  /> 
                <ScrollView style={{marginTop:moderateScale(-5), flex:1 }} > 
                             
                <View style={{borderBottomLeftRadius:moderateScale(50),borderBottomRightRadius:moderateScale(50), elevation:5, backgroundColor:'#679C8A', height:responsiveHeight(30), width:responsiveWidth(100),shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1}}>
                    <TouchableOpacity
                    onPress={()=>{
                        push('UpdateProfile')
                    }}
                     style={{marginTop:moderateScale(11), alignSelf:isRTL?'flex-start':'flex-end',marginHorizontal:moderateScale(11), }} >
                        <Icon name='setting' type='AntDesign'  style={{color:'white',fontSize:responsiveFontSize(10)}} />
                    </TouchableOpacity>
                    <Text style={{fontFamily:isRTL?arrabicFont:englishFont, marginTop:moderateScale(8),alignSelf:'center', fontStyle:'italic', marginHorizontal:moderateScale(8), color:'white',fontSize:responsiveFontSize(12)}} >{currentUser.user.name}</Text>
                </View>
                <View onTouchStart={()=>{
                    this.setState({openModal:true})
                }} style={{marginHorizontal:moderateScale(15),alignSelf:'center', marginTop:moderateScale(-19),elevation:6, zIndex:10000,shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1}}>
                    <Thumbnail  large  source={currentUser.user.avatar?{uri:currentUser.user.avatar}:require('../assets/imgs/profileicon.jpg')} />
                </View>

                <View style={{marginTop:moderateScale(10)}} >
                {this.profileInfoItem("email","MaterialCommunityIcons",currentUser.user.email)}
                {this.profileInfoItem("phone","AntDesign",currentUser.user.phone)}
                {this.profileInfoItem("user","Feather",currentUser.user.gender)}
                {this.profileInfoItem("calendar","AntDesign",currentUser.user.birth_date)}
                </View>

               </ScrollView>

               <Modal
                onRequestClose={()=>this.setState({openModal:false})}
                animationType="slide"
                transparent={false}
                 visible={this.state.openModal}
                 >
                 <ImageBackground
                 source={currentUser.user.avatar?{uri:currentUser.user.avatar}:require('../assets/imgs/profileicon.jpg')}
                 style={{width:responsiveWidth(100),height:responsiveHeight(100)}}
                 >
                     <TouchableOpacity
                     onPress={()=>{
                         this.setState({openModal:false})
                     }}
                     style={{alignSelf:'flex-end', margin:moderateScale(10)}}
                     >
                         <Icon name='close' type='AntDesign' style={{color:'black',fontSize:responsiveFontSize(10)}} />
                     </TouchableOpacity>
                </ImageBackground>    
                 

               </Modal>
            </View>
            
        );
    }
}



const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    barColor: state.lang.color 
})


export default connect(mapToStateProps,mapDispatchToProps)(Profile);

