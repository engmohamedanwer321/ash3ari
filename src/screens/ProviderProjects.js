import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, Animated, ScrollView, TouchableOpacity, Text, Modal, ImageBackground, TextInput, Image, FlatList
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail, Button } from "native-base";
import { responsiveWidth, moderateScale, responsiveFontSize, responsiveHeight } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import { getUser } from '../actions/AuthActions';
import { BASE_END_POINT } from '../AppConfig'
import axios from 'axios';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import strings from '../assets/strings';
import { enableSideMenu, push } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import { Rating, AirbnbRating } from 'react-native-ratings';
import MapView, { Marker } from 'react-native-maps';
import FastImage from 'react-native-fast-image'
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import PageTitleLine from '../components/PageTitleLine'

class ServiceProviderProfile extends Component {

    state = {
        works: [],
        showModal: '',
        work: {},
    }

    componentWillUnmount() {
        this.props.removeItem()
    }

    componentDidMount() {
        enableSideMenu(false, this.props.isRTL)
        this.getWorks()
    }

    componentDidUpdate() {
        enableSideMenu(false, this.props.isRTL)
    }

    getWorks = () => {
        axios.get(`${BASE_END_POINT}projects/provider-projects/${this.props.currentUser.data.id}`)
            .then(response => {
                console.log('DONE  ', response.data)
                this.setState({ works: response.data.data })
            })
            .catch(error => {
                console.log('ERROR  ', error.response)
            })
    }

    deleteProject = (id) => {
        axios.delete(`${BASE_END_POINT}provider/projects/delete/${id}`, {
            headers: {
                //'Content-Type': 'application/json',
                "Authorization": "Bearer " + this.props.currentUser.data.token
                //'Content-Type': 'multipart/form-data',
            },
        })
            .then(response => {
                console.log('DELETED  ', response.data)
                // this.setState({ works: response.data.data })
            })
            .catch(error => {
                console.log('DELETED ERROR  ', error.response)
            })
    }


    ourWorks() {
        const { isRTL } = this.props
        const { works } = this.state
        return (
            <View style={{ width: responsiveWidth(97), alignSelf: 'center', borderRadius: moderateScale(3), marginTop: moderateScale(6), minHeight: responsiveHeight(30) }}>
                <PageTitleLine title={strings.myProjects} iconName='google-pages' iconType='MaterialCommunityIcons' />

                <View style={{ width: responsiveWidth(97), flexDirection: isRTL ? 'row-reverse' : 'row', flexWrap: 'wrap', alignItems: isRTL ? 'flex-end' : 'flex-start', alignSelf: 'center', marginVertical: moderateScale(5) }}>
                    {works.length > 0 ?

                        <FlatList

                            contentContainerStyle={{ alignItems: isRTL ? 'flex-end' : 'flex-start', justifyContent: 'space-between' }}
                            numColumns={2}
                            style={{ marginTop: moderateScale(5), alignSelf: 'center' }}
                            data={works}
                            renderItem={({ item, index }) =>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({ showModal: true, work: val })
                                    }}
                                    style={{ marginVertical: moderateScale(3), elevation: 4, shadowOffset: { width: 0, height: 2 }, shadowColor: 'black', shadowOpacity: 0.2, width: responsiveWidth(45), borderBottomLeftRadius: moderateScale(5), orderBottomRightRadius: moderateScale(5), alignSelf: 'center' }}>
                                    <View style={{ width: responsiveWidth(38), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', backgroundColor: '#BBB9B9' }}>
                                        <Icon name='photo' type='FontAwesome' style={{ fontSize: responsiveFontSize(8), color: 'white' }} />
                                    </View>
                                    <FastImage
                                        resizeMode='stretch'
                                        source={item.file != null ? { uri: item.file } : require('../assets/imgs/defaultLogo.png')}
                                        style={{ width: responsiveWidth(38), height: responsiveHeight(20) }}
                                    />
                                    <View style={{ borderBottomLeftRadius: moderateScale(5), orderBottomRightRadius: moderateScale(5), width: responsiveWidth(38), minHeight: responsiveHeight(9), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue }}>
                                        <Text style={{ color: 'white', textAlign: 'center', width: responsiveWidth(30) }} >{item.title}</Text>
                                    </View>

                                    <TouchableOpacity onPress={() => {
                                        this.removeElement(works, index)
                                        this.deleteProject(item.id)
                                    }}
                                        style={{ position: 'absolute', top: moderateScale(-3), backgroundColor: 'white', borderRadius: moderateScale(3) }}>
                                        <Icon name='delete' type='AntDesign' style={{ fontSize: responsiveFontSize(7), color: 'red' }} />
                                    </TouchableOpacity>
                                </TouchableOpacity>
                            }
                        />

                        /*works.map((val, index) => (
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ showModal: true, work: val })
                                }}
                                style={{ marginVertical: moderateScale(3), elevation: 4, shadowOffset: { width: 0, height: 2 }, shadowColor: 'black', shadowOpacity: 0.2, width: responsiveWidth(39), borderBottomLeftRadius: moderateScale(5), orderBottomRightRadius: moderateScale(5),alignSelf:'center' }}>
                                <View style={{ width: responsiveWidth(38), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', backgroundColor: '#BBB9B9' }}>
                                    <Icon name='photo' type='FontAwesome' style={{ fontSize: responsiveFontSize(8), color: 'white' }} />
                                </View>
                                <FastImage
                                    resizeMode='stretch'
                                    source={val.file != null ? { uri: val.file } : require('../assets/imgs/defaultLogo.png')}
                                    style={{ width: responsiveWidth(38), height: responsiveHeight(20) }}
                                />
                                <View style={{ borderBottomLeftRadius: moderateScale(5), orderBottomRightRadius: moderateScale(5), width: responsiveWidth(38), minHeight: responsiveHeight(9), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue }}>
                                    <Text style={{ color: 'white', textAlign: 'center', width: responsiveWidth(30) }} >{val.title}</Text>
                                </View>
                        
                                <TouchableOpacity onPress={() => {
                                this.removeElement(works, index)
                                this.deleteProject(val.id)
                            }}
                                    style={{ position: 'absolute', top: moderateScale(-3), backgroundColor: 'white', borderRadius: moderateScale(3) }}>
                                    <Icon name='delete' type='AntDesign' style={{ fontSize: responsiveFontSize(7), color: 'red' }} />
                                </TouchableOpacity>
                            </TouchableOpacity>
                        ))*/

                        :
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={{ color: colors.darkBlue, marginTop: moderateScale(20), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center', fontSize: responsiveFontSize(6) }}>{Strings.noWorksFound}</Text>
                        </View>
                    }
                </View>
            </View>
        )
    }

    removeElement(array, index) {

        if (index > -1) {
            array.splice(index, 1);
        }
        this.setState({ works: array })
    }






    content = () => {
        // const {tabNumber} = this.state
        const { currentUser, isRTL } = this.props;
        const { work, showModal } = this.state
        return (
            <View>





                {this.ourWorks()}



                <Modal
                    visible={showModal}
                    onRequestClose={() => {
                        this.setState({ showModal: false })
                    }}
                >
                    <View style={{ flex: 1 }} >
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ showModal: false })
                            }}
                            style={{ marginTop: moderateScale(25), marginHorizontal: moderateScale(10), alignSelf: 'flex-end' }}
                        >
                            <Icon name='close' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(10) }} />
                        </TouchableOpacity>
                        <FastImage
                            resizeMode='contain'
                            source={work.file != null ? { uri: work.file } : require('../assets/imgs/defaultLogo.png')}
                            style={{ width: responsiveWidth(100), height: responsiveHeight(80) }}
                        />
                    </View>
                </Modal>

            </View>
        )
    }



    render() {
        const { currentUser, isRTL } = this.props;
        // const { name, email, phone, message } = this.state
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={strings.profile} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/


            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={strings.profile} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
        );
    }

}

const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color
})

export default connect(mapToStateProps, mapDispatchToProps)(ServiceProviderProfile);