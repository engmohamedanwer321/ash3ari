import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, Animated, ScrollView, TouchableOpacity, Text, Modal, ImageBackground, TextInput, StyleSheet, Alert, Linking
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail, Button } from "native-base";
import { responsiveWidth, moderateScale, responsiveFontSize, responsiveHeight } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import { getUser } from '../actions/AuthActions';
import { BASE_END_POINT } from '../AppConfig'
import axios from 'axios';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import strings from '../assets/strings';
import { enableSideMenu, push, pop, resetTo } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import CollaspeAppHeader from '../common/CollaspeAppHeader';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import InputValidations from '../common/InputValidations'
import PageTitleLine from '../components/PageTitleLine'
import RadioButton from 'radio-button-react-native';
import ImagePicker from 'react-native-image-crop-picker';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import FastImage from 'react-native-fast-image';
import CommentCard from '../components/CommentCard'
import { FlatList } from 'react-native-gesture-handler';
import ParallaxScrollView from 'react-native-parallax-scroll-view';


class RequestAndOfferDetails extends Component {

    state = {
        type:'request',
        comment: ' ',
        loading: false,
        comments: []
    }

    componentDidMount() {
        enableSideMenu(false, null)
        this.getComments()
    }

    componentDidUpdate() {
        enableSideMenu(false, null)
    }

    getComments = () => {
        axios.get(`${BASE_END_POINT}market/single/${this.props.data.id}`,{
            headers: {
            "Authorization": "Bearer " + this.props.currentUser.data.token
            },
        })
        .then(response=>{
           
            this.setState({comments:response.data.data.comments})
        })
        .catch(error=>{
           
        })
    }



    send = () => {
        const {comment} = this.state
        var  url=`${BASE_END_POINT}market/add/comment/${this.props.data.id}`
        
        if (!comment.replace(/\s/g, '').length) {
            this.setState({ comment:''})
        }
        if(comment.replace(/\s/g, '').length){
            this.setState({loading:true})
            var data = new FormData()
            data.append('comment',comment)
            
            axios.post(url,data,{
                headers:{
                    "Authorization": "Bearer " + this.props.currentUser.data.token
                }
            })
            .then(response=>{
                this.setState({
                    loading:false,
                    comment:' ',
                    comments:[response.data.data,...this.state.comments]
                })   
                console.log('DONE')
               
            })
            .catch(error => {
                console.log('Error  ', error)
                this.setState({ loading: false })
                
            })
            
        }
    }


    titleView = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                    <View style={{ width: responsiveWidth(6)}}>
                        <Icon type='MaterialCommunityIcons' name='subtitles-outline' style={{ fontSize: responsiveFontSize(8), color:colors.darkBlue }} />
                    </View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(3)  }}>{Strings.title}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(78), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                    value={this.props.data.title}
                    editable={false}
                    underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
            </View>
        )
    }

    descriptionView = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                <View style={{ width: responsiveWidth(6)}}>
                        <Icon type='FontAwesome' name='file-text-o' style={{ fontSize: responsiveFontSize(7), color:colors.darkBlue }} />
                    </View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(3) }}>{Strings.description}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(78), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb',}}>
                    <TextInput
                    multiline
                    value={this.props.data.description}
                    editable={false}
                    underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
            </View>
        )
    }

    nameView = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                <View style={{ width: responsiveWidth(6)}}>
                        <Icon type='MaterialIcons' name='text-format' style={{ fontSize: responsiveFontSize(10), color:colors.darkBlue }} />
                    </View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(3) }}>{Strings.name}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(78), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                    value={this.props.data.user.name}
                    editable={false}
                    underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
            </View>
        )
    }

    emailView = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                <View style={{ width: responsiveWidth(6)}}>
                        <Icon type='MaterialCommunityIcons' name='email-outline' style={{ fontSize: responsiveFontSize(9), color:colors.darkBlue }} />
                    </View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(3)  }}>{Strings.email}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(78), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                    value={this.props.data.user.email}
                    editable={false}
                    underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
            </View>
        )
    }


    phoneView = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                 <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                <View style={{ width: responsiveWidth(6)}}>
                        <Icon type='Feather' name='phone' style={{ fontSize: responsiveFontSize(9), color:colors.darkBlue }} />
                    </View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(3) }}>{Strings.phone}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(78), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                    value={this.props.data.user.phone}
                    editable={false}
                    underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
            </View>
        )
    }

    timeView = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                <View style={{ width: responsiveWidth(6)}}>
                        <Icon type='MaterialIcons' name='access-time' style={{ fontSize: responsiveFontSize(9), color:colors.darkBlue }} />
                    </View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(3) }}>{Strings.expectedTime}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(78), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                    value={this.props.data.time_count}
                    editable={false}
                    underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
            </View>
        )
    }

    dateView = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.date}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                   value={this.props.data.time_count}
                    editable={false}
                    underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
            </View>
        )
    }

    attachView = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginVertical: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                <View style={{ width: responsiveWidth(6)}}>
                        <Icon type='MaterialIcons' name='attach-file' style={{ fontSize: responsiveFontSize(9), color:colors.darkBlue }} />
                    </View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(3) }}>{Strings.attachment}</Text>
                </View>
                <TouchableOpacity
                onPress={()=>{
                    Linking.openURL(this.props.data.attachment)
                }}
                >
                    <Text style={{textDecorationLine:'underline', marginVertical: moderateScale(5), color:'blue'}}>{this.props.data.attachment} </Text>
                </TouchableOpacity>
            </View>
        )
    }




    commentInput = () => {
        const {comment} = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{width: responsiveWidth(90), alignSelf: 'center' }}>
                 <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                <View style={{ width: responsiveWidth(6)}}>
                        <Icon type='MaterialCommunityIcons' name='comment-text-outline' style={{ fontSize: responsiveFontSize(9), color:colors.darkBlue }} />
                    </View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(3) }}>{Strings.addComment}</Text>
                </View>
                <View style={{ borderColor: colors.lightGray, flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 0.5, height: responsiveHeight(20), marginTop: moderateScale(5) }}>

                    <TextInput
                        value={comment.replace(/\s/g, '').length?comment:null}
                        onChangeText={(val) => { this.setState({ comment: val}) }}
                        multiline={true}
                        placeholder={Strings.addComment} underlineColorAndroid='transparent' style={{ textAlignVertical: 'top', fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {comment.length == 0 &&
                <Text style={{marginHorizontal:moderateScale(3), color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
               
            </Animatable.View>
        )
    }


    addCommentButton = () => {
        const { isRTL, barColor } = this.props
        return(
        <View style={{alignSelf:'center'}}>
            <Button onPress={()=>{
                this.send()
            }} style={{marginVertical:moderateScale(12), backgroundColor:colors.darkBlue,borderRadius:moderateScale(3), width:responsiveWidth(30),height:responsiveHeight(7),justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:colors.white}}>{strings.add}</Text>
            </Button>
        </View>  
        )
    }

    coments = () =>{
        return(
            <FlatList
            style={{marginVertical:moderateScale(10)}} 
            data={this.state.comments}
            renderItem={({item})=><CommentCard data={item} />}
            />
        )
    }

   
    content = () => {
        const { isRTL, barColor } = this.props
        return (
            <View>
                <PageTitleLine title={Strings.details} iconName='local-offer' iconType='MaterialIcons' />
                <View style={{justifyContent:'center',alignItems:'center', borderWidth: 1, borderColor: colors.lightGray, borderRadius: moderateScale(5), marginBottom:moderateScale(15), width: responsiveWidth(90), alignSelf: 'center', marginTop: moderateScale(10), }}>
                    {this.titleView()}
                    {this.descriptionView()}
                    {this.nameView()}
                    {this.emailView()}
                    {this.phoneView()}
                    {this.timeView()}
                    {this.attachView()}
                    {/*this.dateView()*/}           
                </View>
                {this.commentInput()}
                {this.addCommentButton()}
                <PageTitleLine title={Strings.comments} iconName='comment' iconType='FontAwesome' />
                {this.coments()}
            </View>
        )
    }

    render() {
        const { currentUser, isRTL } = this.props;
        const { name, email, phone, message } = this.state
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={Strings.sh3ariOffersAndRequestsDetial} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/

            <ParallaxScrollView
            backgroundColor={colors.darkBlue}
            contentBackgroundColor="white"
            renderFixedHeader={() => <CollaspeAppHeader back title={Strings.sh3ariOffersAndRequestsDetial} />}
            parallaxHeaderHeight={responsiveHeight(35)}
            renderBackground={() => (
                <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

            )}
        >
            {this.content()}
        </ParallaxScrollView>
        );
    }
}


const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color
})

export default connect(mapToStateProps, mapDispatchToProps)(RequestAndOfferDetails);