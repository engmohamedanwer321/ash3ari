import React,{Component} from 'react';
import {View,RefreshControl,Alert,FlatList,Text,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight ,responsiveFontSize} from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';

import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../common/ListFooter';
import {Icon,Thumbnail,Button} from 'native-base'
import ChatPeopleCard from '../components/ChatPeopleCard';
import {selectMenu,removeItem} from '../actions/MenuActions';
import AppHeader from '../common/AppHeader';
import {enableSideMenu,pop} from '../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import * as colors from '../assets/colors'
import {arrabicFont,englishFont} from  '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import Swipeout from 'react-native-swipeout';
import RequestsOffersCard from '../components/RequestsOffersCard'
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import PageTitleLine from '../components/PageTitleLine'
import ParallaxScrollView from 'react-native-parallax-scroll-view';

class RequestsAndOffers extends Component {

    page=1;
    state= {
        requests_offers:[],
        requests_offersLoad:true,
        requests_offersRefresh:false,
        requests_offers404:null, 
        pages:0,
    }

    componentDidMount(){
        
        enableSideMenu(true,this.props.isRTL)
        /*NetInfo.addEventListener(state => {
            if(state.isConnected){
                this.page=1
                this.getRooms(1,false);
                this.setState({loading:true,networkError:false})
            }else{
                this.setState({loading:false,networkError:true})
            }
          }); */ 
          this.getOffersAnRequests(false,this.page)
    }

    componentWillUnmount(){
        this.props.removeItem()
      }

    componentDidUpdate(){
        enableSideMenu(true,this.props.isRTL)
    }  

    getOffersAnRequests = (refresh,page) => {
        if(refresh){
            this.setState({requests_offersRefresh:true})
        }
        var uri=''
        if(this.props.data.type=='requests'){
            uri=`${BASE_END_POINT}market/requests?page=${page}`
        }else{
            uri=`${BASE_END_POINT}market/offers?page=${page}`
        }
        axios.get(uri,{
            headers:{
                Authorization: `Bearer ${this.props.currentUser.data.token}` 
            }
        })
        .then(response=>{
    
          //Alert.alert('Done  ',this.props.data)
          this.setState({
              requests_offers:refresh?response.data.data.providers:[...this.state.requests_offers,...response.data.data.providers],
              requests_offersLoad:false,
              requests_offersRefresh:false,
              pages:response.data.data.paginate.total_pages,
            })
          })
        .catch(error=>{
        
          this.setState({requests_offers404:true,requests_offersLoad:false,})
        })
    }

    content = () => {
        const {requests_offers,requests_offers404,requests_offersLoad,pages,requests_offersRefresh} = this.state
         return(
           <View>
            <PageTitleLine title={this.props.data.title} iconName='local-offer' iconType='MaterialIcons' />
            {
            requests_offers404?
            <NetworError />
            :
            requests_offersLoad?
            <Loading />
            :
            requests_offers.length>0?
            <FlatList 
            style={{marginBottom:moderateScale(10)}}
            data={requests_offers}
            renderItem={({item})=><RequestsOffersCard data={item} />}
            onEndReachedThreshold={.5}
            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
            onEndReached={() => {     
                if(this.page <= pages){
                    this.page=this.page+1;
                    this.getOffersAnRequests(false,this.page)
                 
                }  
            }}
            refreshControl={
            <RefreshControl 
            colors={["#B7ED03",colors.darkBlue]} 
            refreshing={requests_offersRefresh}
            onRefresh={() => {
                this.page = 1
                this.getOffersAnRequests(true,1)
            }}
            />
            }
            
            />
            :
            <NoData />
            }
          </View>
         )
       }


    render(){
        const {categoryName,isRTL} = this.props;
        return(
            /*<ReactNativeParallaxHeader
             scrollViewProps={{showsVerticalScrollIndicator:false}}
             headerMinHeight={responsiveHeight(10)}
             headerMaxHeight={responsiveHeight(35)}
             extraScrollHeight={20}
             navbarColor={colors.darkBlue}
             backgroundImage={require('../assets/imgs/header.png')}
             backgroundImageScale={1.2}
             renderNavBar={()=><CollaspeAppHeader back title={this.props.data.title} />}
             renderContent={()=>this.content()}
             containerStyle={{flex:1}}
             contentContainerStyle={{flexGrow: 1}}
             innerContainerStyle={{flex: 1,}}
           />*/

           <ParallaxScrollView
           backgroundColor={colors.darkBlue}
           contentBackgroundColor="white"
           renderFixedHeader={() => <CollaspeAppHeader back title={this.props.data.title} />}
           parallaxHeaderHeight={responsiveHeight(35)}
           renderBackground={() => (
               <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

           )}
       >
           {this.content()}
       </ParallaxScrollView>
           
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color ,
    currentUser:state.auth.currentUser, 
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps,mapDispatchToProps)(RequestsAndOffers);
