import React,{Component} from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {View,Image,Text,TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import {Button,Icon,Radio} from 'native-base';
import Strings from '../assets/strings';
import  {changeLanguage,changeColor} from '../actions/LanguageActions';
import {  moderateScale, responsiveFontSize, responsiveWidth,responsiveHeight } from "../utils/responsiveDimensions";
import {removeItem} from '../actions/MenuActions';
import Checkbox from 'react-native-custom-checkbox';
import AppHeader from  '../common/AppHeader'
import {enableSideMenu,pop, openSideMenu} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as colors from '../assets/colors'
import * as Animatable from 'react-native-animatable';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import FastImage from 'react-native-fast-image'


class SelectLanguage extends Component {



    state={
        showModal: false,
        lang: this.props.isRTL
    }
    
    componentDidMount(){
        enableSideMenu(false,this.props.isRTL)
       
    }
  
    componentWillUnmount(){
        this.props.removeItem()
      }

      componentDidUpdate(){
        enableSideMenu(false,this.props.isRTL)
      
      }

      settingsItem = (item) =>{
        const {isRTL,barColor} = this.props
        return(
            <View style={{width:responsiveWidth(90),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',marginTop:moderateScale(10)}}>
                <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'white',fontSize:responsiveFontSize(3)}} >{item}</Text>
                <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'white',fontSize:responsiveFontSize(3)}} >{item}</Text>
            </View>
        )
      }

      content = () => {
        const {isRTL,barColor} = this.props
        const {showModal,lang} = this.state;
         return(
           <View>
              <TouchableOpacity
                activeOpacity={1}
                 onPress={()=>{this.setState({showModal:!showModal})}}
                 style={{backgroundColor:'white', marginTop:moderateScale(10), height:responsiveHeight(9),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between', width:responsiveWidth(90),alignSelf:'center',borderRadius:moderateScale(2),borderColor:'white',elevation:1,shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1}}>
                    <Text style={{fontFamily:isRTL?arrabicFont:englishFont,color:'#C4C4C4',fontSize:responsiveFontSize(6.5),marginHorizontal:moderateScale(4)}} >{lang?'اللغة العربية':'English'}</Text>
                    <View style={{marginHorizontal:moderateScale(4)}}>
                        <Icon name={showModal?'chevron-small-up':'chevron-small-down'} type='Entypo'  style={{color:'#C4C4C4',fontSize:responsiveFontSize(6)}} />
                    </View>
                </TouchableOpacity>
               
                {showModal&&
                <Animatable.View animation='flipInX' duration={1500}  style={{marginTop:moderateScale(1), shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,height:responsiveHeight(22),backgroundColor:'white', alignItems:'center', width:responsiveWidth(90),alignSelf:'center',borderRadius:moderateScale(2),elevation:1,}}>
                
                <TouchableOpacity
                 onPress={()=>{ this.setState({lang:false,showModal:false}) }}
                 style={{borderBottomWidth:0.3,borderBottomColor:'#DBDBDB',backgroundColor:'white', marginTop:moderateScale(4), height:responsiveHeight(9),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between', width:responsiveWidth(90)}}>
                    <Text style={{fontFamily:englishFont, color:lang?'#C4C4C4':'black',fontSize:responsiveFontSize(6.5),marginHorizontal:moderateScale(4)}} >English</Text>
                    {!lang&&
                    <View style={{marginHorizontal:moderateScale(4)}}>
                    <Checkbox            
                        checked={lang?false:true}
                        style={{marginHorizontal:moderateScale(4), borderColor:colors.darkBlue, backgroundColor: colors.darkBlue,color: 'white',borderRadius: 0}}
                        /*onChange={(name, checked) => {
                        this.setState({ agreeTerms: checked });
                        }}*/
                    />
                    </View>
                    }
                </TouchableOpacity>

                <TouchableOpacity 
                onPress={()=>{ this.setState({lang:true,showModal:false}) }}
                style={{backgroundColor:'white', marginVertical:moderateScale(4), height:responsiveHeight(9),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between', width:responsiveWidth(90)}}>
                    <Text style={{fontFamily:arrabicFont, color:lang?'black':'#C4C4C4',fontSize:responsiveFontSize(6.5),marginHorizontal:moderateScale(4)}} >اللغة العربية</Text>
                    {lang&&
                     <View style={{marginHorizontal:moderateScale(4)}}>
                     <Checkbox               
                         checked={lang?true:false}
                         style={{marginHorizontal:moderateScale(4), borderColor:colors.darkBlue, backgroundColor:colors.darkBlue,color: 'white',borderRadius: 0}}
                         /*onChange={(name, checked) => {
                         this.setState({ agreeTerms: checked });
                         }}*/
                     />
                     </View>
                    }
                </TouchableOpacity>
                
                </Animatable.View>
                }
            
                <TouchableOpacity
                    style={{width:responsiveWidth(50),alignSelf:'center',height:responsiveHeight(7),alignItems:'center',justifyContent:'center',backgroundColor:colors.darkBlue,marginTop:moderateScale(15),borderRadius:moderateScale(2)}}
                    onPress={()=>{
                    if(lang){
                        this.props.changeLanguage(true);
                        Strings.setLanguage('ar');
                        AsyncStorage.setItem('@lang','ar')
                        console.log('lang   '+ this.props.isRTL)
                        pop()
                    }else{
                        this.props.changeLanguage(false);
                        Strings.setLanguage('en');
                        AsyncStorage.setItem('@lang','en')
                        console.log('lang   '+ this.props.isRTL)
                        pop()
                    }
                    }}
                    >
                    <Text style={{fontFamily:isRTL?arrabicFont:englishFont,color:'white',fontSize:responsiveFontSize(8)}}>{Strings.done}</Text>
                </TouchableOpacity>
           
          </View>
         )
       }
 
     
    render(){
        const {isRTL,barColor} = this.props
        const {showModal,lang} = this.state;
        return(
            /*<ReactNativeParallaxHeader
            scrollViewProps={{showsVerticalScrollIndicator:false}}
            headerMinHeight={responsiveHeight(10)}
            headerMaxHeight={responsiveHeight(35)}
            extraScrollHeight={20}
            navbarColor={colors.darkBlue}
            backgroundImage={require('../assets/imgs/header.png')}
            backgroundImageScale={1.2}
            renderNavBar={()=><CollaspeAppHeader back title={Strings.language} />}
            renderContent={()=>this.content()}
            containerStyle={{flex:1}}
            contentContainerStyle={{flexGrow: 1}}
            innerContainerStyle={{flex: 1,}}
          />*/ 
          
          
          <ParallaxScrollView
           backgroundColor={colors.darkBlue}
           contentBackgroundColor="white"
           renderFixedHeader={() => <CollaspeAppHeader back title={Strings.language} />}
           parallaxHeaderHeight={responsiveHeight(35)}
           renderBackground={() => (
               <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

           )}
       >
           {this.content()}
       </ParallaxScrollView>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color 
   
})

const mapDispatchToProps = {
    changeLanguage,
    changeColor,
    removeItem
}



export default connect(mapToStateProps,mapDispatchToProps)(SelectLanguage);