import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, Animated, ScrollView, TouchableOpacity, Text, Modal, ImageBackground, TextInput, Image
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail, Button } from "native-base";
import { responsiveWidth, moderateScale, responsiveFontSize, responsiveHeight } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import { getUser } from '../actions/AuthActions';
import { BASE_END_POINT } from '../AppConfig'
import axios from 'axios';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import strings from '../assets/strings';
import { enableSideMenu, push } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import { Rating, AirbnbRating } from 'react-native-ratings';
import MapView, { Marker } from 'react-native-maps';
import FastImage from 'react-native-fast-image'
import ParallaxScrollView from 'react-native-parallax-scroll-view';

class ServiceProviderProfile extends Component {

    state = {
        works: [],
        showModal: '',
        work: {},
        showZoomImgModal: false
    }

    componentWillUnmount() {
        this.props.removeItem()
    }

    componentDidMount() {
        enableSideMenu(false, this.props.isRTL)
        this.getWorks()
    }

    componentDidUpdate() {
        enableSideMenu(false, this.props.isRTL)
    }

    getWorks = () => {
        axios.get(`${BASE_END_POINT}projects/provider-projects/${this.props.currentUser.data.id}`)
            .then(response => {
                console.log('DONE  ', response.data)
                this.setState({ works: response.data.data })
            })
            .catch(error => {
                console.log('ERROR  ', error.response)
            })
    }

    rateSection() {
        const { isRTL, currentUser } = this.props
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center', width: responsiveWidth(80), justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(8) }}>
                <View style={{ paddingHorizontal: moderateScale(14), width: responsiveWidth(50), borderRightColor: colors.greenApp, borderLeftColor: colors.greenApp, borderRightWidth: isRTL ? 0 : 3, borderLeftWidth: isRTL ? 3 : 0 }}>
                    <Text style={{ fontSize: responsiveFontSize(6), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont }}>{currentUser.data.orderCount ? currentUser.data.orderCount : 0}</Text>
                    <Text style={{ fontSize: responsiveFontSize(6), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont, marginTop: moderateScale(5) }}>{Strings.contractsConcluded}</Text>
                </View>

                <View style={{ paddingHorizontal: moderateScale(14), width: responsiveWidth(50), justifyContent: 'center', alignItems: 'center', }}>
                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                        <Text style={{ fontSize: responsiveFontSize(6), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(5), marginHorizontal: moderateScale(1) }}>{currentUser.data.rate}</Text>
                        <Icon name='star' type='AntDesign' style={{ color: '#ffcc00', fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(1) }} />
                    </View>

                    <Text style={{ fontSize: responsiveFontSize(5), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, marginTop: moderateScale(5) }}>{Strings.rate}</Text>
                </View>
            </View>
        )
    }

    basicDataContainer() {
        const { isRTL, currentUser } = this.props
        return (
            <View style={{ flexDirection: 'column', width: responsiveWidth(90), alignSelf: 'center', marginTop: moderateScale(12), backgroundColor: '#dddddd', paddingVertical: moderateScale(5), borderRadius: moderateScale(3) }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', }}>
                    <View style={{ width: responsiveWidth(8), justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='circle' type='FontAwesome' style={{ color: currentUser.data.isOnline ? '#81e021' : 'red', fontSize: responsiveFontSize(8), marginHorizontal: moderateScale(1) }} />
                    </View>
                    <Text style={{ fontSize: responsiveFontSize(5), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(4), }}>{currentUser.data.isOnline == true ? Strings.connected : Strings.disConnected}</Text>
                </View>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(4) }}>
                    <View style={{ width: responsiveWidth(8), justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='institution' type='FontAwesome' style={{ color: 'black', fontSize: responsiveFontSize(6), marginHorizontal: moderateScale(1) }} />
                    </View>
                    <Text style={{ fontSize: responsiveFontSize(5), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(4), }}>{currentUser.data.country_name}</Text>
                </View>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(4) }}>
                    <View style={{ width: responsiveWidth(8), justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='clockcircleo' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), marginHorizontal: moderateScale(1) }} />
                    </View>
                    <Text style={{ fontSize: responsiveFontSize(5), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(4), }}>{currentUser.data.created_year}</Text>
                </View>

                {currentUser.data.commerical_no != null &&
                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(2) }}>
                        <View style={{ width: responsiveWidth(8), justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='newspaper-o' type='FontAwesome' style={{ color: 'black', fontSize: responsiveFontSize(6), marginHorizontal: moderateScale(1) }} />
                        </View>
                        <Text style={{ fontSize: responsiveFontSize(5), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(4), }}>{strings.commercialRegister} : {currentUser.data.commerical_no}</Text>
                    </View>
                }

                {/*<View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(4) }}>
                    <View style={{ width: responsiveWidth(8), justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='map-marker' type='FontAwesome' style={{ color: 'black', fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(1) }} />
                    </View>
                    <TouchableOpacity>
                        <Text style={{ fontSize: responsiveFontSize(5), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(4), }}>{Strings.goToLocation}</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(4) }}>
                    <View style={{ width: responsiveWidth(8), justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='mail' type='Entypo' style={{ color: 'black', fontSize: responsiveFontSize(6), marginHorizontal: moderateScale(1) }} />
                    </View>
                    <TouchableOpacity >
                        <Text style={{ fontSize: responsiveFontSize(5), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(4), }}>{Strings.conversation}</Text>
                    </TouchableOpacity>
                </View>*/}

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(4) }}>
                    <View style={{ width: responsiveWidth(8), justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='like1' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(6), marginHorizontal: moderateScale(1) }} />
                    </View>
                    <Text style={{ fontSize: responsiveFontSize(5), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(4), }}>{currentUser.data.provider_type == 'one' ? Strings.individual : Strings.company}</Text>
                </View>
            </View>
        )
    }

    whoUs() {
        const { isRTL, currentUser } = this.props
        return (
            <View style={{ width: responsiveWidth(90), alignSelf: 'center', borderColor: 'gray', borderWidth: 0.5, borderRadius: moderateScale(3), marginTop: moderateScale(6), minHeight: responsiveHeight(20) }}>
                <View style={{ borderBottomColor: 'gray', borderBottomWidth: 1.5, alignSelf: isRTL ? 'flex-end' : 'flex-start', width: responsiveWidth(85), marginTop: moderateScale(4), justifyContent: 'center', alignItems: 'center', height: responsiveHeight(6), }}>
                    <Text style={{ borderBottomColor: '#80ad30', borderBottomWidth: 1.5, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(5), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(4), height: responsiveHeight(6) }}>{Strings.whoUs}</Text>
                </View>
                <View style={{ width: responsiveWidth(85), alignSelf: 'center', alignItems: 'center', justifyContent: 'center', marginVertical: moderateScale(3) }}>
                    <Text style={{ textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont }}>{currentUser.data.bio}</Text>
                </View>

            </View>
        )
    }

    ourFields() {
        const { isRTL, currentUser } = this.props
        return (
            <View style={{ width: responsiveWidth(90), minHeight: responsiveHeight(20), alignSelf: 'center', borderColor: 'gray', borderWidth: 0.5, borderRadius: moderateScale(3), marginTop: moderateScale(6) }}>
                <View style={{ borderBottomColor: 'gray', borderBottomWidth: 1.5, alignSelf: isRTL ? 'flex-end' : 'flex-start', width: responsiveWidth(85), marginTop: moderateScale(4), justifyContent: 'center', alignItems: 'center', height: responsiveHeight(6), }}>
                    <Text style={{ borderBottomColor: '#80ad30', borderBottomWidth: 1.5, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(5), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(4), height: responsiveHeight(6) }}>{Strings.ourFields}</Text>
                </View>

                <View style={{ width: responsiveWidth(86), flexDirection: isRTL ? 'row-reverse' : 'row', flexWrap: 'wrap', alignItems: 'center', alignSelf: 'center', marginVertical: moderateScale(5) }}>
                    {currentUser.data.sub_categories.map(val => (

                        <Button style={{ width: responsiveWidth(26), margin: moderateScale(2), justifyContent: 'center', alignItems: 'center', borderRadius: moderateScale(2), backgroundColor: colors.darkBlue }}>
                            <Text style={{ paddingHorizontal: moderateScale(5), paddingVertical: moderateScale(0), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center', fontSize: responsiveFontSize(5) }}>{isRTL ? val.ar_name : val.en_name}</Text>
                        </Button>


                    ))}
                </View>
                {/*this.ourFieldsButtons()*/}

            </View>
        )
    }

    contactData() {
        const { isRTL, currentUser } = this.props
        return (
            <View style={{ width: responsiveWidth(90), alignSelf: 'center', borderColor: 'gray', borderWidth: 0.5, borderRadius: moderateScale(3), marginTop: moderateScale(6), height: responsiveHeight(50) }}>
                <View style={{ borderBottomColor: 'gray', borderBottomWidth: 1.5, alignSelf: isRTL ? 'flex-end' : 'flex-start', width: responsiveWidth(85), marginTop: moderateScale(4), justifyContent: 'center', alignItems: 'center', height: responsiveHeight(6), }}>
                    <Text style={{ borderBottomColor: '#80ad30', borderBottomWidth: 1.5, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(5), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(4), height: responsiveHeight(6) }}>{Strings.contactData}</Text>
                </View>

                <View style={{ borderBottomColor: 'gray', borderBottomWidth: 0.5, width: responsiveWidth(80), alignSelf: 'center', marginTop: moderateScale(5) }}>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), textAlign: isRTL ? 'right' : 'left', fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(80) }}>{Strings.email}</Text>
                    <Text style={{ marginTop: moderateScale(5), color: 'gray', fontSize: responsiveFontSize(6), textAlign: isRTL ? 'right' : 'left', fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(80) }}>{currentUser.data.email}</Text>
                </View>

                <View style={{ borderBottomColor: 'gray', borderBottomWidth: 0.5, width: responsiveWidth(80), alignSelf: 'center', marginTop: moderateScale(5) }}>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), textAlign: isRTL ? 'right' : 'left', fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(80) }}>{Strings.phone}</Text>
                    <Text style={{ marginTop: moderateScale(5), color: 'gray', fontSize: responsiveFontSize(6), textAlign: isRTL ? 'right' : 'left', fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(80) }}>{currentUser.data.phone}</Text>
                </View>

                <View style={{ borderBottomColor: 'gray', borderBottomWidth: 0.5, width: responsiveWidth(80), alignSelf: 'center', marginTop: moderateScale(5) }}>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), textAlign: isRTL ? 'right' : 'left', fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(80) }}>{Strings.address}</Text>
                    <Text style={{ marginTop: moderateScale(5), color: 'gray', fontSize: responsiveFontSize(6), textAlign: isRTL ? 'right' : 'left', fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(80) }}>{currentUser.data.country_name} - {currentUser.data.city_name}</Text>
                </View>

            </View>
        )
    }

    ourWorks() {
        const { isRTL } = this.props
        const { works } = this.state
        return (
            <View style={{ width: responsiveWidth(90), alignSelf: 'center', borderColor: 'gray', borderWidth: 0.5, borderRadius: moderateScale(3), marginTop: moderateScale(6), minHeight: responsiveHeight(30) }}>
                <View style={{ borderBottomColor: 'gray', borderBottomWidth: 1.5, alignSelf: isRTL ? 'flex-end' : 'flex-start', width: responsiveWidth(85), marginTop: moderateScale(4), justifyContent: 'center', alignItems: 'center', height: responsiveHeight(6), }}>
                    <Text style={{ borderBottomColor: '#80ad30', borderBottomWidth: 1.5, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(5), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(4), height: responsiveHeight(6) }}>{Strings.ourWorks}</Text>
                </View>

                <View style={{ width: responsiveWidth(88), flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-around', marginVertical: moderateScale(5) }}>
                    {works.length > 0 ?
                        works.map(val => (
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ showModal: true, work: val })
                                }}
                                style={{ marginVertical: moderateScale(3), elevation: 4, shadowOffset: { width: 0, height: 2 }, shadowColor: 'black', shadowOpacity: 0.2, width: responsiveWidth(38), borderBottomLeftRadius: moderateScale(5), orderBottomRightRadius: moderateScale(5), }}>
                                <View style={{ width: responsiveWidth(38), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', backgroundColor: '#BBB9B9' }}>
                                    <Icon name='photo' type='FontAwesome' style={{ fontSize: responsiveFontSize(8), color: 'white' }} />
                                </View>
                                <FastImage
                                    resizeMode='stretch'
                                    source={val.file != null ? { uri: val.file } : require('../assets/imgs/defaultLogo.png')}
                                    style={{ width: responsiveWidth(38), height: responsiveHeight(20) }}
                                />
                                <View style={{ borderBottomLeftRadius: moderateScale(5), orderBottomRightRadius: moderateScale(5), width: responsiveWidth(38), minHeight: responsiveHeight(9), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue }}>
                                    <Text style={{ color: 'white', textAlign: 'center', width: responsiveWidth(30) }} >{val.title}</Text>
                                </View>
                            </TouchableOpacity>
                        ))
                        :
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={{ color: colors.darkBlue, marginTop: moderateScale(20), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center', fontSize: responsiveFontSize(6) }}>{Strings.noWorksFound}</Text>
                        </View>
                    }
                </View>
            </View>
        )
    }

    sh3ariMembersDesign() {
        const { isRTL, currentUser } = this.props
        return (
            <View style={{ width: responsiveWidth(90), alignSelf: 'center', borderColor: 'gray', borderWidth: 0.5, borderRadius: moderateScale(3), marginTop: moderateScale(8), height: responsiveHeight(20) }}>
                <View style={{ borderBottomColor: 'gray', borderBottomWidth: 1.5, alignSelf: isRTL ? 'flex-end' : 'flex-start', width: responsiveWidth(85), marginTop: moderateScale(6), justifyContent: 'center', alignItems: 'center', height: responsiveHeight(6), }}>
                    <Text style={{ borderBottomColor: '#80ad30', borderBottomWidth: 1.5, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(5), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(4), height: responsiveHeight(6) }}>{Strings.sh3ariMembersDesign}</Text>
                </View>

                <View style={{ marginTop: moderateScale(10), alignSelf: 'center' }}>
                    <AirbnbRating
                        isDisabled
                        showRating={false}
                        starContainerStyle={{ margin: 0 }}
                        style={{ paddingVertical: 0 }}
                        count={5}
                        defaultRating={currentUser.data.rate}
                        size={20}
                    />
                </View>

            </View>
        )
    }

    makeOrder() {
        const { isRTL } = this.props
        return (
            <View style={{ width: responsiveWidth(90), alignSelf: 'center', borderColor: 'gray', borderWidth: 0.5, borderRadius: moderateScale(3), marginVertical: moderateScale(6), }}>
                <View style={{ borderBottomColor: 'gray', borderBottomWidth: 1.5, alignSelf: isRTL ? 'flex-end' : 'flex-start', width: responsiveWidth(85), marginTop: moderateScale(4), justifyContent: 'center', alignItems: 'center', height: responsiveHeight(6), }}>
                    <Text style={{ borderBottomColor: '#80ad30', borderBottomWidth: 1.5, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(5), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(4), height: responsiveHeight(6) }}>{Strings.makeOrder}</Text>
                </View>

                {this.makeOrderButton()}

            </View>
        )
    }

    map() {
        const { isRTL, currentUser } = this.props
        return (
            <View style={{ width: responsiveWidth(90), alignSelf: 'center', borderColor: 'gray', borderWidth: 0.5, borderRadius: moderateScale(3), marginVertical: moderateScale(6), }}>
                <View style={{ borderBottomColor: 'gray', borderBottomWidth: 1.5, alignSelf: isRTL ? 'flex-end' : 'flex-start', width: responsiveWidth(85), marginTop: moderateScale(4), justifyContent: 'center', alignItems: 'center', height: responsiveHeight(6), }}>
                    <Text style={{ borderBottomColor: '#80ad30', borderBottomWidth: 1.5, fontSize: responsiveFontSize(6), lineHeight: responsiveHeight(5), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, paddingHorizontal: moderateScale(4), height: responsiveHeight(6) }}>{Strings.locationOnMap}</Text>
                </View>

                <MapView
                    style={{ alignSelf: 'center', width: responsiveWidth(88), height: responsiveHeight(60), marginVertical: moderateScale(5) }}
                    region={{
                        latitude: Number(currentUser.data.lat),
                        longitude: Number(currentUser.data.lng),
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    }}
                >
                    <Marker
                        coordinate={{
                            latitude: Number(currentUser.data.lat),
                            longitude: Number(currentUser.data.lng),
                            latitudeDelta: 0.015,
                            longitudeDelta: 0.0121,
                        }}
                        image={require('../assets/imgs/marker.png')}
                    />
                </MapView>

            </View>
        )
    }

    ourFieldsButtons = () => {
        //const { name, email, phone, message } = this.state
        const { currentUser, isRTL } = this.props;
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: isRTL ? 'flex-end' : 'flex-start', width: responsiveWidth(90), marginVertical: moderateScale(10) }}>
                <Button style={{ marginHorizontal: moderateScale(4), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(2), backgroundColor: colors.darkBlue }}>
                    <Text style={{ paddingHorizontal: moderateScale(3), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center', fontSize: responsiveFontSize(5) }}>{Strings.intermediatEducation}</Text>
                </Button>

                <Button style={{ marginHorizontal: moderateScale(4), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(2), backgroundColor: colors.darkBlue }}>
                    <Text style={{ paddingHorizontal: moderateScale(3), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center', fontSize: responsiveFontSize(6) }}>{Strings.primaryEducation}</Text>
                </Button>
            </View>
        )
    }

    makeOrderButton = () => {
        //const { name, email, phone, message } = this.state
        const { currentUser, isRTL, data } = this.props;
        return (
            <View style={{ alignSelf: 'center', width: responsiveWidth(90), marginVertical: moderateScale(5) }}>
                <Button onPress={() => push('MakeOrder', currentUser)} style={{ marginHorizontal: moderateScale(4), width: responsiveWidth(40), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(2), backgroundColor: colors.greenApp }}>
                    <Text style={{ paddingHorizontal: moderateScale(3), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center', fontSize: moderateScale(6) }}>{Strings.makeOrder}</Text>
                </Button>
            </View>
        )
    }

    imageZoom = (img) => {
        const { showZoomImgModal, zoomImg } = this.state
        return (
            <Modal
                visible={showZoomImgModal}
                onRequestClose={() => {
                    this.setState({ showZoomImgModal: false })
                }}
            >
                <View style={{ flex: 1 }} >
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({ showZoomImgModal: false })
                        }}
                        style={{ marginTop: moderateScale(25), marginHorizontal: moderateScale(10), alignSelf: 'flex-end' }}
                    >
                        <Icon name='close' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(10) }} />
                    </TouchableOpacity>
                    <FastImage
                        resizeMode='contain'
                        source={{ uri: img }}
                        style={{ width: responsiveWidth(100), height: responsiveHeight(80) }}
                    />
                </View>
            </Modal>

        )
    }

    content = () => {
        // const {tabNumber} = this.state
        const { currentUser, isRTL } = this.props;
        const { work, showModal } = this.state
        return (
            <View>

                <View style={{ alignSelf: 'center', marginTop: moderateScale(5), justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ width: responsiveWidth(38), height: responsiveWidth(38), borderRadius: responsiveWidth(19), borderWidth: moderateScale(1), borderColor: 'green', justifyContent: 'center', alignItems: 'center' }}>

                        <TouchableOpacity onPress={() => {
                            this.setState({ zoomImg: currentUser.data.image, showZoomImgModal: true })

                        }}
                        >
                            <FastImage source={{ uri: currentUser.data.image }} style={{ width: responsiveWidth(35), height: responsiveWidth(35), borderRadius: responsiveWidth(17.5), borderWidth: 3, }} />
                            {this.imageZoom(currentUser.data.image)}
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontSize: responsiveFontSize(6), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, marginTop: moderateScale(5) }}>{currentUser.data.name}</Text>
                </View>

                {this.rateSection()}
                {this.basicDataContainer()}
                {this.whoUs()}
                {this.ourFields()}
                {this.ourWorks()}
                {this.contactData()}
                {this.sh3ariMembersDesign()}
                {/*this.makeOrder()*/}
                {this.map()}

                <Modal
                    visible={showModal}
                    onRequestClose={() => {
                        this.setState({ showModal: false })
                    }}
                >
                    <View style={{ flex: 1 }} >
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ showModal: false })
                            }}
                            style={{ marginTop: moderateScale(25), marginHorizontal: moderateScale(10), alignSelf: 'flex-end' }}
                        >
                            <Icon name='close' type='AntDesign' style={{ color: 'black', fontSize: responsiveFontSize(10) }} />
                        </TouchableOpacity>
                        <FastImage
                            resizeMode='contain'
                            source={work.file != null ? { uri: work.file } : require('../assets/imgs/defaultLogo.png')}
                            style={{ width: responsiveWidth(100), height: responsiveHeight(80) }}
                        />
                    </View>
                </Modal>

            </View>
        )
    }



    render() {
        const { currentUser, isRTL } = this.props;
        // const { name, email, phone, message } = this.state
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={strings.profile} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/


            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={strings.profile} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
        );
    }

}

const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color
})

export default connect(mapToStateProps, mapDispatchToProps)(ServiceProviderProfile);