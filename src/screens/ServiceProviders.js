import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
  View, FlatList, Image, ActivityIndicator, ScrollView, TouchableOpacity, Text, Modal, ImageBackground, TextInput, StyleSheet, RefreshControl
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail, Button } from "native-base";
import { responsiveWidth, moderateScale, responsiveFontSize, responsiveHeight } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import { getUser } from '../actions/AuthActions';
import { BASE_END_POINT } from '../AppConfig'
import axios from 'axios';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import strings from '../assets/strings';
import { enableSideMenu, push } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import ServiceProviderCard from '../components/ServiceProviderCard'
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header'
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import NetInfo from "@react-native-community/netinfo";
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import FastImage from 'react-native-fast-image'


class ServiceProviders extends Component {

  page = 1
  state = {
    providers: [],
    providersLoad: true,
    providers404: null,
    providersRefresh: false,
    pages: 1,
    providerType:this.props.data.providerType,
    countries: [],
    selectCountry: null,
    cities: [],
    selectCity: null,
    departments: [],
    selectDepartment: null,
    selectCategory: [],
    searchCategories: [],
    subCategoryLoading: false

  }

  componentDidMount() {
    enableSideMenu(false, this.props.isRTL)
    this.getCategories()
    //this.getSubCategories()
    this.getCountries()

  

    if (this.props.data.providerType == 'bySubCategory') {
      this.getProvidersBySubCategory(false, 1)
    } else if (this.props.data.providerType == 'bySearch') {
      this.getProvidersBySearch(false, 1)
    
    }
    else {
      this.getProvidrs(false, 1)
    }

  }

  getCategories = () => {
    const { isRTL } = this.props
    axios.get(BASE_END_POINT + 'categories')
      .then(response => {
     
        const res = response.data.data

        var searchCategories = []
        var searchSubCategories = []

        response.data.data.map((itemCat) => {
          itemCat.sub_categories.map((itemSub) => {
            searchSubCategories = [...searchSubCategories, { name: isRTL ? itemSub.ar_name : itemSub.en_name, id: itemSub.id }]
          })
          searchCategories = [...searchCategories, { name: isRTL ? itemCat.name_ar : itemCat.item_en, id: itemCat.id, children: searchSubCategories }]
          searchSubCategories = []
        })

     

        this.setState({
          categories: res,
          categoriesLoad: false,
          /*searchCategories: [
            {
              name: strings.selectCategory,
              id: 0,
              children: searchCategories
            }
          ],*/
          departments: searchCategories

        })
      })
      .catch(error => {
      
        this.setState({ categories404: true, categoriesLoad: false, })
      })
  }


  getSubCategoriesById = (selectCategory) => {
    const { isRTL } = this.props
    //const {selectCategory} = this.state
    /*this.setState({
      departments: []
      //selectDepartment: response.data.data[0]
    })*/
    this.setState({ subCategoryLoading: true })
    axios.get(`${BASE_END_POINT}sub_categories/${selectCategory.id}`)
      .then(response => {

        var subCategories = []

        /*response.data.data.map((item) => {
          subCategories.push({id: item.id, name :isRTL? item.name_ar : item.name_en})
          
        })*/
        response.data.data.map((item) => {
          subCategories = [...subCategories, { name: isRTL ? item.name_ar : item.name_en, id: item.id }]
        })

       

        this.setState({
          eventDesigns: response.data.data,
          eventDesignsLoad: false,
          departments: [
            {
              name: strings.categories,
              id: 1,
              children: subCategories
            }
          ],
          subCategoryLoading: false

          //selectDepartment: response.data.data[0]
        })
      })
      .catch(error => {
      
        this.setState({ eventDesigns404: true, eventDesignsLoad: false, })
      })
  }


  getCountries = () => {
    axios.get(`${BASE_END_POINT}locations/countries`)
      .then(response => {
      
        const res = response.data.data
        this.setState({
          countries: [
            {
              name: strings.countries,
              id: 0,
              children: res
            }
          ]
        })
        //this.setState({ selectCountry: res[0] })
        //this.getCities(res[0].id)
      })
      .catch(error => {
    
      })
  }

  getCities = (countryID) => {
    axios.get(`${BASE_END_POINT}locations/cities/${countryID}`)
      .then(response => {
    
        this.setState({
          cities: [
            {
              name: strings.cities,
              id: 1,
              children: response.data.data
            }
          ],
          //selectCity: response.data.data[0]
        })

      })
      .catch(error => {
      
      })
  }

  getSubCategories = () => {
    const { isRTL } = this.props
    axios.get(`${BASE_END_POINT}sub-categories`)
      .then(response => {
      

        var subCategories = []
        response.data.data.map((item) => {
          subCategories = [...subCategories, { name: isRTL ? item.name_ar : item.name_en, id: item.id }]
        })


        this.setState({
          eventDesigns: response.data.data,
          eventDesignsLoad: false,
          departments: [
            {
              name: strings.categories,
              id: 1,
              children: subCategories
            }
          ],
          //selectDepartment: response.data.data[0]
        })
      })
      .catch(error => {
       
        this.setState({ eventDesigns404: true, eventDesignsLoad: false, })
      })
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isRTL != this.props.isRTL) {
      this.getSubCategories()
    }
    //enableSideMenu(true, this.props.isRTL)
  }

  searchSection = () => {
    const { isRTL } = this.props
    const { countries, selectCountry, cities, selectCity, departments, selectDepartment, searchCategories, selectCategory } = this.state
    return (
      <>
        <View style={{ marginTop: moderateScale(7), alignSelf: 'center' }} >

          <SectionedMultiSelect
            styles={{
              selectToggle: { width: responsiveWidth(70), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', borderColor: colors.darkGray, borderStyle: 'solid', borderWidth: 0.5 },
              selectToggleText: { textAlign: 'center', color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
              subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              container: { minHeight: responsiveHeight(50), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
              button:{backgroundColor:colors.blueTabs}

            }}
            items={countries}
            alwaysShowSelectText
            single
            uniqueKey="id"
            subKey="children"
            selectText={selectCountry ? selectCountry.name : strings.selectCountry}
            readOnlyHeadings={true}
            expandDropDowns
            showDropDowns={false}
            modalWithTouchable
            hideSearch 
            confirmText={strings.close}
            searchPlaceholderText={strings.search}
            onSelectedItemsChange={(selectedItems) => {
              // this.setState({ countries: selectedItems });
            }
            }
            onSelectedItemObjectsChange={(selectedItems) => {
              //console.log("ITEM2   ", selectedItems[0].name)
              this.setState({ selectCountry: selectedItems[0] });
              this.getCities(selectedItems[0].id)
            }
            }
          />


          <SectionedMultiSelect
            styles={{
              selectToggle: { marginTop: moderateScale(3), width: responsiveWidth(70), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', borderColor: colors.darkGray, borderStyle: 'solid', borderWidth: 0.5 },
              selectToggleText: { textAlign: 'center', color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
              subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(8), alignSelf: 'center' },
              button:{backgroundColor:colors.blueTabs}
            }}
            items={cities}
            alwaysShowSelectText
            single
            noItemsComponent={<Text style={{textAlign:'center', marginTop:moderateScale(20), fontFamily:isRTL?arrabicFont : englishFont, fontSize:responsiveFontSize(7)}}>{strings.mustChooseCountryFirst}</Text>}
            uniqueKey="id"
            subKey="children"
            selectText={selectCity ? selectCity.name : strings.selectCity}
            readOnlyHeadings={true}
            expandDropDowns
            showDropDowns={false}
            modalWithTouchable
            hideSearch
            confirmText={strings.close}
            searchPlaceholderText={strings.search}
            onSelectedItemsChange={(selectedItems) => {
              // this.setState({ countries: selectedItems });
            }
            }
            onSelectedItemObjectsChange={(selectedItems) => {
              //console.log("ITEM2   ", selectedItems[0].name)
              this.setState({ selectCity: selectedItems[0] });
            }
            }
          />


          {/*<SectionedMultiSelect
            styles={{
              selectToggle: { marginTop: moderateScale(3), width: responsiveWidth(70), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', alignSelf: 'center', borderColor: colors.darkGray, borderStyle: 'solid', borderWidth: 0.5, backgroundColor: colors.white },
              selectToggleText: { color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
              subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
              container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(8), alignSelf: 'center' },
            }}
            items={searchCategories}
            alwaysShowSelectText
            single
            uniqueKey="id"
            subKey="children"
            selectText={selectCategory.name ? selectCategory.name : strings.selectMainCategory}
            readOnlyHeadings={true}
            expandDropDowns
            showDropDowns={false}
            modalWithTouchable
            hideConfirm
            searchPlaceholderText={strings.search}
            onSelectedItemsChange={(selectedItems) => {
              // this.setState({ countries: selectedItems });
            }
            }
            onSelectedItemObjectsChange={(selectedItems) => {
              console.log("ITEM2   ", selectedItems[0].id)
              this.setState({ selectCategory: selectedItems[0], departments: [], selectDepartment: null });
              this.getSubCategoriesById(selectedItems[0])
            }
            }
          />*/}

          <View>
            {this.state.subCategoryLoading == true &&
              <View style={{ position: 'absolute', alignSelf: 'center', justifyContent: 'center', alignItems: 'center', top: moderateScale(5) }} >
                <ActivityIndicator size='large' />
              </View>
            }
            <SectionedMultiSelect
              styles={{
                selectToggle: { marginTop: moderateScale(3), width: responsiveWidth(70), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', borderColor: colors.darkGray, borderStyle: 'solid', borderWidth: 0.5 },
                selectToggleText: { textAlign: 'center', color: 'black', marginTop: moderateScale(6), marginLeft: moderateScale(4), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
                subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(8), alignSelf: 'center' },
                button:{backgroundColor:colors.blueTabs}
              }}
              items={departments}
              alwaysShowSelectText
              single
              uniqueKey="id"
              subKey="children"
              selectText={selectDepartment ? selectDepartment.name : strings.selectCategory}
              readOnlyHeadings={true}
              expandDropDowns
              showDropDowns={false}
              modalWithTouchable
              confirmText={strings.close}
              searchPlaceholderText={strings.search}
              onSelectedItemsChange={(selectedItems) => {
                // this.setState({ countries: selectedItems });
              }
              }
              onSelectedItemObjectsChange={(selectedItems) => {
                //console.log("ITEM2   ", selectedItems[0].name)
                this.setState({ selectDepartment: selectedItems[0] });
              }
              }
            />

          </View>

          <Button onPress={() => {
            //push('ServiceProviders')
            //[this.setState({providerType:'byCurrentSearch'}),
            this.setState({providerType:'byCurrentSearch', providersLoad:true})
            this.page=1
            this.getProviderOnMap(false,1)
          }} style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(6), backgroundColor: colors.darkBlue, borderRadius: moderateScale(3), alignSelf: 'center', marginTop: moderateScale(10) }} >
            <Icon name='search' type='FontAwesome' style={{ color: colors.white, fontSize: responsiveFontSize(7) }} />
            <Text style={{ marginHorizontal: moderateScale(0), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{strings.search}</Text>
          </Button>

        </View>
      </>
    )
  }

  getProvidrs = (refresh, page) => {
 
    if (refresh) {
      this.setState({ providersRefresh: true })
    }
    axios.get(`${BASE_END_POINT}service/ads/provider/${this.props.data.id}?page=${page}`)
      .then(response => {
       
        //response.data.data.providers
        this.setState({
          pages: response.data.data.paginate.last_page,
          providersRefresh: false,
          providers: refresh ? response.data.data.providers : [...this.state.providers, ...response.data.data.providers],
          providersLoad: false,
        })
      })
      .catch(error => {
       
        this.setState({ providersRefresh: false, providers404: true, providersLoad: false, })
      })
  }

  getProvidersBySubCategory = (refresh, page) => {
    const { data } = this.props

    if (refresh) {
      this.setState({ providersRefresh: true })
    }

    
    axios.get(`${BASE_END_POINT}providers/${data.data.id}?page=${page}`)
      .then(response => {

        //response.data.data.providers
    
        if (response.data) {
          this.setState({
           
            pages: response.data.data.paginate.last_page,
            providersRefresh: false,
            providers: refresh ? response.data.data.providers : [...this.state.providers, ...response.data.data.providers],
            //providers:response.data.data.providers,  
            providersLoad: false,
          })
        }
      })
      .catch(error => {
      
        this.setState({ providersRefresh: false, providers404: true, providersLoad: false, })
      })
  }

  getProvidersBySearch = (refresh, page) => {

    const { data } = this.props
 
    if (refresh) {
      this.setState({ providersRefresh: true })
    }

    /*var d ={}
    //optional
    if(data.selectCountry){
      d['country_id']=data.selectCountry
    }

    if(data.selectCity){
      d['city_id']=data.selectCity
    }

    if(data.selectDepartment){
      d['sub_category_id']=data.selectDepartment
    }*/

    var d = new FormData()

    if (data.selectCountry) {
      d.append('country_id', data.selectCountry)
    }

    if (data.selectCity) {
      d.append('city_id', data.selectCity)
    }

    if (data.selectDepartment) {
      d.append('sub_category_id', data.selectDepartment)
    }

    if (!data.selectCountry && !data.selectCity && !data.selectDepartment) {
      d = null
    }


    axios.post(`${BASE_END_POINT}search?page=${page}`, d)
      .then(response => {
       
        //response.data.data.providers
        this.setState({
          pages: response.data.data.paginate.last_page,
          providersRefresh: false,
          providers: refresh ? response.data.data.providers : [...this.state.providers, ...response.data.data.providers],
          //providers: response.data.data.providers,
          providersLoad: false,
        })
      })
      .catch(error => {
       
        this.setState({ providersRefresh: false, providers404: true, providersLoad: false, })
      })
  }


  getProviderOnMap = (refresh, page) => {
    const { selectCountry, selectCity, selectDepartment } = this.state
    if (refresh) {
      this.setState({ providersRefresh: true })
    }
    /* var data = {}
     if (selectCountry) {
       data['country_id'] = selectCountry.id
     }
     if (selectCity) {
       data['city_id'] = selectCity.id
     }
     if (selectDepartment) {
       data['sub_category_id'] = selectDepartment.id
     }*/
     //console.log('IDDDDDDD:', selectCity.id)

    var d = new FormData()

    if (selectCountry) {
      d.append('country_id', selectCountry.id)
    }

    if (selectCity) {
      d.append('city_id', selectCity.id)
    }

    if (selectDepartment) {
      d.append('sub_category_id', selectDepartment.id)
    }

    if (!selectCountry && !selectCity && !selectDepartment) {
      d = null
    }
    //console.log('fffff:',selectDepartment.id)
    //console.log("MY D", data)

    
    axios.post(`${BASE_END_POINT}search?page=${page}`, d)
      .then(response => {
     
        this.setState({ providersLoad: false, 
          //providers: response.data.data.providers
          pages: response.data.data.paginate.last_page,
          providers: refresh ? response.data.data.providers : page ==1 ? response.data.data.providers : [...this.state.providers, ...response.data.data.providers],
          providersRefresh: false,
         })
        /*if (response.data.data.providers.length == 0) {
          RNToasty.Info({ title: strings.noResultsFound })
        }*/
      })
      .catch(error => {
      
        this.setState({ providersRefresh: false, providers404: true, providersLoad: false, })
        //this.setState({ providersLoad: false })
      })

  }

  content = () => {
    const { isRTL, barColor } = this.props
    const { pages, providers, providers404, providersLoad, providersRefresh,providerType } = this.state
    return (
      <View>
        {this.searchSection()}
        {
          providers404 ?
            <NetworError />
            :
            providersLoad ?
              <View style={{ marginTop: moderateScale(20) }} >
                <ActivityIndicator size='large' />
              </View>
              :
              providers.length > 0 ?
                <FlatList
                  style={{ marginBottom: moderateScale(10), alignSelf: 'center' }}
                  numColumns={2}
                  data={providers}
                  renderItem={({ item }) => <ServiceProviderCard data={item} />}
                  onEndReachedThreshold={0.5}
                  onEndReached={({ distanceFromEnd }) => {
                    
                    //if (distanceFromEnd == 0) {
                      this.page = this.page + 1;
                      if (this.page <= pages) {
                        
                        if (providerType == 'bySubCategory') {
                          this.getProvidersBySubCategory(false, this.page)
                        } //else {
                          //this.getProvidrs(false,this.page)
                         else if (providerType == 'byCurrentSearch') {
                            
                            this.getProviderOnMap(false, this.page)
                          } else if (providerType == 'bySearch') {
                            this.getProvidersBySearch(false, this.page)
                            
                          }
                          else {
                            this.getProvidrs(false, this.page)
                          }


                        //}

                        
                      }
                    //}
                  }}


                  refreshControl={
                    <RefreshControl
                      colors={["#B7ED03", colors.darkBlue]}
                      refreshing={providersRefresh}
                      onRefresh={() => {
                        this.page = 1
                        if (providerType == 'bySubCategory') {
                          this.getProvidersBySubCategory(true, 1)
                        } else {
                          //this.getProvidrs(true, 1)

                          if (providerType == 'byCurrentSearcch') {
                            this.getProviderOnMap(true, this.page)
                          } else if (providerType == 'bySearch') {
                            this.getProvidersBySearch(false, this.page)
                            
                          }
                          else {
                            this.getProvidrs(false, this.page)
                          }

                        }

                      }}
                    />
                  }


                />
                :
                <View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(20) }} >
                  <FastImage
                    style={{ width: responsiveWidth(40), height: responsiveHeight(30) }}
                    source={require('../assets/imgs/noData.png')}
                  />
                  <Text style={{ fontFamily: englishFont, alignSelf: 'center', fontSize: responsiveFontSize(8), marginTop: moderateScale(-10) }} >{strings.weWillHereSoon}</Text>
                </View>
        }
      </View>
    )
  }



  render() {
    const { currentUser, isRTL } = this.props;
    return (

      /*<ReactNativeParallaxHeader
        scrollViewProps={{ showsVerticalScrollIndicator: false }}
        headerMinHeight={responsiveHeight(10)}
        headerMaxHeight={responsiveHeight(35)}
        extraScrollHeight={20}
        navbarColor={colors.darkBlue}
        backgroundImage={require('../assets/imgs/header.png')}
        backgroundImageScale={1.2}
        renderNavBar={() => <CollaspeAppHeader back title={Strings.serviceProviders} />}
        renderContent={() => this.content()}
        containerStyle={{ flex: 1 }}
        contentContainerStyle={{ flexGrow: 1 }}
        innerContainerStyle={{ flex: 1, }}
      />*/


      <ParallaxScrollView
      backgroundColor={colors.darkBlue}
      contentBackgroundColor="white"
      renderFixedHeader={() => <CollaspeAppHeader back title={Strings.serviceProviders} />}
      parallaxHeaderHeight={responsiveHeight(35)}
      renderBackground={() => (
          <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

      )}
  >
      {this.content()}
  </ParallaxScrollView>


    );
  }
}


const mapDispatchToProps = {
  getUser,
  removeItem
}

const mapToStateProps = state => ({
  isRTL: state.lang.RTL,
  currentUser: state.auth.currentUser,
  barColor: state.lang.color
})

export default connect(mapToStateProps, mapDispatchToProps)(ServiceProviders);