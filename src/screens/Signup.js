import React, { Component } from 'react';
import {
  View,TouchableOpacity,Text,ScrollView,Image,TextInput,Modal,ActivityIndicator,ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage  from '@react-native-community/async-storage'
import { Button,Item, Picker, Label,Icon} from "native-base";
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";

import Strings from  '../assets/strings';
import Checkbox from 'react-native-custom-checkbox';
import { signup,getUser } from '../actions/AuthActions';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import {removeItem} from '../actions/MenuActions';
import { RNToasty } from 'react-native-toasty';
import AppHeader from  '../common/AppHeader'
import CodeInput from 'react-native-confirmation-code-input';
import Swiper from "react-native-swiper";
import moment from 'moment'
import DateTimePicker from "react-native-modal-datetime-picker";
import {arrabicFont,englishFont} from '../common/AppFont'
import AnimateLoadingButton from 'react-native-animate-loading-button';
import * as Animatable from 'react-native-animatable';




class Signup extends Component {
  swiper=null;
 
  constructor(props){
    super(props)
    moment.locale('en')
  }

  state={
    name:' ',
    nameError:Strings.require,
    email: ' ',
    emailError:Strings.require,
    password: ' ',
    passwordError:Strings.require,
    hidePassword:true,
    confirmPassword: ' ',
    confirmHidePassword: true,
    phone: ' ',
    phoneError:Strings.require,
    genderText:Strings.male,
    selectedGender:'male',
    openModal: false,
    agreeTerms:false,
    date:'',
    showDate:false
    
  }


  componentDidMount() {
    this.setState({date:moment(new Date()).format("YYYY-MM-DD")})
  }

  

  render() {
    const  { isRTL,navigator,userToken } = this.props;
    const  {date,hidePassword,confirmHidePassword, nameError,phoneError,emailError,passwordError, name,email,password,confirmPassword,phone,genderText,selectedGender,agreeTerms} = this.state
    return (
      <View style={{ flex: 1,backgroundColor:'white' }}>
        <AppHeader title={Strings.registeration} navigator={navigator} />
        <ScrollView>
        <Animatable.View animation={isRTL?"slideInRight":"slideInLeft"} duration={1000} style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
          <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center', justifyContent:'center', backgroundColor:'white',  marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(0), borderBottomWidth:1,borderBottomColor:'#cccbcb',height:responsiveHeight(8)}}>
              <View style={{marginHorizontal:moderateScale(5),}}>
                  <Icon name='user' type='AntDesign' style={{color:'gray',fontSize:responsiveFontSize(8)}} />
              </View>
              <TextInput 
              onChangeText={(val)=>{this.setState({name:val})}}
              placeholder={Strings.enterYourName} underlineColorAndroid='transparent' style={{fontSize:responsiveFontSize(6),width:responsiveWidth(80), fontFamily:isRTL?arrabicFont:englishFont, marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
          </View>
          {name.length==0&&
          <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}>{nameError}</Text>
          }
          </Animatable.View>

          <Animatable.View animation={isRTL?"slideInLeft":"slideInRight"} duration={1000} style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
          <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center', justifyContent:'center',backgroundColor:'white',marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(0), borderBottomWidth:1,borderBottomColor:'#cccbcb',height:responsiveHeight(8)}}>
              <View style={{marginHorizontal:moderateScale(5),}}>
                  <Icon name='email-outline' type='MaterialCommunityIcons' style={{color:'gray',fontSize:responsiveFontSize(8)}} />
              </View>
              <TextInput
              keyboardType='email-address'
              onChangeText={(val)=>{this.setState({email:val})}}
               placeholder={Strings.enterYourEmail} underlineColorAndroid='transparent' style={{fontSize:responsiveFontSize(6),width:responsiveWidth(80),fontFamily:isRTL?arrabicFont:englishFont, marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
          </View>
          {email.length==0&&
          <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {emailError}</Text>
          }
        </Animatable.View>

        <Animatable.View animation={isRTL?"slideInRight":"slideInLeft"} duration={1000} style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
          <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center', justifyContent:'center',backgroundColor:'white', marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(0), borderBottomWidth:1,borderBottomColor:'#cccbcb',height:responsiveHeight(8)}}>
              <View style={{marginHorizontal:moderateScale(5),}}>
                  <Icon name='phone' type='FontAwesome' style={{color:'gray',fontSize:responsiveFontSize(8)}} />
              </View>
              <TextInput
              keyboardType='phone-pad'
               onChangeText={(val)=>{this.setState({phone:val})}}
               placeholder={Strings.enterYourPhone} underlineColorAndroid='transparent' style={{fontSize:responsiveFontSize(6),width:responsiveWidth(80), fontFamily:isRTL?arrabicFont:englishFont, marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
          </View>
          {phone.length==0&&
          <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {phoneError}</Text>
          }
        </Animatable.View>

        <Animatable.View animation={isRTL?"slideInLeft":"slideInRight"} duration={1000} 
         style={{ marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
          <TouchableOpacity
          onPress={()=>{this.setState({openModal:true})}} 
          style={{backgroundColor:'white',alignItems:'center', flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(0), borderBottomWidth:1,borderBottomColor:'#cccbcb',height:responsiveHeight(8)}}
          >
             <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'center'}}>
             <View style={{marginHorizontal:moderateScale(5),}}>
                  <Icon name='gender-male-female' type='MaterialCommunityIcons' style={{color:'gray',fontSize:responsiveFontSize(8)}} />
              </View>
            <Text style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont, marginHorizontal:moderateScale(0)}}>{genderText}</Text>
            </View>

          <Icon name='chevron-small-down' type='Entypo'  style={{marginHorizontal:moderateScale(5), color:'black',fontSize:responsiveFontSize(7)}} />
          </TouchableOpacity>
        </Animatable.View>
        
        <Modal
         onRequestClose={()=>this.setState({openModal:false})}
        animationType="slide"
        transparent={false}
         visible={this.state.openModal}
         >
            <TouchableOpacity
            onPress={()=>{this.setState({openModal:false})}}
             style={{alignSelf:'flex-end',margin:moderateScale(10)}}>
              <Icon name='closecircle' type='AntDesign' />
            </TouchableOpacity>
            <TouchableOpacity
                onPress={()=>this.setState({selectedGender:'male', genderText:Strings.male, openModal:false})}
                style={{flexDirection:'row',justifyContent:isRTL?'flex-end':'flex-start',marginBottom:moderateScale(5), borderBottomWidth:0.4,borderBottomColor:'gray', flexDirection:'row',width:responsiveWidth(100),marginTop:moderateScale(5)}}>
                <Text style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont, alignSelf:isRTL?'flex-end':'flex-start',marginHorizontal:moderateScale(5),fontSize:responsiveFontSize(7)}} >{Strings.male}</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={()=>this.setState({selectedGender:'female', genderText:Strings.female, openModal:false})}
                style={{flexDirection:'row',justifyContent:isRTL?'flex-end':'flex-start',  marginBottom:moderateScale(5), borderBottomWidth:0.4,borderBottomColor:'gray',width:responsiveWidth(100),marginTop:moderateScale(5)}}>
                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont, marginHorizontal:moderateScale(5),fontSize:responsiveFontSize(7)}}>{Strings.female}</Text>
            </TouchableOpacity>
        </Modal>

        <Animatable.View animation={isRTL?"slideInRight":"slideInLeft"} duration={1000} style={{marginVertical:moderateScale(7), width:responsiveWidth(90),alignSelf:'center'}}>
          <TouchableOpacity 
          onPress={()=>{
            this.setState({showDate:true})
          }}
           activeOpacity={1} style={{borderRadius:moderateScale(0), borderBottomWidth:1,borderBottomColor:'#cccbcb',justifyContent:'center', height:responsiveHeight(8), marginTop:moderateScale(3),width:responsiveWidth(90),backgroundColor:'white'}}>
           <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont, fontSize:responsiveFontSize(7), textAlign:isRTL?'right':'left',marginHorizontal:moderateScale(5)}} >{date}</Text>
          </TouchableOpacity>
         
        </Animatable.View>

        <DateTimePicker
            
            mode='date'
            isVisible={this.state.showDate}
            onConfirm={(val)=>{
            //const d = new Date(val).toISOString().substring(0,10);
            //console.log('Date   ',d)
             this.setState({showDate:false,date:moment(val).format("YYYY-MM-DD")})
            }}
            onCancel={ ()=>{ this.setState({showDate:false}) } }
            />


        <Animatable.View animation={isRTL?"slideInLeft":"slideInRight"} duration={1000} style={{marginTop:moderateScale(12), width:responsiveWidth(90),alignSelf:'center'}}>
                    <View style={{justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row',alignItems:'center', backgroundColor:'white', elevation:0.2, marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(0), borderBottomWidth:1,borderBottomColor:'#cccbcb',height:responsiveHeight(8)}}>
                    <View style={{marginHorizontal:moderateScale(5),}}>
                        <Icon name='lock1' type='AntDesign' style={{color:'gray',fontSize:responsiveFontSize(8)}} />
                    </View>
                        <TextInput         
                        secureTextEntry={hidePassword}
                        onChangeText={(val)=>{this.setState({password:val})}}
                        placeholder={Strings.enterYourPassword} underlineColorAndroid='transparent' style={{fontFamily:isRTL?arrabicFont:englishFont, textAlign:isRTL?'right':'left', width:responsiveWidth(75), marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
                        <TouchableOpacity 

                        onPress={()=>{this.setState({hidePassword:!hidePassword})}}
                        >
                            <Icon style={{fontSize:responsiveFontSize(6), color:'#D6D6D6'}} name={hidePassword?'eye-off':'eye'} type='Feather' />
                        </TouchableOpacity>
                    </View>
                    {password.length==0&&
                      <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {passwordError}</Text>
                    }
          </Animatable.View>

      
        
          <Animatable.View animation={isRTL?"slideInRight":"slideInLeft"} duration={1000} style={{marginTop:moderateScale(12), width:responsiveWidth(90),alignSelf:'center'}}>
           <View style={{justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row',alignItems:'center', backgroundColor:'white',  marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(0), borderBottomWidth:1,borderBottomColor:'#cccbcb',height:responsiveHeight(8)}}>
              <View style={{marginHorizontal:moderateScale(5),}}>
                  <Icon name='lock1' type='AntDesign' style={{color:'gray',fontSize:responsiveFontSize(8)}} />
              </View>

            <TextInput         
            secureTextEntry={confirmHidePassword}
            onChangeText={(val)=>{this.setState({confirmPassword:val})}}
            placeholder={Strings.enterYourConfirmPassword} underlineColorAndroid='transparent' style={{fontFamily:isRTL?arrabicFont:englishFont, width:responsiveWidth(75),fontSize:responsiveFontSize(6), marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr',textAlign:isRTL?'right':'left'}}  />
            <TouchableOpacity 
             
              onPress={()=>{this.setState({confirmHidePassword:!confirmHidePassword})}}
            >
              <Icon style={{fontSize:responsiveFontSize(6), color:'#D6D6D6'}} name={confirmHidePassword?'eye-off':'eye'} type='Feather' />
              </TouchableOpacity>
              </View>
              {confirmPassword.length==0&&
              <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
              }
          </Animatable.View>
      

        <TouchableOpacity style={{alignItems:'center',marginTop:moderateScale(6), marginHorizontal:moderateScale(7),flexDirection:isRTL?'row-reverse':'row',alignSelf:isRTL?'flex-end':'flex-start'}}>
        <Checkbox
            checked={this.state.agreeTerms}
            style={{backgroundColor: "white",color: 'black',borderRadius: 5}}
            onChange={(name, checked) => {
              this.setState({ agreeTerms: checked });
            }}
          />
          <Text style={{fontFamily:isRTL?arrabicFont:englishFont, fontSize:responsiveFontSize(6), color:'black',marginHorizontal:moderateScale(4)}}>{Strings.agreeToOurPrivacy}</Text>
        </TouchableOpacity>

        <View style={{marginTop:moderateScale(10)}} >
                <AnimateLoadingButton
                ref={c => (this.loadingButton = c)}
                width={responsiveWidth(80)}
                height={responsiveHeight(8)}
                title={Strings.signup}
                titleFontSize={responsiveFontSize(8)}
                titleColor="white"
                backgroundColor="#679C8A"
                borderRadius={moderateScale(3)}
                onPress={()=>{
                  this.loadingButton.showLoading(this.props.loading);
                  if(this.state.agreeTerms){
                  if(!this.props.loading){ 
                  if(name.replace(/\s/g, '').length<3){
                    this.setState({name:'',nameError:Strings.nameLength})
                  }
                  if(phone.replace(/\s/g, '').length<11){
                    this.setState({phone:'',phoneError:Strings.phoneLength})
                  }
                  if(password.replace(/\s/g, '').length<8){
                    this.setState({password:'',passwordError:Strings.passwordLength})
                  }       
                  if(!phone.replace(/\s/g, '').length){
                    this.setState({phone:'',phoneError:Strings.require})
                  }
                  if(!name.replace(/\s/g, '').length){
                    this.setState({name:'',nameError:Strings.require})
                  }
                  if(!email.replace(/\s/g, '').length){
                    this.setState({email:'',emailError:Strings.require})
                  }
                  if(!password.replace(/\s/g, '').length){
                    this.setState({password:'',passwordError:Strings.require})
                  }
                  if(!confirmPassword.replace(/\s/g, '').length){
                    this.setState({confirmPassword:''})
                  }
        
                  if(password!=confirmPassword){
                    RNToasty.Error({title:Strings.errorConfirmPassword})
                  }
        
                  if( password.replace(/\s/g, '').length>=8&& name.replace(/\s/g, '').length>=3&&phone.replace(/\s/g, '').length>=11&& password==confirmPassword&&phone.replace(/\s/g, '').length&&name.replace(/\s/g, '').length&&email.replace(/\s/g, '').length&&password.replace(/\s/g, '').length&&confirmPassword.replace(/\s/g, '').length){
                   
                     const data = new FormData()
                     data.append('name',name)
                     data.append('email',email.toLocaleLowerCase())
                     data.append('phone',phone)
                     data.append('gender',selectedGender)
                     data.append('birth_date',date)
                     data.append('password',password) 
        
                    /* const data = {
                      name:name,
                      email:email.toLocaleLowerCase(),
                      phone:phone,
                      gender:selectedGender,
                      birth_date:date,
                      password:password
                     } */     
                    this.props.signup(data,navigator)  
                    this.loadingButton.showLoading(true);       
                  }
                }
              }
                  
                }}
                 />
         </View>       

       

        <View style={{marginVertical:moderateScale(6),width:responsiveWidth(87),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:isRTL?'flex-end':'flex-start'}}>
            <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'black',fontSize:responsiveFontSize(6)}}>{Strings.alreadHaveAccount}</Text>
            <TouchableOpacity
            onPress={()=>{
              navigator.push({
                screen:"Login",
                animated: true,
                animationType:'slide-horizontal'
              })
            }}
             style={{marginVertical:moderateScale(2)}}>
              <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'black',fontSize:responsiveFontSize(7)}}>  {Strings.login}</Text>
            </TouchableOpacity>
        </View>
       
      </ScrollView>

        
      </View>
    );
  }
}

const mapDispatchToProps = {
    signup,
    removeItem,
    getUser
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    loading: state.auth.loading,
    barColor: state.lang.color,
    userToken: state.auth.userToken, 
})


export default connect(mapToStateProps,mapDispatchToProps)(Signup);

