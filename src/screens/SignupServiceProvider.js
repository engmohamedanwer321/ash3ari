import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform, Switch
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button, Radio } from 'native-base';
import { login, setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../controlls/NavigationControll'
import AnimateLoadingButton from 'react-native-animate-loading-button';
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader';
import { removeItem } from '../actions/MenuActions';
import { getAdsCategories } from '../actions/CategoriesActions';
import StepIndicator from 'react-native-step-indicator';
import Checkbox from 'react-native-custom-checkbox';
import RadioButton from 'radio-button-react-native';
import AsyncStorage from '@react-native-community/async-storage'
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-crop-picker';
import CollaspeAppHeader from '../common/CollaspeAppHeader';
import { BASE_END_POINT } from '../AppConfig'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import MapView, { Marker } from 'react-native-maps';
import { RNToasty } from 'react-native-toasty'
import InputValidations from '../common/InputValidations'
import axios from 'axios'
import Geolocation from '@react-native-community/geolocation';
import GetLocation from 'react-native-get-location'
import strings from '../assets/strings';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import AppSuccessDialog from '../common/AppSuccessDialog'






class SignupServiceProvider extends Component {

    countryKeys = [
        // this is the parent or 'item'
        {
            name: Strings.countryKey,
            id: 0,
            // these are the children or 'sub items'
            children: [
                {
                    name: '966',
                    id: 966,
                },
                {
                    name: '971',
                    id: 971,
                },
                {
                    name: '965',
                    id: 965,
                },
                {
                    name: '973',
                    id: 973,
                },
                {
                    name: '968',
                    id: 968,
                },
            ],
        },
    ];

    state = {
        currentPage: this.props.data ? this.props.data.isVerify ? 4 : 0 : 0,
        currentUserData: this.props.data ? this.props.data.userData : null,
        providerType: 'company', //select type company or individual
        companyType: 'MNC', companyTypeUpdt: ' ',
        name: '', nameUpdt: ' ',
        address: '', addressUpdt: ' ',
        selectCountryKey: '966',
        countries: [],
        categories: [],
        subCategories: [],
        logisticsCharitableType: 0, logisticsDeliveryType: 0, logisticsMapType: 0,
        selectCountry: Strings.selectCountry,
        selectCity: Strings.selectCity, selectCityUpdt:' ',
        selectRegion: Strings.selectRegion, selectRegionUpdt: ' ',
        cities: [],
        selectCity: null,
        area: [],
        selectArea: null,
        countryKey: '', countryKeyUpdt: ' ',
        phone: '', phoneUpdt: ' ',
        email: '', emailUpdt: ' ',
        image: null, imageUpdt: ' ',
        password: '', passwordUpdt: ' ',
        selectAdsCategoryId: '', selectAdsCategory: '', selectAdsCategoryUpdt: ' ',
        hidePassword: true,
        confirmPassword: '', confirmPasswordUpdt: ' ', confirmPasswordValidate: '',
        hideConfirmPassword: true,
        description: '', descriptionUpdt: ' ',
        logisticsType: 0,
        employeeNumbers: '', employeeNumbersUpdt: ' ',
        creationYear: '', creationYearUpdt: ' ',
        commercialRegister: '', commercialRegisterUpdt: ' ',
        primarEducation: '', primarEducationUpdt: ' ',
        intermediatEducation: '', intermediatEducationUpdt: ' ',
        universitdEucation: '', universitdEucationUpdt: ' ',
        verifactionCode: '', verifactionCodeUpdt: ' ',
        mainCategories: [],
        selectMainCategory: null,
        subCategories: [],
        ads: [],
        selectSubCategory: '', selectSubCategoryId: '', selectSubCategoryUpdt: ' ',
        selectSubCategoriesIds: [],
        selectCountryID: ' ',
        countries: [{
            location: {
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
            }, name: 'Countries',
            id: 0
        }],
        location: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
        },
        selectCityID: ' ',
        /*cities: [{
            name: 'Cities',
            id: 0
        }],*/
        selectRegionID: ' ',
        regions: [{
            name: 'Regions',
            id: 0
        }],
        availabilityEmail: true,
        availabilityPhone: true,
        loading: false,
        maroofAccountSwitch: false,
        freelanceAccountSwitch: false,
        verifySuccess: false,
        passwordMinLength: false

    }

    componentDidMount() {
        enableSideMenu(false, null)
        //Alert.alert('Test')
        this.getCurrentLocation()
        this.getCountries()
        this.getCategories()
        this.getAds()

    }

    componentDidUpdate(prevProps) {
        if (prevProps.isRTL != this.props.isRTL) {
            this.getCategories()
        }
        //enableSideMenu(true, this.props.isRTL)
    }

    getCurrentLocation = () => {
        /*  Geolocation.getCurrentPosition(
              (location) => {
                  console.log('location', location)
                  //Alert.alert("LAt  "+location.coords.latitude)
                  this.setState({
                      location: {
                          latitude: location.coords.latitude,
                          longitude: location.coords.longitude,
                          latitudeDelta: 0.015,
                          longitudeDelta: 0.0121
                      }
                  })
              }
          );*/


        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 200000,
        })
            .then(location => {

                this.setState({
                    location: {
                        latitude: location.coords.latitude,
                        longitude: location.coords.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121
                    }
                })
            })
            .catch(error => {
                const { code, message } = error;
                // console.warn(code, message);
            })
    }

    getCountries = () => {
        this.setState({ loginLoading: true })
        axios.get(`${BASE_END_POINT}locations/countries`)
            .then(response => {
                //Alert.alert('Done')
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {


                    var newCountries = [{
                        name: Strings.countries,
                        id: 0, children: response.data.data
                    }]

                    this.setState({ countries: newCountries })
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {
                //Alert.alert('Error   ',error)

                this.setState({ loginLoading: false })
            })
    }

    getCities = (countryId) => {

        this.setState({ loginLoading: true })

        axios.get(`${BASE_END_POINT}locations/cities/` + countryId)
            .then(response => {
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {


                    var newCities = [{
                        name: Strings.cities,
                        id: 0, children: response.data.data
                    }]

                    this.setState({ cities: newCities })
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {

                this.setState({ loginLoading: false })
            })
    }

    getRegions = (cityId) => {
        this.setState({ loginLoading: true })

        axios.get(`${BASE_END_POINT}locations/regions/` + cityId)
            .then(response => {
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {


                    var newRegions = [{
                        name: Strings.regions,
                        id: 0, children: response.data.data
                    }]

                    this.setState({ regions: newRegions })
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {

                this.setState({ loginLoading: false })
            })
    }

    ///////////// Get Categories
    getCategories = () => {
        const { isRTL } = this.props
        this.setState({ loginLoading: true })
        axios.get(`${BASE_END_POINT}categories`)
            .then(response => {
                //Alert.alert('Done')
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {

                    var categories = []
                    response.data.data.map((item) => {
                        categories = [...categories, { name: isRTL ? item.name_ar : item.name_en, id: item.id }]
                    })

                    var newCategories = [{
                        name: Strings.mainCategory,
                        id: 0, children: categories
                    }]

                    this.setState({ categories: newCategories })
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {
                //Alert.alert('Error   ',error)

                this.setState({ loginLoading: false })
            })
    }
    ///////// 
    getSubCategories = (selectCategoryId) => {
        const { isRTL } = this.props
        this.setState({ loginLoading: true, subCategories: [], selectSubCategory: '' })
        axios.get(`${BASE_END_POINT}sub_categories/${selectCategoryId}`)
            .then(response => {
                //Alert.alert('Done')
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {

                    var subCategories = []
                    response.data.data.map((item) => {
                        subCategories = [...subCategories, { name: isRTL ? item.name_ar : item.name_en, id: item.id }]
                    })

                    var newSubCategories = [{
                        name: Strings.subCategory,
                        id: 0, children: subCategories
                    }]

                    var selectSubCategoriesIds = this.state.selectSubCategoriesIds
                    subCategories.map(item => {
                        selectSubCategoriesIds.push(item)
                    })

                    var obj = {};

                    for (var i = 0, len = selectSubCategoriesIds.length; i < len; i++) {
                        obj[selectSubCategoriesIds[i]['id']] = selectSubCategoriesIds[i];
                    }
                    selectSubCategoriesIds = new Array();
                    for (var key in obj) {
                        selectSubCategoriesIds.push(obj[key]);
                    }

                    selectSubCategoriesIds = selectSubCategoriesIds.filter((x, i, a) => a.indexOf(x) == i)

                    console.log('selectSubCategoriesIds', selectSubCategoriesIds)
                    this.setState({ subCategories: newSubCategories, selectSubCategoriesIds: selectSubCategoriesIds })
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {
                //Alert.alert('Error   ',error)

                this.setState({ loginLoading: false })
            })
    }


    getAds = () => {
        axios.get(`${BASE_END_POINT}get-ads-categories`)
            .then(response => {

                var newAds = [{
                    name: Strings.adsCategory,
                    id: 0, children: response.data.data
                }]
                this.setState({ ads: newAds })
                // this.setState({ ads: response.data.data, })
            })
            .catch(error => {

                this.setState({ ads404: true, })
            })
    }

    checkEmail = (email) => {
        var data = new FormData()
        data.append('email', email)

        axios.post(`${BASE_END_POINT}check_email`, data)
            .then(response => {

                const res = response.data

                if (response.data.status == true) {
                    this.setState({ availabilityEmail: true })
                } else {

                    this.setState({ availabilityEmail: false })
                }
            })
            .catch(error => {
                // RNToasty.Error({ title: 'fdfdf' })

            })
    }

    checkPhone = (phone) => {
        const { selectCountryKey } = this.state
        var data = new FormData()
        data.append('phone', phone)
        data.append('code', selectCountryKey)
        axios.post(`${BASE_END_POINT}check_phone`, data)
            .then(response => {

                const res = response.data
console.log('availabilityEmail', res)
                if (response.data.status == true) {
                    this.setState({ availabilityPhone: true })
                } else {

                    this.setState({ availabilityPhone: false })
                }
            })
            .catch(error => {
                // RNToasty.Error({ title: 'fdfdf' })

            })
    }


    signup = () => {
        const { name, image, selectCountryKey, selectCityID, phone, email, password, selectRegionID, selectAdsCategoryId, providerType, description, logisticsCharitableType, logisticsDeliveryType, logisticsMapType, employeeNumbers, creationYear, commercialRegister, companyType, location, selectSubCategoryId, selectSubCategoriesIds, maroofAccountSwitch, freelanceAccountSwitch } = this.state

        console.log('DATA:', name, phone, email, password)
        if (name.replace(/\s/g, '').length && phone.replace(/\s/g, '').length && email.replace(/\s/g, '').length && password.replace(/\s/g, '').length && InputValidations.validateEmail(email) == true) {
            this.setState({ signupLoading: true })
            var data = new FormData()
            data.append('name', name)
            data.append('email', email)
            data.append('code', selectCountryKey)
            data.append('phone', phone)
            data.append('password', password)
            data.append('city_id', selectCityID)
            if (image) {
                data.append('image', {
                    uri: image,
                    type: 'multipart/form-data',
                    name: 'photoImage'
                })
            }


            data.append('job', 'مدرس')
            selectSubCategoriesIds.map(item => {
                data.append('sub_categories[]', item.id)
            })

            data.append('bio', description)
            data.append('lat', location.latitude)
            data.append('lng', location.longitude)
            data.append('discount', '')
            //data.append('emp_no', employeeNumbers)
            //data.append('creation_year', creationYear)
            if ((commercialRegister.toString()).replace(/\s/g, '') != '') {
                data.append('commerical_no', commercialRegister)
            }
            if (maroofAccountSwitch == true) { data.append('account_maroof', 'yes') } else { data.append('account_maroof', 'no') }
            if (freelanceAccountSwitch == true) { data.append('account_freelancer', 'yes') } else { data.append('account_freelancer', 'no') }
            console.log('maroofAccountSwitch', maroofAccountSwitch)
            console.log('freelanceAccountSwitch', freelanceAccountSwitch)


            data.append('map', logisticsMapType)
            data.append('delivery', logisticsDeliveryType)
            data.append('charitable', logisticsCharitableType)
            data.append('provider_type', providerType)
            data.append('provider_company_type', companyType)
            if ((selectAdsCategoryId.toString()).replace(/\s/g, '').length) {
                data.append('ads_category', selectAdsCategoryId)
            }


            axios.post(`${BASE_END_POINT}auth/register/provider`, data)
                .then(response => {
                    this.setState({ signupLoading: false })
                    const res = response.data

                    if ('data' in res) {

                        //AsyncStorage.setItem('USER', JSON.stringify(res))
                        //AsyncStorage.setItem('VERIFY', 'yes')
                        //this.props.setUser(res)
                        this.setState({ currentPage: 4, currentUserData: res })
                    } else {
                        RNToasty.Error({ title: res.msg })

                    }
                })
                .catch(error => {
                    RNToasty.Error({ title: error.response.data.message })

                    this.setState({ signupLoading: false })

                })
        }
    }

    verify = () => {
        const { currentUser } = this.props
        const { verifactionCode, currentUserData } = this.state


        this.setState({ loading: true })
        var data = new FormData()
        data.append('phone', currentUserData.data.phone.split(currentUserData.data.phone.substring(0, 3).toString())[1])
        data.append('verification_code', verifactionCode)
        data.append('code', currentUserData.data.phone.substring(0, 3).toString())

        axios.post(`${BASE_END_POINT}auth/activate`, data, {
            headers: {
                Authorization: `Bearer ${currentUserData.data.token}`
            }
        })
            .then(response => {
                this.setState({ loading: false })
                const res = response.data
                if ('msg' in res) {
                    RNToasty.Error({ title: res.msg })

                } else {
                    this.props.setUser(res)
                    this.setState({verifySuccess:true})
                    resetTo('Home')
                    RNToasty.Success({ title: Strings.welcome })

                    AsyncStorage.setItem('USER', JSON.stringify(response.data))
                    this.props.setUser(response.data)
                }

            })
            .catch(error => {

                this.setState({ loading: false })
            })
    }

    resendCode = () => {
        const { currentUser } = this.props
        var data = new FormData()
        data.append('phone', currentUser.data.phone)
        axios.post(`${BASE_END_POINT}auth/resend_code`, data, {
            headers: {
                "Content-Type": `multipart/form-data`
            }
        })
            .then(response => {
                this.setState({ loading: false })
                const res = response.data
                RNToasty.Success({ title: Strings.resenCodeDone })

            })
            .catch(error => {

            })
    }



    nameInput = () => {
        const { name, nameUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.name}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ name: val, nameUpdt: val }) }}
                        placeholder={Strings.name}
                        placeholderTextColor={colors.lightGray}
                        value={name} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {nameUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    addressInput = () => {
        const { address, addressUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.address}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ address: val, addressUpdt: val }) }}
                        placeholder={Strings.address}
                        placeholderTextColor={colors.lightGray}
                        value={address} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }}
                    />
                </View>
                {addressUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    countryKeyInput = () => {
        const { countryKey, selectCountryKey } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.countryKey}</Text>*/}
                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', color: 'white' },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { height: responsiveHeight(45), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(22), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={this.countryKeys}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        selectText={this.state.selectCountryKey}
                        readOnlyHeadings={true}
                        expandDropDowns
                        showDropDowns={false}
                        modalWithTouchable
                        confirmText={strings.close}
                        searchPlaceholderText={Strings.search}
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }
                        onSelectedItemObjectsChange={(selectedItems) => {
                            //console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectCountryKey: selectedItems[0].name });


                        }
                        }
                    />
                </View>
                {selectCountryKey.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    emailInput = () => {
        const { email, emailUpdt, availabilityEmail } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.email}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { [this.setState({ email: val, emailUpdt: val }), this.checkEmail(val)] }}
                        keyboardType={'email-address'}
                        placeholder={Strings.email}
                        placeholderTextColor={colors.lightGray}
                        value={email} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {emailUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
                {email.length > 0 && availabilityEmail == false &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.emailNotAvailable}</Text>

                }
            </Animatable.View>
        )
    }

    phoneInput = () => {
        const { phone, phoneUpdt, availabilityPhone } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.phone}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { [this.setState({ phone: val, phoneUpdt: val }), this.checkPhone(val)] }}
                        keyboardType={'phone-pad'}
                        placeholder={Strings.phone}
                        placeholderTextColor={colors.lightGray}
                        value={phone} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {phoneUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
                {phone.length > 0 && availabilityPhone == false &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.phoneNotAvailable}</Text>

                }
            </Animatable.View>
        )
    }

    passwordInput = () => {
        const { isRTL } = this.props
        const { password, hidePassword, passwordUpdt, passwordMinLength } = this.state
        return (
            <Animatable.View animation="slideInLeft" duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.password}</Text>*/}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderRadius: moderateScale(0), borderBottomWidth: 1, borderBottomColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                        secureTextEntry={hidePassword}
                        onChangeText={(val) => { this.setState({ password: val, passwordUpdt: val, passwordMinLength: false }) }}
                        placeholder={Strings.password}
                        placeholderTextColor={colors.lightGray}
                        value={password} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(79), marginHorizontal: moderateScale(0), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                    <TouchableOpacity

                        onPress={() => { this.setState({ hidePassword: !hidePassword }) }}
                    >
                        <Icon style={{ fontSize: responsiveFontSize(6), color: '#D6D6D6' }} name={hidePassword ? 'eye-off' : 'eye'} type='Feather' />
                    </TouchableOpacity>
                </View>
                {passwordUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
                {passwordMinLength &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.atLeast6Char}</Text>
                }
            </Animatable.View>
        )
    }


    confirmPasswordInput = () => {
        const { isRTL } = this.props
        const { confirmPassword, hideConfirmPassword, confirmPasswordUpdt, confirmPasswordValidate } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginVertical: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.confirmPassword}</Text>*/}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderRadius: moderateScale(0), borderBottomWidth: 1, borderBottomColor: '#cccbcb' }}>
                    <TextInput
                        secureTextEntry={hideConfirmPassword}
                        onChangeText={(val) => { this.setState({ confirmPassword: val, confirmPasswordUpdt: val, confirmPasswordValidate: '' }) }}
                        placeholder={Strings.confirmPassword}
                        placeholderTextColor={colors.lightGray}
                        value={confirmPassword} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(79), marginHorizontal: moderateScale(0), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                    <TouchableOpacity

                        onPress={() => { this.setState({ hideConfirmPassword: !hideConfirmPassword }) }}
                    >
                        <Icon style={{ fontSize: responsiveFontSize(6), color: '#D6D6D6' }} name={hideConfirmPassword ? 'eye-off' : 'eye'} type='Feather' />
                    </TouchableOpacity>
                </View>
                {confirmPasswordUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
                {confirmPasswordValidate.length > 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {confirmPasswordValidate}</Text>
                }
            </Animatable.View>
        )
    }

    imageInput = () => {
        const { isRTL } = this.props
        const { image, imageUpdt } = this.state
        return (
            <View style={{ marginTop: moderateScale(10), width: responsiveWidth(85), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}>

                <Text style={{ marginHorizontal: moderateScale(2), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' }}> {Strings.selectProfileImage}</Text>
                <View style={{ marginTop: moderateScale(10), marginHorizontal: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                    <Button
                        onPress={() => {
                            ImagePicker.openPicker({
                                width: 300,
                                height: 400,
                                cropping: true
                            }).then(image => {

                                this.setState({ image: image.path, imageUpdt: image.path })
                            });
                        }}
                        style={{ backgroundColor: colors.white, justifyContent: 'center', alignItems: 'center', width: responsiveWidth(25), height: responsiveWidth(25) }} >
                        <Icon name='plus' type='AntDesign' style={{ color: colors.black, fontSize: responsiveFontSize(15) }} />
                    </Button>
                    {image &&
                        <FastImage
                            source={{ uri: image }}
                            style={{ borderRadius: moderateScale(3), marginHorizontal: moderateScale(10), width: responsiveWidth(30), height: responsiveWidth(30) }}
                        />
                    }
                    {imageUpdt.length == 0 &&
                        <></>
                        /*<Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                    }
                </View>
            </View>
        )
    }

    descriptionInpu = () => {
        const { isRTL } = this.props
        const { description, descriptionUpdt } = this.setState
        return (
            <View style={{ alignSelf: 'center', width: responsiveWidth(90) }} >
                <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, color: colors.darkBlue }}>{Strings.description}({Strings.optional})</Text>
                <TextInput
                    placeholder={Strings.description}
                    placeholderTextColor={colors.lightGray}
                    multiline
                    onChangeText={(val) => { this.setState({ description: val, descriptionUpdt: val }) }}
                    value={description}
                    style={{ padding: moderateScale(5), textAlignVertical: 'top', marginTop: moderateScale(3), width: responsiveWidth(90), height: responsiveHeight(20), borderRadius: moderateScale(5), borderColor: colors.lightGray, borderWidth: 1 }}
                />
            </View>
        )
    }

    logisticsTypeRadioButtons = () => {
        const { isRTL } = this.props
        const { logisticsCharitableType, logisticsDeliveryType, logisticsMapType } = this.state
        return (
            <View style={{ marginTop: moderateScale(10), marginHorizontal: moderateScale(3) }} >

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                    <RadioButton
                        innerCircleColor={colors.orange}
                        currentValue={this.state.logisticsCharitableType}
                        value={1}
                        onPress={(value) => { this.state.logisticsCharitableType == 0 ? this.setState({ logisticsCharitableType: 1 }) : this.setState({ logisticsCharitableType: 0 }) }}>
                    </RadioButton>
                    <Text style={{ marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.charitableOffers}</Text>
                </View>


                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', marginTop: moderateScale(4) }}>
                    <RadioButton
                        innerCircleColor={colors.orange}
                        currentValue={this.state.logisticsDeliveryType}
                        value={1}
                        onPress={(value) => { this.state.logisticsDeliveryType == 0 ? this.setState({ logisticsDeliveryType: 1 }) : this.setState({ logisticsDeliveryType: 0 }) }}>
                    </RadioButton>
                    <Text style={{ marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.deliveryServices}</Text>
                </View>


                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', marginTop: moderateScale(4) }}>
                    <RadioButton
                        innerCircleColor={colors.orange}
                        currentValue={this.state.logisticsMapType}
                        value={1}
                        onPress={(value) => { this.state.logisticsMapType == 0 ? this.setState({ logisticsMapType: 1 }) : this.setState({ logisticsMapType: 0 }) }}>
                    </RadioButton>
                    <Text style={{ marginTop: moderateScale(1), marginHorizontal: moderateScale(2), fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.locationOnMap}</Text>
                </View>
            </View>
        )
    }


    maroofAccountSwitch = () => {
        const { isRTL } = this.props
        const { maroofAccountSwitch } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(85), justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center', marginTop: moderateScale(7) }}>
                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.areYouHaveAccountInMaroof}</Text>
                <Switch
                    onValueChange={(val) => this.setState({ maroofAccountSwitch: !maroofAccountSwitch })}
                    value={maroofAccountSwitch}
                    trackColor={{ true: colors.darkBlue, false: 'grey' }} />
            </View>
        )
    }

    freelanceAccountSwitch = () => {
        const { isRTL } = this.props
        const { freelanceAccountSwitch } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(85), justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center', marginTop: moderateScale(7) }}>
                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.areYouHaveAccountInFreelance}</Text>
                <Switch
                    onValueChange={(val) => this.setState({ freelanceAccountSwitch: !freelanceAccountSwitch })}
                    value={freelanceAccountSwitch}
                    trackColor={{ true: colors.darkBlue, false: 'grey' }} />
            </View>
        )
    }




    employeeNumbersInput = () => {
        const { employeeNumbers, employeeNumbersUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.employeeNumbers}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ employeeNumbers: val, employeeNumbersUpdt: val }) }}
                        placeholder={Strings.employeeNumbers}
                        placeholderTextColor={colors.lightGray}
                        value={employeeNumbers} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {employeeNumbersUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }


    creationYearInput = () => {
        const { creationYear, creationYearUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.creationYear}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ creationYear: val, creationYearUpdt: val }) }}
                        placeholder={Strings.creationYear}
                        placeholderTextColor={colors.lightGray}
                        value={creationYear} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {/*creationYearUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                }
            </Animatable.View>
        )
    }


    commercialRegisterInput = () => {
        const { commercialRegister, commercialRegisterUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.commercialRegister}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ commercialRegister: val, commercialRegisterUpdt: val }) }}
                        placeholder={Strings.commercialRegister + ' ( ' + Strings.optional + ' )'}
                        placeholderTextColor={colors.lightGray}
                        value={commercialRegister} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {/*commercialRegisterUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                }
            </Animatable.View>
        )
    }



    primarEducationInput = () => {
        const { primarEducation, primarEducationUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.primarEducation}</Text>
                    <TextInput
                        onChangeText={(val) => { this.setState({ primarEducation: val, primarEducationUpdt: val }) }}
                        placeholder={Strings.primarEducation}
                        placeholderTextColor={colors.lightGray}
                        value={primarEducation} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {/*primarEducationUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                }
            </Animatable.View>
        )
    }



    intermediatEducationInput = () => {
        const { intermediatEducation } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.intermediatEducation}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ intermediatEducation: val }) }}
                        placeholder={Strings.intermediatEducation}
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {/*intermediatEducation.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                }
            </Animatable.View>
        )
    }

    universitdEucationInput = () => {
        const { universitdEucation } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.universitdEucation}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ universitdEucation: val }) }}
                        placeholder={Strings.universitdEucation}
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {/*universitdEucation.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                }
            </Animatable.View>
        )
    }



    country_city_areas_pickerInputs = () => {
        const { countries, selectCountry, cities, selectCity, selectRegion, area, selectArea, selectRegionUpdt,selectCityID, selectCityUpdt } = this.state
        const { isRTL } = this.props
        return (
            <View>
                {/*countries */}
                <Animatable.View animation={"slideInLeft"} duration={1000} style={{ marginTop: moderateScale(9), width: responsiveWidth(85), alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: '#cccbcb', }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(5), }}>{Strings.selectCountry}</Text>*/}
                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', color: 'white' },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { height: responsiveHeight(70), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(10), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={this.state.countries}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        selectText={this.state.selectCountry}
                        readOnlyHeadings={true}
                        expandDropDowns
                        showDropDowns={false}
                        modalWithTouchable
                        hideSearch
                        confirmText={strings.close}
                        searchPlaceholderText={Strings.search}
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }
                        onSelectedItemObjectsChange={(selectedItems) => {
                            //console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectCountry: selectedItems[0].name, selectCountryID: selectedItems[0].id });

                            this.getCities(selectedItems[0].id)
                        }
                        }
                    />

                </Animatable.View>

                {/*city */}
                <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(9), width: responsiveWidth(85), alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: '#cccbcb', }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(5), }}>{Strings.selectCity}</Text>*/}

                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center' },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { height: responsiveHeight(70), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(10), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={this.state.cities}
                        alwaysShowSelectText
                        single
                        noItemsComponent={<Text style={{ textAlign: 'center', marginTop: moderateScale(20), fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{strings.mustChooseCountryFirst}</Text>}
                        uniqueKey="id"
                        subKey="children"
                        selectText={this.state.selectCity ? this.state.selectCity : Strings.selectCity}
                        readOnlyHeadings={true}
                        expandDropDowns
                        showDropDowns={false}
                        hideSearch
                        modalWithTouchable
                        confirmText={strings.close}
                        searchPlaceholderText={Strings.search}
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }
                        onSelectedItemObjectsChange={(selectedItems) => {
                            //console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectCity: selectedItems[0].name, selectCityID: selectedItems[0].id , selectCityUpdt:selectedItems[0].id});
                            this.getRegions(selectedItems[0].id);
                        }
                        }
                    />
                    {selectCityUpdt.length ==0  &&
                        <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }

                </Animatable.View>


                {/*areas */}
                {/*<Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginVertical: moderateScale(9), width: responsiveWidth(85), alignSelf: 'center' }}>

                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(5), }}>{Strings.selectArea}</Text>*}
                    <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                        <SectionedMultiSelect
                            styles={{
                                selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center' },
                                selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' },
                                subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                                container: { height: responsiveHeight(70), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(10), alignSelf: 'center' },
                                button:{backgroundColor:colors.blueTabs}
                            }}
                            items={this.state.regions}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectRegion ? this.state.selectRegion : Strings.selectRegion}
                            readOnlyHeadings={true}
                            expandDropDowns
                            showDropDowns={false}
                            modalWithTouchable
                            confirmText={strings.close}
                            searchPlaceholderText={Strings.search}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                //console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selectRegion: selectedItems[0].name, selectRegionID: selectedItems[0].id, selectRegionUpdt: selectedItems[0].name });
                            }
                            }
                        />
                    </View>
                    {selectRegionUpdt.length == 0 &&
                        <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>*/}

            </View>
        )
    }


    mainCategory_subCategory_pickerInputs = () => {
        const { mainCategories, selectSubCategory, selectAdsCategoryUpdt, selectSubCategoryUpdt, selectSubCategoriesIds } = this.state
        const { isRTL } = this.props
        return (
            <View>
                {/*Ads Categories*/}

                <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(9), width: responsiveWidth(85), alignSelf: 'center' }}>

                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.adsCategory}</Text>*/}
                    <View style={{ backgroundColor: 'white', marginTop: moderateScale(0), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                        <SectionedMultiSelect
                            styles={{
                                selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                                button: { backgroundColor: colors.blueTabs }
                            }}
                            items={this.state.ads}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectAdsCategory ? this.state.selectAdsCategory : Strings.adsCategory}
                            readOnlyHeadings={true}
                            expandDropDowns
                            showDropDowns={false}
                            modalWithTouchable
                            confirmText={strings.close}
                            searchPlaceholderText={Strings.search}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                //console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selectAdsCategory: selectedItems[0].name, selectAdsCategoryId: selectedItems[0].id, selectAdsCategoryUpdt: selectedItems[0].name });

                            }
                            }
                        />
                    </View>
                    {selectAdsCategoryUpdt.length == 0 &&
                        <></>
                        /*<Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                    }

                </Animatable.View>
                {/*mainCategories */}
                <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(9), width: responsiveWidth(85), alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: '#cccbcb', }}>

                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont,  }}>{Strings.mainCategory}</Text>*/}

                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={this.state.categories}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        selectText={this.state.selectMainCategory ? this.state.selectMainCategory : Strings.mainCategory}
                        readOnlyHeadings={true}
                        expandDropDowns
                        showDropDowns={false}
                        modalWithTouchable
                        confirmText={strings.close}
                        searchPlaceholderText={Strings.search}
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }
                        onSelectedItemObjectsChange={(selectedItems) => {
                            //console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectMainCategory: selectedItems[0].name, selectCategoryId: selectedItems[0].id });
                            this.getSubCategories(selectedItems[0].id)
                        }
                        }
                    />

                </Animatable.View>

                {/*Sub categories */}
                {/*<Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginVertical: moderateScale(9), width: responsiveWidth(85), alignSelf: 'center' }}>

                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, }}>{Strings.subCategory}</Text>*}
                    <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>

                        <SectionedMultiSelect
                            styles={{
                                selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center' },
                                selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                                button: { backgroundColor: colors.blueTabs }
                            }}
                            items={this.state.subCategories}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectSubCategory ? this.state.selectSubCategory : Strings.subCategory}
                            readOnlyHeadings={true}
                            expandDropDowns
                            showDropDowns={false}
                            modalWithTouchable
                            confirmText={strings.close}
                            searchPlaceholderText={Strings.search}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                //console.log(" SubCATTTT  ", selectedItems[0].name)
                                var subCategoriesIds = selectSubCategoriesIds
                                subCategoriesIds.push(selectedItems[0])
                                this.setState({ selectSubCategory: selectedItems[0].name, selectSubCategoryId: selectedItems[0].id, selectSubCategoryUpdt: selectedItems[0].name, selectSubCategoriesIds: subCategoriesIds });

                            }
                            }
                        />
                    </View>
                    {selectSubCategoryUpdt.length == 0 &&
                        <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>*/}


                <View style={{ width: responsiveWidth(90), flexDirection: isRTL ? 'row-reverse' : 'row', flexWrap: 'wrap', alignItems: 'center', alignSelf: 'center', marginVertical: moderateScale(5), paddingHorizontal: moderateScale(1) }}>
                    {selectSubCategoriesIds.map((val, elem) => (
                        <View>

                            <View style={{ width: responsiveWidth(28.5), height: responsiveHeight(6), marginHorizontal: moderateScale(0.5), justifyContent: 'center', marginVertical: moderateScale(1), alignItems: 'center', borderRadius: moderateScale(2), backgroundColor: colors.darkBlue }}>
                                <Text style={{ paddingHorizontal: moderateScale(1), paddingVertical: moderateScale(0), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center', fontSize: responsiveFontSize(5) }}>{val.name}</Text>
                            </View>

                            <TouchableOpacity onPress={() => this.removeElement(selectSubCategoriesIds, elem)}
                                style={{ position: 'absolute', top: moderateScale(-3), backgroundColor: 'white', borderRadius: moderateScale(3) }}>
                                <Icon name='delete' type='AntDesign' style={{ fontSize: responsiveFontSize(7), color: 'red' }} />
                            </TouchableOpacity>
                        </View>

                    ))}

                </View>


            </View>
        )
    }


    removeElement(array, index) {

        if (index > -1) {
            array.splice(index, 1);
        }
        this.setState({ selectSubCategoriesIds: array })
    }

    map = () => {
        const { isRTL } = this.props
        const { location } = this.state
        return (
            <View>
                <Text style={{ color: colors.darkBlue, alignSelf: isRTL ? 'flex-end' : 'flex-start', marginTop: moderateScale(10), marginHorizontal: moderateScale(7), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.selectYouLocation}</Text>
                <MapView
                    onPress={(coordinate) => {
                        //Alert.alert(coordinate.nativeEvent.coordinate.latitude+"")
                        //console.log(coordinate.nativeEvent.coordinate.latitude)
                        this.setState({
                            location: {
                                latitude: coordinate.nativeEvent.coordinate.latitude,
                                longitude: coordinate.nativeEvent.coordinate.longitude,
                                latitudeDelta: 0.015,
                                longitudeDelta: 0.0121,
                            }
                        })
                    }}
                    style={{ alignSelf: 'center', width: responsiveWidth(100), height: responsiveHeight(55), marginVertical: moderateScale(5) }}
                    region={location}
                >
                    <Marker
                        coordinate={location}
                        image={require('../assets/imgs/marker.png')}
                    />
                </MapView>
            </View>
        )
    }

    pagesIndicator = () => {
        const { isRTL } = this.props
        return (
            <View style={{ width: responsiveWidth(80), alignSelf: 'center', marginVertical: moderateScale(10), }}>
                <StepIndicator
                    onPress={(page) => {
                        //this.setState({ currentPage: page })
                    }}
                    stepCount={4}
                    customStyles={customStyles}
                    currentPosition={this.state.currentPage}
                //labels={labels}
                />
            </View>
        )
    }

    page1 = () => {
        const { isRTL } = this.props
        const { providerType } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ width: responsiveWidth(100) }} >

                <View style={{ alignSelf: 'center', marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-around', width: responsiveWidth(60) }}>

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center' }}>
                        {/* <View style={{ marginBottom: moderateScale(2) }}>
                            <Icon name='university' type='FontAwesome' style={{ fontSize: responsiveFontSize(10) }} />
                         </View> */}
                        <RadioButton
                            innerCircleColor='orange'
                            currentValue={this.state.providerType}
                            value='company'
                            onPress={(value) => { this.setState({ providerType: value }) }} >
                        </RadioButton>
                        <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2) }}>{Strings.firm}</Text>

                    </View>

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center' }}>
                        {/*<View style={{ marginBottom: moderateScale(2) }}>
                            <Icon name='user-alt' type='FontAwesome5' style={{ fontSize: responsiveFontSize(10) }} />
                        </View>*/}
                        <RadioButton
                            innerCircleColor='orange'
                            currentValue={this.state.providerType}
                            value='individual'
                            onPress={(value) => { this.setState({ providerType: value }) }} >
                        </RadioButton>
                        <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2) }}>{Strings.individual}</Text>

                    </View>

                </View>


                {providerType == 'company' &&
                    <View style={{ borderBottomColor: colors.lightGray, borderBottomWidth: 2, marginHorizontal: moderateScale(5), marginTop: moderateScale(12) }}>
                        <Text style={{ paddingBottom: moderateScale(5), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontSize: responsiveFontSize(6), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.selectCompanyType}</Text>
                        <View style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', width: responsiveWidth(40), height: 2, backgroundColor: 'green' }} />
                    </View>
                }

                {providerType == 'company' &&
                    <View style={{ marginTop: moderateScale(10), width: responsiveWidth(70), alignSelf: 'center' }}>
                        <Text style={{ alignSelf: isRTL ? 'flex-start' : 'flex-end', color: 'orange', fontFamily: isRTL ? arrabicFont : englishFont, }}>{Strings.distinguished}</Text>
                        <View style={{ paddingVertical: moderateScale(2), borderWidth: 1, borderColor: colors.lightGray, borderRadius: moderateScale(3), width: responsiveWidth(70), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-around' }}>

                            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(20), alignItems: 'center' }} >
                                <RadioButton
                                    innerCircleColor='orange'
                                    currentValue={this.state.companyType}
                                    value={'MNC'}
                                    onPress={(value) => { this.setState({ companyType: value }) }} >
                                </RadioButton>
                                <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2) }}>{Strings.holding}</Text>
                            </View>

                            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(20), alignItems: 'center' }} >
                                <RadioButton innerCircleColor='orange'
                                    currentValue={this.state.companyType}
                                    value={'LTD'}
                                    onPress={(value) => { this.setState({ companyType: value }) }} >
                                </RadioButton>
                                <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2) }}>{Strings.input}</Text>
                            </View>
                        </View>
                    </View>
                }

                {providerType == 'company' &&
                    <View style={{ marginTop: moderateScale(5), width: responsiveWidth(70), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-around' }}>
                        <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(20), alignItems: 'center' }} >
                            <RadioButton
                                innerCircleColor='orange'
                                currentValue={this.state.companyType}
                                value={'PVT'}
                                onPress={(value) => { this.setState({ companyType: value }) }} >
                            </RadioButton>
                            <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2) }} >{Strings.limited}</Text>
                        </View>

                        <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(20), alignItems: 'center' }} >
                            <RadioButton innerCircleColor='orange'
                                currentValue={this.state.companyType}
                                value={'Registered'}
                                onPress={(value) => { this.setState({ companyType: value }) }} >
                            </RadioButton>
                            <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2) }}>{Strings.starch}</Text>

                        </View>
                    </View>
                }


                <Button onPress={() => {
                    this.setState({ currentPage: 1 })
                }} style={{ padding: 0, marginVertical: moderateScale(20), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.next}</Text>
                </Button>

            </Animatable.View>
        )
    }

    page2 = () => {
        const { isRTL } = this.props
        const { type, name, address, selectRegionUpdt, selectCityID } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ width: responsiveWidth(100), }} >

                <View style={{ borderWidth: 1, width: responsiveWidth(90), borderRadius: moderateScale(5), borderColor: colors.lightGray, alignSelf: 'center', marginTop: moderateScale(10) }}>
                    {this.nameInput()}
                    {this.addressInput()}
                    {this.country_city_areas_pickerInputs()}
                </View>

                <View style={{ marginBottom: moderateScale(15), width: responsiveWidth(85), justifyContent: 'space-between', flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center' }}>

                    <Button onPress={() => {
                        this.setState({ currentPage: 0 })
                    }} style={{ padding: 0, marginTop: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: 'gray' }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.previous}</Text>
                    </Button>

                    <Button onPress={() => {
                        if (!name.replace(/\s/g, '').length) { this.setState({ nameUpdt: '' }) }
                        if (!address.replace(/\s/g, '').length) { this.setState({ addressUpdt: '' }) }
                        if (!(selectCityID.toString()).replace(/\s/g, '').length) { this.setState({ selectCityUpdt: '' }) }
                        if (name.replace(/\s/g, '').length && address.replace(/\s/g, '').length && (selectCityID.toString()).replace(/\s/g, '').length) {
                            this.setState({ currentPage: 2 })
                        }

                    }} style={{ padding: 0, marginTop: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.next}</Text>
                    </Button>
                </View>


            </Animatable.View>
        )
    }


    page3 = () => {
        const { isRTL } = this.props
        const { type, email, phone, image, password, confirmPassword, availabilityPhone, availabilityEmail } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ width: responsiveWidth(100), }} >

                <View style={{ width: responsiveWidth(90), alignSelf: 'center', borderWidth: 1, borderColor: colors.lightGray, borderRadius: moderateScale(5), marginTop: moderateScale(0) }}>
                    {this.countryKeyInput()}
                    {this.phoneInput()}
                    {this.emailInput()}

                    {this.imageInput()}
                    {this.passwordInput()}
                    {/*this.confirmPasswordInput()*/}
                </View>

                <View style={{ marginBottom: moderateScale(15), width: responsiveWidth(85), justifyContent: 'space-between', flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center' }}>

                    <Button onPress={() => {
                        this.setState({ currentPage: 1 })
                    }} style={{ padding: 0, marginTop: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: 'gray' }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.previous}</Text>
                    </Button>

                    <Button onPress={() => {
                        if (!email.replace(/\s/g, '').length) { this.setState({ emailUpdt: '' }) }
                        if (InputValidations.validateEmail(email) == false) { RNToasty.Error({ title: Strings.emailNotValid }) }
                        if (!phone.replace(/\s/g, '').length) { this.setState({ phoneUpdt: '' }) }



                        if (!password.replace(/\s/g, '').length) { this.setState({ passwordUpdt: '' }) }
                        if (password.replace(/\s/g, '').length < 6) { this.setState({ passwordMinLength: true }) }
                        //if (!confirmPassword.replace(/\s/g, '').length) { this.setState({ confirmPasswordUpdt: '' }) }
                        //if (confirmPassword != password) { this.setState({ confirmPasswordValidate: Strings.dosntMatchWithPassword }) }
                        if (email.replace(/\s/g, '').length && InputValidations.validateEmail(email) == true && phone.replace(/\s/g, '').length && password.replace(/\s/g, '').length && password.replace(/\s/g, '').length >= 6 && availabilityPhone == true && availabilityEmail == true) {
                            this.setState({ currentPage: 3 })
                        }

                    }} style={{ padding: 0, marginTop: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.next}</Text>
                    </Button>
                </View>

            </Animatable.View>
        )
    }

    page4 = () => {
        const { isRTL } = this.props
        const { employeeNumbers, selectAdsCategory, selectSubCategory, selectSubCategoriesIds } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1000} style={{ width: responsiveWidth(100), }}>
                {this.descriptionInpu()}
                <View style={{ padding: moderateScale(4), borderRadius: moderateScale(10), marginTop: moderateScale(7), marginHorizontal: moderateScale(5), alignSelf: isRTL ? 'flex-end' : 'flex-start' }} >
                    <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, color: colors.darkBlue }}>{Strings.additionalLogisticsServices}</Text>
                </View>
                <View style={{ alignSelf: 'center', paddingVertical: moderateScale(3), width: responsiveWidth(90), borderRadius: moderateScale(5), borderWidth: 1, borderColor: colors.lightGray }}>
                    {this.logisticsTypeRadioButtons()}
                    {/*this.employeeNumbersInput()*/}
                    {/*this.creationYearInput()*/}
                    {this.commercialRegisterInput()}
                    {this.maroofAccountSwitch()}
                    {this.freelanceAccountSwitch()}
                    <View style={{ borderRadius: moderateScale(10), marginTop: moderateScale(5), marginHorizontal: moderateScale(5), alignSelf: isRTL ? 'flex-end' : 'flex-start' }} >
                        <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, color: colors.darkBlue, fontSize: responsiveFontSize(6), textDecorationLine: 'underline' }}>{Strings.chooseAppropriateService}</Text>
                    </View>
                    {this.mainCategory_subCategory_pickerInputs()}

                    {/*this.intermediatEducationInput()*/}
                    {/*this.primarEducationInput()*/}
                    {/*this.universitdEucationInput()*/}

                </View>
                {this.map()}
                {this.state.signupLoading &&
                    <LoadingDialogOverlay title={Strings.wait} />
                }
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center', alignItems: 'center' }}>


                    <Button onPress={() => {
                        this.setState({ currentPage: 2 })
                    }} style={{ padding: 0, marginHorizontal: moderateScale(5), marginVertical: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: 'white' }} >
                        <Text style={{ color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.previous}</Text>
                    </Button>

                    <Button onPress={() => {

                        //this.props.getAdsCategories()
                        if (!employeeNumbers.replace(/\s/g, '').length) { this.setState({ employeeNumbersUpdt: '' }) }
                        if (!selectAdsCategory.replace(/\s/g, '').length) { this.setState({ selectAdsCategoryUpdt: '' }) }
                        //if (!selectSubCategory.replace(/\s/g, '').length) { this.setState({ selectSubCategoryUpdt: '' }) }
                        if (selectSubCategoriesIds.length <= 0) { RNToasty.Error({ title: Strings.mustChooseSubCategoryatLeast }) }


                        if (selectSubCategoriesIds.length > 0) {
                            this.signup()
                        }

                        //this.setState({currentPage:3})
                    }} style={{ padding: 0, marginHorizontal: moderateScale(5), marginVertical: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.save}</Text>
                    </Button>
                </View>
            </Animatable.View>
        )
    }


    page5 = () => {
        const { isRTL } = this.props
        const { loading, isFullcode, verifactionCode, verifactionCodeUpdt } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1000} style={{ width: responsiveWidth(100), }}>
                <View style={{ alignSelf: 'center', width: responsiveWidth(90), marginTop: moderateScale(10) }}>
                    <Text style={{ alignSelf: 'center', textAlign: isRTL ? 'right' : 'left', color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(5.5) }} >{Strings.kindlyVerifyYourAccount}</Text>
                    <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, marginTop: moderateScale(6) }} >{Strings.verifactionCode} *</Text>

                    <TextInput
                        onChangeText={(val) => { this.setState({ verifactionCode: val, verifactionCodeUpdt: val }) }}
                        placeholder={Strings.enterVerifactionCode}
                        style={{ paddingHorizontal: moderateScale(2), alignSelf: 'center', marginTop: moderateScale(5), width: responsiveWidth(88), height: responsiveHeight(8), borderRadius: moderateScale(3), borderWidth: 1, borderColor: colors.lightGray, textAlign: isRTL ? 'right' : 'left' }} />
                    <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, marginTop: moderateScale(3), marginHorizontal: responsiveWidth(2) }} >{Strings.verificationPhoneNumber}</Text>
                    {verifactionCodeUpdt.length == 0 &&
                        <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </View>


                <Button onPress={() => {
                    if (!verifactionCode.replace(/\s/g, '').length) {
                        this.setState({ verifactionCodeUpdt: '' })
                    }
                    if (verifactionCode.replace(/\s/g, '').length) {
                        this.verify()
                    }
                }}
                    style={{ marginTop: moderateScale(15), alignSelf: 'center', width: responsiveWidth(40), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.greenApp, borderRadius: moderateScale(1) }} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.verify} </Text>
                </Button>



                {this.memberSection()}
                {loading && <LoadingDialogOverlay title={Strings.wait} />}

            </Animatable.View>
        )
    }

    memberSection = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginBottom: moderateScale(17), marginTop: moderateScale(15), alignSelf: 'center' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ height: 2, width: responsiveWidth(45), backgroundColor: '#cccbcb' }} />
                    <Text style={{ marginHorizontal: moderateScale(1), color: colors.lightGray, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.ash3ariMember}</Text>
                    <View style={{ height: 2, width: responsiveWidth(45), backgroundColor: '#cccbcb' }} />
                </View>
                <Button onPress={() => { push('Login') }} style={{ alignSelf: 'center', marginTop: moderateScale(12), height: responsiveHeight(7), width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(1) }} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.login2}</Text>
                </Button>
                {this.state.currentPage == 4 &&
                    <Button onPress={() => { this.resendCode() }} style={{ alignSelf: 'center', marginTop: moderateScale(6), height: responsiveHeight(7), width: responsiveWidth(50), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(1) }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.notReciveVerifyCode}</Text>
                    </Button>
                }
            </View>
        )
    }

    content = () => {
        const { isRTL, barColor } = this.props
        const { currentPage } = this.state
        return (
            <View>
                {this.pagesIndicator()}

                {currentPage == 0 && this.page1()}
                {currentPage == 1 && this.page2()}
                {currentPage == 2 && this.page3()}
                {currentPage == 3 && this.page4()}
                {currentPage == 4 && this.page5()}

            </View>
        )
    }


    render() {
        const { isRTL, userToken } = this.props;

        return (
            /* <ReactNativeParallaxHeader
                 scrollViewProps={{ showsVerticalScrollIndicator: false }}
                 headerMinHeight={responsiveHeight(10)}
                 headerMaxHeight={responsiveHeight(35)}
                 extraScrollHeight={20}
                 navbarColor={colors.darkBlue}
                 backgroundImage={require('../assets/imgs/header.png')}
                 backgroundImageScale={1.2}
                 renderNavBar={() => <CollaspeAppHeader back title={Strings.signupAsServiceProvider} />}
                 renderContent={() => this.content()}
                 containerStyle={{ flex: 1 }}
                 contentContainerStyle={{ flexGrow: 1 }}
                 innerContainerStyle={{ flex: 1, }}
             />*/

            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={Strings.signupAsServiceProvider} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
                <AppSuccessDialog
                    open={this.state.verifySuccess}
                    message={Strings.successfullyRegistered}
                    onPress={() => this.setState({ verifySuccess: false })} />
            </ParallaxScrollView>
        );
    }
}

const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: 'green',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: 'green',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: 'green',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: 'green',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: 'green',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#fe7013',
}

const mapDispatchToProps = {
    login,
    removeItem,
    getAdsCategories,
    setUser
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(SignupServiceProvider);

