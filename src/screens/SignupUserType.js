import React, { Component } from 'react';
import {
  View,TouchableOpacity,Image,Text,ScrollView,TextInput,Alert,Platform
} from 'react-native';
import { connect } from 'react-redux';
import {Icon,Button} from 'native-base';
import {login} from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import {enableSideMenu,resetTo,push} from '../controlls/NavigationControll'
import AnimateLoadingButton from 'react-native-animate-loading-button';
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader';
import {removeItem} from '../actions/MenuActions';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import PageTitleLine from '../components/PageTitleLine'
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import FastImage from 'react-native-fast-image'


class SignupUserType extends Component {

    state={
        phone: ' ',
        email: ' ',
        password: ' ',
        hidePassword: true,
    }

    componentDidMount(){
        enableSideMenu(false,null)
    }

    otherMemberSection = () => {
        const {isRTL} = this.props
        return(
            <View style={{marginTop:moderateScale(15),alignSelf:'center'}}>
                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                    <View style={{height:2,width:responsiveWidth(45),backgroundColor:'#cccbcb' }}  />
                    <Text style={{marginHorizontal:moderateScale(1), color:colors.lightGray, fontFamily:isRTL?arrabicFont:englishFont}} >{Strings.ash3ariMember}</Text>
                    <View style={{height:2,width:responsiveWidth(45),backgroundColor:'#cccbcb' }}  />
                </View>
                <Button onPress={()=>{push('Login')}} style={{alignSelf:'center', marginTop:moderateScale(12), height:responsiveHeight(7),width:responsiveWidth(30),justifyContent:'center',alignItems:'center',backgroundColor:colors.darkBlue,borderRadius:moderateScale(1)}} >
                    <Text style={{ color:colors.white,fontFamily:isRTL?arrabicFont:englishFont}}>{Strings.login}</Text>
                </Button>
            </View>
        )
    }


    memberSection = () => {
        const {isRTL} = this.props
        return(
            <View style={{marginTop:moderateScale(15),alignSelf:'center'}}>
                <View style={{alignSelf:'center',width:responsiveWidth(85),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <Button onPress={()=>{push('SignupServiceProvider')}} style={{alignSelf:'center', marginTop:moderateScale(12), height:responsiveHeight(9),width:responsiveWidth(40),justifyContent:'center',alignItems:'center',backgroundColor:colors.darkBlue,borderRadius:moderateScale(1),flexDirection:'column'}} >
                        <Icon name='shopping' type='MaterialCommunityIcons' style={{color:colors.white,fontSize:responsiveFontSize(7)}} />
                        <Text style={{ color:colors.white,fontFamily:isRTL?arrabicFont:englishFont}}>{Strings.serviceProvider}</Text>
                    </Button>
                    <Button onPress={()=>{push('SignupBeneficiaryMember')}} style={{flexDirection:'column',alignSelf:'center', marginTop:moderateScale(10),height:responsiveHeight(9),width:responsiveWidth(40),marginHorizontal:moderateScale(7), justifyContent:'center',alignItems:'center',backgroundColor:'gray',borderRadius:moderateScale(1)}} >
                        <Icon name='user-alt' type='FontAwesome5' style={{color:colors.white,fontSize:responsiveFontSize(7)}} />
                        <Text style={{color:colors.white,fontFamily:isRTL?arrabicFont:englishFont}}>{Strings.beneficiaryMember}</Text>
                    </Button>
                </View>
            </View>
        )
    }
    
    content = () => {
        const {isRTL,barColor} = this.props
         return(
           <View>

            <PageTitleLine title={Strings.signup} iconName='hand-point-up' iconType='FontAwesome5' />
              {this.memberSection()}
              {this.otherMemberSection()}
          </View>
         )
       }

    render(){
        const  { isRTL,userToken } = this.props;
        const  { phone,password,hidePassword,email} = this.state
        return(
           /* <ReactNativeParallaxHeader
            scrollViewProps={{showsVerticalScrollIndicator:false}}
            headerMinHeight={responsiveHeight(10)}
            headerMaxHeight={responsiveHeight(35)}
            extraScrollHeight={20}
            navbarColor={colors.darkBlue}
            backgroundImage={require('../assets/imgs/header.png')}
            backgroundImageScale={1.2}
            renderNavBar={()=><CollaspeAppHeader back title={Strings.signup} />}
            renderContent={()=>this.content()}
            containerStyle={{flex:1}}
            contentContainerStyle={{flexGrow: 1}}
            innerContainerStyle={{flex: 1,}}
          /> */   
          
          
          <ParallaxScrollView
      backgroundColor={colors.darkBlue}
      contentBackgroundColor="white"
      renderFixedHeader={() => <CollaspeAppHeader back title={Strings.signup} />}
      parallaxHeaderHeight={responsiveHeight(35)}
      renderBackground={() => (
          <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

      )}
  >
      {this.content()}
  </ParallaxScrollView>
        );
    }
}

const mapDispatchToProps = {
    login,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps,mapDispatchToProps)(SignupUserType);

