import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, Image, ImageBackground, TouchableOpacity, Text, Alert
} from 'react-native';
import { connect } from 'react-redux';
import { responsiveHeight, responsiveWidth, responsiveFontSize, moderateScale } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { setUser, userToken } from '../actions/AuthActions';
import { changeLanguage, changeColor } from '../actions/LanguageActions';
import { enableSideMenu, push, resetTo } from '../controlls/NavigationControll'
import { selectMenu } from '../actions/MenuActions';
//import firebase,{Notification } from 'react-native-firebase';
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient'
import { Button } from 'native-base'
import { arrabicFont } from '../common/AppFont'
import * as colors from '../assets/colors';
import {
    checkFirbaseNotificationPermision,
    getFirebaseNotificationToken,
    clickOnFirebaseNotification,
    showFirebaseNotifcation
} from '../controlls/FirebasePushNotificationControll'
import { WebView } from 'react-native-webview';
import { checkVersion } from "react-native-check-version";




class SplashScreen extends Component {

    constructor(props) {
        super(props)
        this.checkAppVersion()
        this.state = {
            showCheckVersionDialog: false
        }
    }

    //console.log(VersionCheck)

    checkAppVersion = async () => {

        const version = await checkVersion();
        if (version.needsUpdate) {
            //console.log(`App has a ${version.updateType} update pending.`);

            this.setState({ showCheckVersionDialog: true })
        }

    }

    componentDidMount() {
        enableSideMenu(false, null)
        setTimeout(() => {
            if (this.state.showCheckVersionDialog == true) {
                push('GoStoreForUpdate')
            } else {

                this.checkLogin()
                this.checkLanguage()
            }

        }, 1000 * 4.5)
    }



    checkLogin = async () => {
        const userJSON = await AsyncStorage.getItem('USER');
        if (userJSON) {

            const userInfo = JSON.parse(userJSON);
            this.props.setUser(userInfo);

            //this.checkVerify(userInfo)
            resetTo('Home')
        } else {

            resetTo('Home')
        }

    }

    checkVerify = async (user) => {
        const isVerify = await AsyncStorage.getItem('VERIFY');
        if (isVerify == 'yes') {
            //console.log('IS VERIFY')
            const data = {
                isVerify: true
            }
            if (user.data.role == 'client') {
                push('SignupBeneficiaryMember', data)
            } else {
                push('SignupServiceProvider', data)
            }


        } else {

            resetTo('Home')
        }

    }

    checkLanguage = async () => {

        const lang = await AsyncStorage.getItem('@lang');

        if (lang === 'en') {
            this.props.changeLanguage(false);
            Strings.setLanguage('en')
        } else {
            this.props.changeLanguage(true);
            Strings.setLanguage('ar')
        }

    }
    //source={require('../assets/imgs/splashBackground.png')}

    render() {
        return (
            <FastImage source={require('../assets/imgs/GIF.gif')} style={{ flex: 1, height: responsiveHeight(100), width: responsiveWidth(100) }}>
                {/*<TouchableOpacity
                onPress={()=>{
                    this.checkLogin()
                }}
                style={{width:responsiveWidth(68),height:responsiveHeight(8),alignSelf:'center',position:'absolute',bottom:responsiveHeight(17),}}
                >

            </TouchableOpacity>*/}
            </FastImage>
        );
    }
}

const mapToStateProps = state => ({
    currentUser: state.auth.currentUser,
    userTokens: state.auth.userToken,
})

const mapDispatchToProps = {
    setUser,
    changeLanguage,
    userToken,
    selectMenu,
}

export default connect(mapToStateProps, mapDispatchToProps)(SplashScreen);


