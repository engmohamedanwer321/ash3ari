import React, { Component } from 'react';
import {
    View, TouchableOpacity, ScrollView, FlatList, Text, Platform, AppState,
    Alert, ImageBackground, BackHandler
} from 'react-native';
import { moderateScale, responsiveFontSize, responsiveWidth, responsiveHeight } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import { Icon, Button } from 'native-base';
import strings from '../assets/strings';
import AppHeader from '../common/AppHeader'
import { getUnreadNotificationsNumers } from '../actions/NotificationAction';
import { selectMenu, removeItem } from '../actions/MenuActions';
import { getUnseenMessages } from '../actions/ChatAction'
import { enableSideMenu, push } from '../controlls/NavigationControll'
import * as colors from '../assets/colors'
import { arrabicFont, englishFont } from '../common/AppFont'
import DeapartmentCard from '../components/DeapartmentCard'
import AdsCard from '../components/AdsCard'
import SubCategoryCard from '../components/SubCategoryCard'
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import RNScrollable from 'react-native-scrollable-header';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import MapView, { Marker } from 'react-native-maps';
import PageTitleLine from '../components/PageTitleLine'
import axios from 'axios'
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import NetInfo from "@react-native-community/netinfo";
import { BASE_END_POINT } from '../AppConfig';
import FastImage from 'react-native-fast-image'
import MarketCard from '../components/MarketCard'
import ParallaxScrollView from 'react-native-parallax-scroll-view';



class SubCategories extends Component {

    state = {
        noConnection: null,
        tabNumber: 1,
        departmentNumber: 1,
        countries: [],
        selectCountry: null,
        cities: [],
        selectCity: null,
        departments: [],
        selectDepartmen: null,
        ads: [],
        adsLoad: true,
        ads404: null,
        eventDesigns: [],
        eventDesignsLoad: true,
        subCategories404: null,
        sliders: [],
        categories: []
    };

    componentDidMount() {
     
        enableSideMenu(false, this.props.isRTL)
        this.getSubCategories()
    }

    componentDidUpdate() {
        enableSideMenu(false, this.props.isRTL)
    }

    componentWillUnmount() {
        this.props.removeItem()
    }


    getSubCategories = () => {
        const { data, isRTL } = this.props

        axios.get(BASE_END_POINT + 'sub_categories/' + data.id)
            .then(response => {
           
                this.setState({ subCategories: response.data.data, eventDesignsLoad: false, })
            })
            .catch(error => {
               
                this.setState({ subCategories404: true, eventDesignsLoad: false, })
            })
    }



    departmentsSection = () => {
        const { isRTL } = this.props
        const { departmentNumber, sliders } = this.state
        return (
            <View style={{ width: responsiveWidth(100), marginTop: moderateScale(0) }} >


                {sliders.length > 0 &&
                    <FastImage
                        style={{ width: responsiveWidth(100), height: responsiveHeight(25) }}
                        source={{ uri: sliders[0].name }}
                    />
                }

                <View style={{ marginTop: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', marginHorizontal: moderateScale(5), alignSelf: isRTL ? 'flex-end' : 'flex-start' }} >
                    <TouchableOpacity onPress={() => {
                        if (departmentNumber != 1) {
                            this.setState({ departmentNumber: 1 })
                        }
                    }} style={{ borderBottomColor: departmentNumber == 1 ? colors.green : 'transparent', borderBottomWidth: 2, paddingVertical: moderateScale(3) }}>
                        <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), color: colors.black }} >{strings.mainDepartmens}</Text>
                    </TouchableOpacity>

                    {/*<TouchableOpacity onPress={() => {
            if (departmentNumber != 2) {
              this.setState({ departmentNumber: 2 })
            }
          }} style={{ marginHorizontal: moderateScale(7), borderBottomColor: departmentNumber == 2 ? colors.green : 'transparent', borderBottomWidth: 2, paddingVertical: moderateScale(3) }}>
            <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7), color: colors.black }} >{strings.subDepartmens}</Text>
          </TouchableOpacity>*/}
                </View>

                <FlatList
                    numColumns={2}
                    style={{ marginTop: moderateScale(5), alignSelf: 'center' }}
                    data={this.state.categories}
                    renderItem={({ item }) => <DeapartmentCard data={item} />}
                />
            </View>
        )
    }


    subCategoriesSection = () => {
        const { subCategories, subCategories404, eventDesignsLoad } = this.state
        return (
            <View>
                <PageTitleLine title={strings.subDepartmens} iconName='axis-x-arrow' iconType='MaterialCommunityIcons' />
                {
                    subCategories404 ?
                        <NetworError />
                        :
                        eventDesignsLoad ?
                            <Loading />
                            :
                            subCategories.length > 0 ?
                                <FlatList
                                    style={{ alignSelf: 'center', marginVertical: moderateScale(7) }}
                                    numColumns={2}

                                    data={subCategories}
                                    renderItem={({ item }) => <SubCategoryCard data={item} />}
                                />
                                :
                                <NoData />

                }
            </View>
        )
    }


    content = () => {
        const { tabNumber } = this.state
        return (
            <View>
                {this.subCategoriesSection()}
            </View>
        )
    }


    render() {
        const { tabNumber } = this.state
        return (
            /*<View style={{ flex: 1 }}>
                <ReactNativeParallaxHeader
                    scrollViewProps={{ showsVerticalScrollIndicator: false }}
                    headerMinHeight={responsiveHeight(10)}
                    headerMaxHeight={responsiveHeight(35)}
                    extraScrollHeight={20}
                    navbarColor={colors.darkBlue}
                    backgroundImage={require('../assets/imgs/header.png')}
                    backgroundImageScale={1.2}
                    renderNavBar={() => <CollaspeAppHeader back title={strings.subDepartmens} />}
                    renderContent={() => this.content()}
                    containerStyle={{ flex: 1 }}
                    contentContainerStyle={{ flexGrow: 1 }}
                    innerContainerStyle={{ flex: 1, }}
                />
            </View>*/

<ParallaxScrollView
backgroundColor={colors.darkBlue}
contentBackgroundColor="white"
renderFixedHeader={() => <CollaspeAppHeader back title={strings.subDepartmens} />}
parallaxHeaderHeight={responsiveHeight(35)}
renderBackground={() => (
   <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35),  alignItems: 'center', justifyContent: 'center' }}/>
      
  )}
>
{this.content()}
</ParallaxScrollView>

        )
    }
}


const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    unReaded: state.noti.unReaded,
    unSeen: state.chat.unSeen,
    currentUser: state.auth.currentUser
})

const mapDispatchToProps = {
    selectMenu,
    getUnreadNotificationsNumers,
    removeItem,
    getUnseenMessages,


}

export default connect(mapToStateProps, mapDispatchToProps)(SubCategories);