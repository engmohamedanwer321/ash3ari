import React, { Component } from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {
  View,TextInput,Modal,ScrollView,TouchableOpacity,Text,Alert
} from 'react-native';
import { connect } from 'react-redux';
import {  Icon, Thumbnail,Item,Picker,Label } from "native-base";
import { responsiveWidth, moderateScale,responsiveFontSize,responsiveHeight } from "../utils/responsiveDimensions";
import Strings from  '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import {getUser,signup} from '../actions/AuthActions';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import ImagePicker from 'react-native-image-crop-picker';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {selectMenu,removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import AppHeader from '../common/AppHeader'
import LottieView from 'lottie-react-native';
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment'
import {enableSideMenu,push,resetTo} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from '../common/AppFont'
import AnimateLoadingButton from 'react-native-animate-loading-button';
import * as Animatable from 'react-native-animatable';



class UpdateProfile extends Component {
   
    constructor(props){
        super(props)
        moment.locale('en')
    }

    state = {
       name: this.props.currentUser.user.name,
       email: this.props.currentUser.user.email,
       phone:this.props.currentUser.user.phone,
       img:null,
       password: ' ',
       showImageModal:false,
       date:'',
       showDate:false,
       password: ' ',
       passwordError:Strings.require,
       hidePassword:true,
       loading:false,

    }

        
    componentDidMount(){    
        this.setState({date:this.props.currentUser.user.birth_date})
        enableSideMenu(false,this.props.isRTL)
        
    }


    
    onUpdate = (user) => {
        this.setState({loading:true})
        axios.post(`${BASE_END_POINT}user/register`,user, {
            headers: {
              'Content-Type': 'application/json',
            },
          }).then(response => {
            RNToasty.Success({title:Strings.updateProfileSuccess})
            AsyncStorage.setItem('@QsathaUser', JSON.stringify(response.data));   
           
         
            this.props.getUser(response.data)
            this.setState({loading:false})
            resetTo('Home')          
          })
          .catch(error=>{
            this.setState({loading:false})
         
          })
    }

   
  

    render(){
        const {currentUser,isRTL} = this.props;
        const {hidePassword,passwordError, name, email, phone,password,img,date} = this.state;
       
        return(
            <View style={{ flex:1,backgroundColor:'white' }}>
                <AppHeader title={Strings.updateProfile} />
                <ScrollView >
                <View style={{alignSelf:'center',marginTop:moderateScale(8),justifyContent:"center",alignItems:'center'}}>
                    <FastImage style={{height:150,width:150,borderRadius:75}} source={img?{uri:img}:currentUser.user.avatar?{uri:currentUser.user.avatar}:require('../assets/imgs/profileicon.jpg')} />
                </View>
                <TouchableOpacity
                    activeOpacity={1}
                      onPress={()=>{
                        this.setState({showImageModal:true})
                    }}
                     style={{marginLeft:moderateScale(26), alignSelf:'center', marginTop:moderateScale(-16), backgroundColor:'#679C8A', height:50,width:50,borderRadius:25,justifyContent:'center',alignItems:'center'}} >
                        <Icon name='camera-alt' type='MaterialIcons' style={{color:'white',fontSize:responsiveFontSize(10)}} />
                </TouchableOpacity>

                <Animatable.View animation={isRTL?"slideInRight":"slideInLeft"} duration={1500} style={{marginVertical:moderateScale(3), width:responsiveWidth(90),alignSelf:'center'}}>
                <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.name}</Text>

                    <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center', justifyContent:'center',backgroundColor:'white', marginTop:moderateScale(1),width:responsiveWidth(90), borderBottomWidth:1,borderBottomColor:'#cccbcb',height:responsiveHeight(8)}}>
                        <View style={{marginHorizontal:moderateScale(5),}}>
                        <Icon name='user' type='AntDesign' style={{color:'gray',fontSize:responsiveFontSize(8)}} />
                        </View>
                        <TextInput 
                        value={name}
                        onChangeText={(val)=>{this.setState({name:val})}}
                        placeholder={Strings.enterYourName} underlineColorAndroid='transparent' style={{fontSize:responsiveFontSize(6),width:responsiveWidth(80), fontFamily:isRTL?arrabicFont:englishFont, textAlign:isRTL?'right':'left', width:responsiveWidth(81), marginHorizontal:moderateScale(0), fontFamily:isRTL?arrabicFont:englishFont,width:responsiveWidth(80),fontSize:responsiveFontSize(6), textAlign:isRTL?'right':'left', marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {name.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </Animatable.View>

                <Animatable.View animation={isRTL?"slideInLeft":"slideInRight"} duration={1500} style={{marginTop:moderateScale(10), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.email}</Text>
                    <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center', justifyContent:'center',backgroundColor:'white',  marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderBottomWidth:1,borderBottomColor:'#cccbcb',height:responsiveHeight(8)}}>
                    <View style={{marginHorizontal:moderateScale(5),}}>
                        <Icon name='email-outline' type='MaterialCommunityIcons' style={{color:'gray',fontSize:responsiveFontSize(8)}} />
                    </View>
                        <TextInput
                        value={email}
                        keyboardType='email-address'
                        onChangeText={(val)=>{this.setState({email:val})}}
                        placeholder={Strings.enterYourEmail} underlineColorAndroid='transparent' style={{fontSize:responsiveFontSize(6),width:responsiveWidth(80), fontFamily:isRTL?arrabicFont:englishFont, textAlign:isRTL?'right':'left', width:responsiveWidth(81), marginHorizontal:moderateScale(0), fontFamily:isRTL?arrabicFont:englishFont, textAlign:isRTL?'right':'left', marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {email.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </Animatable.View>

                <Animatable.View animation={isRTL?"slideInRight":"slideInLeft"} duration={1500} style={{marginTop:moderateScale(10), width:responsiveWidth(90),alignSelf:'center'}}>
                <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.birthDate}</Text>
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({showDate:true})
                }}
          
                activeOpacity={1} style={{borderRadius:moderateScale(2), borderBottomWidth:1,borderBottomColor:'#cccbcb',justifyContent:'center', height:responsiveHeight(8), marginTop:moderateScale(3),width:responsiveWidth(90),elevation:0.2,backgroundColor:'white'}}>
                <Text  style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont, fontSize:responsiveFontSize(7), textAlign:isRTL?'right':'left',marginHorizontal:moderateScale(5)}} >{date}</Text>
                </TouchableOpacity>
         
                </Animatable.View>

        <DateTimePicker
            
            mode='date'
            isVisible={this.state.showDate}
            onConfirm={(val)=>{
            //const d = new Date(val).toISOString().substring(0,10);
            //console.log('Date   ',d)
             this.setState({showDate:false,date:moment(val).format("YYYY-MM-DD")})
            }}
            onCancel={ ()=>{ this.setState({showDate:false}) } }
            />

            
            

                <Animatable.View animation={isRTL?"slideInLeft":"slideInRight"} duration={1500} style={{marginTop:moderateScale(10), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.phone}</Text>
                    <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center', justifyContent:'center',backgroundColor:'white',  marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderBottomWidth:1,borderBottomColor:'#cccbcb',height:responsiveHeight(8)}}>
                    <View style={{marginHorizontal:moderateScale(5),}}>
                        <Icon name='phone' type='FontAwesome' style={{color:'gray',fontSize:responsiveFontSize(8)}} />
                    </View>
                        <TextInput
                        value={phone}
                        keyboardType='phone-pad'
                        onChangeText={(val)=>{this.setState({phone:val})}}
                        placeholder={Strings.enterYourPhone} underlineColorAndroid='transparent' style={{ffontSize:responsiveFontSize(6),width:responsiveWidth(80), fontFamily:isRTL?arrabicFont:englishFont, textAlign:isRTL?'right':'left', width:responsiveWidth(81), marginHorizontal:moderateScale(0), ontFamily:isRTL?arrabicFont:englishFont, textAlign:isRTL?'right':'left', marginHorizontal:moderateScale(4), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                    </View>
                    {phone.length==0&&
                    <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                    }
                </Animatable.View>

                <Animatable.View animation={isRTL?"slideInRight":"slideInLeft"} duration={1500} style={{marginVertical:moderateScale(10), width:responsiveWidth(90),alignSelf:'center'}}>
                    <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'black',fontSize:responsiveFontSize(7),alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.password}</Text>
                    <View style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center', justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row',alignItems:'center', backgroundColor:'white',  marginTop:moderateScale(1),width:responsiveWidth(90),borderRadius:moderateScale(2), borderBottomWidth:1,borderBottomColor:'#cccbcb',height:responsiveHeight(8)}}>
                    <View style={{marginHorizontal:moderateScale(5),}}>
                        <Icon name='lock1' type='AntDesign' style={{color:'gray',fontSize:responsiveFontSize(8)}} />
                    </View>
                        <TextInput         
                        secureTextEntry={hidePassword}
                        onChangeText={(val)=>{this.setState({password:val})}}
                        placeholder={Strings.enterYourPassword} underlineColorAndroid='transparent' style={{fontSize:responsiveFontSize(6), fontFamily:isRTL?arrabicFont:englishFont, textAlign:isRTL?'right':'left', width:responsiveWidth(75), marginHorizontal:moderateScale(0), color:'gray',direction:isRTL?'rtl':'ltr'}}  />
                        <TouchableOpacity 

                        onPress={()=>{this.setState({hidePassword:!hidePassword})}}
                        >
                            <Icon style={{fontSize:responsiveFontSize(6), color:'#D6D6D6'}} name={hidePassword?'eye-off':'eye'} type='Feather' />
                        </TouchableOpacity>
                    </View>
                    {password.length==0&&
                      <Text style={{color:'red',fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {passwordError}</Text>
                    }
                </Animatable.View>


                <View style={{marginVertical:moderateScale(15)}} >
                <AnimateLoadingButton
                ref={c => (this.loadingButton = c)}
                width={responsiveWidth(80)}
                height={responsiveHeight(8)}
                title={Strings.save}
                titleFontSize={responsiveFontSize(8)}
                titleColor="white"
                backgroundColor="#679C8A"
                borderRadius={moderateScale(3)}
                onPress={()=>{
                    this.loadingButton.showLoading(this.state.loading);
                    if(this.state.loading){
                        Alert.alert('ddddd')
                    }
                    if(!this.state.loading){
                        
                        if(name.replace(/\s/g, '').length<3){
                            this.setState({name:'',nameError:Strings.nameLength})
                            }
                            if(phone.replace(/\s/g, '').length<11){
                            this.setState({phone:'',phoneError:Strings.phoneLength})
                            }
                            if(password.replace(/\s/g, '').length<6){
                            this.setState({password:'',passwordError:Strings.passwordLength})
                            }     
                            if(!phone.replace(/\s/g, '').length){
                                this.setState({phone:''})
                            }
                            if(!name.replace(/\s/g, '').length){
                                this.setState({name:''})
                            }
                            if(!email.replace(/\s/g, '').length){
                                this.setState({email:''})
                            }
                            if(!password.replace(/\s/g, '').length){
                                this.setState({passwordError:Strings.require})
                            }
                            
            
                            if(password.replace(/\s/g, '').length>=6&& name.replace(/\s/g, '').length>=3&&phone.replace(/\s/g, '').length>=11&&password.replace(/\s/g, '').length&& phone.replace(/\s/g, '').length&&name.replace(/\s/g, '').length&&email.replace(/\s/g, '').length){
                            //this.onUpdate(name,email,phone,currentUser.user.country);
                            const data = new FormData()
                            if(img){
                                data.append('avatar',{
                                uri: img,
                                type: 'multipart/form-data',
                                name: 'productImages'
                                })
                            }
                            data.append('user_id',currentUser.user.id)
                            data.append('name',name)
                            data.append('email',email.toLocaleLowerCase())
                            data.append('phone',phone)
                            data.append('gender',currentUser.user.gender)
                            data.append('birth_date',date)
                            data.append('password',password) 
                          
                            this.setState({loading:true})
                            this.onUpdate(data) 
            
                            }
                           
                    }
                }}
                />
                </View>




        </ScrollView>
        
             
                <Dialog
                width={responsiveWidth(90)}
                onHardwareBackPress={()=>{this.setState({ showImageModal: false })}}
                visible={this.state.showImageModal}
                onTouchOutside={() => {
                this.setState({ showImageModal: false });
                }}
                >   
                <View style={{width:responsiveWidth(90),marginVertical:moderateScale(10)}}>
                
                <TouchableOpacity
                 onPress={()=>{
                     this.setState({showImageModal:false})
                     ImagePicker.openCamera({
                        cropping: true,
                        width: 500,
                        height: 500,

                      }).then(image => {
                        this.setState({img:image.path});
                      });
                }}
                 style={{width:responsiveWidth(90),borderBottomColor:'#679C8A',borderBottomWidth:1 ,height:responsiveHeight(8),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <View style={{marginHorizontal:moderateScale(5)}}>
                    <Icon type='Entypo' name='camera' style={{fontSize:responsiveFontSize(8), color:'#679C8A'}} />
                    </View>
                    <Text style={{color:'#679C8A',fontSize:responsiveFontSize(8)}}>{Strings.camer}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                onPress={()=>{
                    this.setState({showImageModal:false})
                    ImagePicker.openPicker({
                        cropping: true,
                        width: 500,
                        height: 500,

                    })
                    .then(image => {
                         this.setState({img:image.path})
                      });
                }}
                 style={{width:responsiveWidth(90),height:responsiveHeight(8),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <View style={{marginHorizontal:moderateScale(7)}}>
                    <Icon type='MaterialIcons' name='photo-library' style={{fontSize:responsiveFontSize(8),color:'#679C8A'}} />
                    </View>
                    <Text style={{color:'#679C8A',fontSize:responsiveFontSize(8)}} >{Strings.gallery}</Text>
                </TouchableOpacity>

                
                </View>        
                </Dialog>
                {this.state.loading &&
            <LoadingDialogOverlay title={Strings.wait} />}
        </View>
            
        );
    }
}


const mapDispatchToProps = {
    getUser,
    removeItem,
    signup
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    barColor: state.lang.color,
    loading: state.auth.loading, 
})


export default connect(mapToStateProps,mapDispatchToProps)(UpdateProfile);

