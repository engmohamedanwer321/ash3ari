import { Navigation } from 'react-native-navigation';
import {HOC} from '../controlls/NavigationControll'
import SplashScreen from '../screens/SplashScreen';
import MenuContent from "../components/MenuContent";
import Login from '../screens/Login';
import Home from '../screens/Home';
import UpdateProfile from './UpdateProfile'
import Visits from './Visits';
import Signup from '../screens/Signup';
import Profile from './Profile';
import ForgetPassword from './ForgetPassword';
import Chat from './Chat';
import Diet from './Diet';
import ChatPeople from './ChatPeople'
import DirectChat from './DirectChat'
import SelectLanguage from './SelectLanguage';
import ContactUs from './ContactUs'
import MyAccount from './MyAccount'
import Orders from './Orders'
import SignupUserType from './SignupUserType'
import SignupServiceProvider from './SignupServiceProvider'
import SignupBeneficiaryMember from './SignupBeneficiaryMember'
import Notifications from './Notifications'
import MakeOrder from './MakeOrder'
import ServiceProviderProfile from './ServiceProviderProfile'
import Payment from './Payment'
import ServiceProviders from './ServiceProviders'
import UpdateServiceProviderProfile from './UpdateServiceProviderProfile'
import Wallet from './Wallet'
import ProviderOrders from './ProviderOrders'
import SubCategories from './SubCategories'
import AddRequestAndOffer from './AddRequestAndOffer'
import AddNewProject from './AddNewProject'
import RequestAndOfferDetails from './RequestAndOfferDetails'
import RequestsAndOffers from './RequestsAndOffers'
import ServiceProviderPage from './ServiceProviderPage'
import UploadTransferReceipt from './UploadTransferReceipt'
import BankAccounts from './BankAccounts'
import GoStoreForUpdate from './GoStoreForUpdate'
import OrderDetails from './OrderDetails'
import ProviderOrderDetails from './ProviderOrderDetails'
import ProviderProjects from './ProviderProjects'

export default function registerScreens() {
     Navigation.registerComponent("SplashScreen", ()=>HOC(SplashScreen), () => SplashScreen);
     Navigation.registerComponent("MenuContent", ()=>HOC(MenuContent), () => MenuContent);
     Navigation.registerComponent("Login",()=>HOC(Login), () => Login); 
     Navigation.registerComponent("Home", ()=>HOC(Home), () => Home)
     Navigation.registerComponent("UpdateProfile",()=>HOC(UpdateProfile), () => UpdateProfile);
     Navigation.registerComponent("Visits",()=>HOC(Visits), () => Visits);
     Navigation.registerComponent("Signup",()=>HOC(Signup), () => Signup);
     Navigation.registerComponent("Profile",()=>HOC(Profile), () => Profile);
     Navigation.registerComponent("ForgetPassword",()=>HOC(ForgetPassword), () => ForgetPassword);
     Navigation.registerComponent("Diet",()=>HOC(Diet), () => Diet);
     Navigation.registerComponent("DirectChat", ()=>HOC(SplashScreen), () => DirectChat);
     Navigation.registerComponent("ChatPeople",()=>HOC(ChatPeople), () => ChatPeople);
     Navigation.registerComponent("Chat", ()=>HOC(Chat), ()=> Chat);
     Navigation.registerComponent("SelectLanguage",()=>HOC(SelectLanguage), () => SelectLanguage);
     Navigation.registerComponent("ContactUs",()=>HOC(ContactUs), () => ContactUs);
     Navigation.registerComponent("MyAccount",()=>HOC(MyAccount), () => MyAccount);
     Navigation.registerComponent("Orders",()=>HOC(Orders), () => Orders);
     Navigation.registerComponent("SignupUserType",()=>HOC(SignupUserType), () => SignupUserType);
     Navigation.registerComponent("SignupServiceProvider",()=>HOC(SignupServiceProvider), () => SignupServiceProvider);
     Navigation.registerComponent("SignupBeneficiaryMember",()=>HOC(SignupBeneficiaryMember), () => SignupBeneficiaryMember);
     Navigation.registerComponent("Notifications",()=>HOC(Notifications), () => Notifications);
     Navigation.registerComponent("MakeOrder",()=>HOC(MakeOrder), () => MakeOrder);
     Navigation.registerComponent("ServiceProviderProfile",()=>HOC(ServiceProviderProfile), () => ServiceProviderProfile);
     Navigation.registerComponent("Payment",()=>HOC(Payment), () => Payment);
     Navigation.registerComponent("ServiceProviders",()=>HOC(ServiceProviders), () => ServiceProviders);
     Navigation.registerComponent("UpdateServiceProviderProfile",()=>HOC(UpdateServiceProviderProfile), () => UpdateServiceProviderProfile);
     Navigation.registerComponent("Wallet",()=>HOC(Wallet), () => Wallet);
     Navigation.registerComponent("ProviderOrders",()=>HOC(ProviderOrders), () => ProviderOrders);
     Navigation.registerComponent("SubCategories",()=>HOC(SubCategories), () => SubCategories);
     Navigation.registerComponent("AddRequestAndOffer",()=>HOC(AddRequestAndOffer), () => AddRequestAndOffer);
     Navigation.registerComponent("AddNewProject",()=>HOC(AddNewProject), () => AddNewProject);
     Navigation.registerComponent("RequestAndOfferDetails",()=>HOC(RequestAndOfferDetails), () => RequestAndOfferDetails);
     Navigation.registerComponent("RequestsAndOffers",()=>HOC(RequestsAndOffers), () => RequestsAndOffers);
     Navigation.registerComponent("ServiceProviderPage",()=>HOC(ServiceProviderPage), () => ServiceProviderPage);
     Navigation.registerComponent("UploadTransferReceipt",()=>HOC(UploadTransferReceipt), () => UploadTransferReceipt);
     Navigation.registerComponent("BankAccounts",()=>HOC(BankAccounts), () => BankAccounts);
     Navigation.registerComponent("GoStoreForUpdate",()=>HOC(GoStoreForUpdate), () => GoStoreForUpdate);
     Navigation.registerComponent("OrderDetails",()=>HOC(OrderDetails), () => OrderDetails);
     Navigation.registerComponent("ProviderOrderDetails",()=>HOC(ProviderOrderDetails), () => ProviderOrderDetails);
     Navigation.registerComponent("ProviderProjects",()=>HOC(ProviderProjects), () => ProviderProjects);
     
}

